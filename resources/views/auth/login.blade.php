@extends('templates.auth')
@section('content')

    <div></div>
        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
        	<div class="card-box card-box-auth">
            <div class="panel-heading text-center"> 
                <img src="{{ asset('assets/images/ico_berau.png') }}" style="width: 20%">
                <h2 class="text-center"><strong class="text-custom"><span style="color: black"> CHID </span> <span style="font-weight: 450; color: #D87A19">Website</span></strong> </h2>
            </div> 
            <div class="panel-body" style="padding-top: 0px; padding-left: 15px; padding-right: 15px">
            @if ($errors->has('error'))
                <div class="alert alert-danger">
                    <strong>{{ $errors->first('error') }}</strong>
                </div>
            @endif
            <form class="form-horizontal m-t-20" action="{{url('/login')}}" method="post">
                {{csrf_field()}}
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input name="username" class="form-control" type="text" required="" placeholder="Username">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input name="password" class="form-control" type="password" required="" placeholder="Password">
                    </div>
                </div>
                
                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-inverse btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                    </div>
                </div>

            </form> 
            
            </div>
            
        </div>
@endsection