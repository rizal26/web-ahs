@extends('templates.main')
@section('content')

<style>
    .welcome{
        background-color: #00000080;
        padding: 0px 12px 0px 12px;
        text-align: right;
        border-radius: 9px;
    }
    .welcome-text{
        color: #ffffff85;
        font-size: 27px;
    }
    
    .jam {
        font-size: 14px;
        color: #ffffff99;
        padding-bottom: 11px;
    }
</style>
                        
                        <div class="welcome pull-right">
                            <p class="welcome-text">Welcome to CHID Website</p>
                            <div class="jam" id="timestamp">
                                {{-- 17 Feb 2020 - 10:19:11 WITA --}}
                            </div>
                        </div>
                        <script>
                                $(document).ready(function() {
                                    $('.welcome').hide().fadeIn();
                                    setInterval(timestamp, 1000);
                                    function timestamp() {
                                        $.ajax({
                                            url: '{{url("/home/timestamp")}}',
                                            success: function(data) {
                                                var html = data;
                                                $('#timestamp').html(html);
                                            },
                                        });
                                    }
                                });
                        
                        </script>
@endsection