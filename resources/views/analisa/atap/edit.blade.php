@extends('templates.main')

@section('content')
                <!-- Forms -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="m-t-0 header-title"><b>Update Kelompok</b></h4>
                                    <p class="text-muted m-b-30 font-13">
                                        Form Kelompok Update
                                    </p>
                                    @if($errors->any())
                                        @foreach($errors->all() as $error)
                                            @if($errors->has('success'))
                                                <div class="alert alert-success">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @else
                                                <div class="alert alert-danger">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @endif    
                                        @endforeach
                                    @endif
                                    <form method="post" action="{{route('kelompok.edit', $kelompok->id)}}" class="form-horizontal m-b-10" role="form">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label for="descr" class="col-sm-3 control-label">Description</label>
                                            <div class="col-sm-9">
                                              <input name="descr" value="{{ $kelompok->descr }}" type="text" class="form-control" id="descr" placeholder="Description" required>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group m-b-0">
                                            <div class="col-sm-offset-3 col-sm-9">
                                              <button type="submit" id="update" class="btn btn-inverse waves-effect waves-light">Update</button>
                                              <a href="{{ url('kelompok') }}" class="btn btn-default waves-effect waves-light">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                    $("#pass").click(function() {
                        $("#btn-pass").addClass("hidden");
                        $("#btn-newpass").removeClass("hidden");
                        $("#btn-renewpass").removeClass("hidden");

                        $("#update").click(function() {
                            if ($("#val-pass").val() != $("#val-repass").val()) {
                                sweetAlert("Oops...","Password tidak sama","error");  
                                return false;
                            }
                        });
                    });
                    
                    
                </script>
@endsection