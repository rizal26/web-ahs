@extends('templates.main')

@section('content')
                <!-- Forms -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="m-t-0 header-title"><b>Add Material</b></h4>
                                    <p class="text-muted m-b-30 font-13">
                                        Form Material Creation
                                    </p>
                                    @if($errors->any())
                                        @foreach($errors->all() as $error)
                                            @if($errors->has('success'))
                                                <div class="alert alert-success">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @else
                                                <div class="alert alert-danger">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @endif    
                                        @endforeach
                                    @endif
                                    <form method="post" action="{{route('material.add')}}" class="form-horizontal m-b-10" role="form" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <div class="form-group" style="margin-bottom: 1px">
                                            <label for="foto" class="col-sm-3 control-label">Foto</label>
                                            <div class="col-sm-9">
                                                <input name="foto" type="file" accept="image/*" class="form-control" id="foto" placeholder="Foto" required>
                                                <p  style="line-height: 2; font-size: 10px; margin-bottom: 1px;">*max 2 MB</p>
                                            </div>
                                        </div>
                                        {{-- <div class="form-group">
                                            <label for="id_material" class="col-sm-3 control-label">Kode Material</label>
                                            <div class="col-sm-9">
                                              <input name="id_material" type="text" class="form-control" id="id_material" placeholder="Kode Material" readonly>
                                            </div>
                                        </div> --}}
                                        <div class="form-group">
                                            <label for="descr" class="col-sm-3 control-label">Nama Material</label>
                                            <div class="col-sm-9">
                                                <input name="descr" type="text" class="form-control" id="descr" placeholder="Nama Material" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="kelompok_material" class="col-sm-3 control-label">Kelompok</label>
                                            <div class="col-sm-9">
                                                <select class="form-control select2-kelompok" id="kelompok" name="kelompok" required>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="satuan" class="col-sm-3 control-label">Satuan</label>
                                            <div class="col-sm-9">
                                                <input name="satuan" type="text" class="form-control" id="satuan" placeholder="Satuan" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="harga" class="col-sm-3 control-label">Harga</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="harga" placeholder="Harga e.g. Rp. 1,234,567.89" data-a-sign="Rp. " class="form-control autonumber">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group m-b-0">
                                            <div class="col-sm-offset-3 col-sm-9">
                                              <button type="submit" class="btn btn-inverse waves-effect waves-light">Create</button>
                                              <a href="{{ url('material') }}" class="btn btn-default waves-effect waves-light">Back</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script src="{{asset('assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js')}}" type="text/javascript"></script>
                <script src="{{asset('assets/plugins/autoNumeric/autoNumeric.js')}}" type="text/javascript"></script>

                <script type="text/javascript">
                    jQuery(function($) {
                        $('.autonumber').autoNumeric('init');    
                    });
                </script>

                <script type="text/javascript">
                    $(function() {
                        $(".select2-kelompok").select2({
                            placeholder: 'Pilih Kelompok',
                            allowClear: true,
                            matcher: true,
                            ajax: {
                                url: '{{ url("/kelompok/ajaxselect") }}',
                                dataType: 'json',
                                data: function (params) {
                                    var query = {
                                        search: params.term,
                                    }

                                    return query;
                                }
                            }
                        });                        
                    });
                </script>
@endsection