@extends('templates.main')

@section('content')
              <!-- Page-Title -->
              <div class="row">
                    <div class="col-sm-12">
                        @if (Session::get('role') == 1)
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{ url('material/add') }}" class="btn btn-default dropdown-toggle waves-effect">Add New<span class="m-l-5"><i class="fa fa-plus"></i></span></a>
                        </div>
                        @endif

                        <h4 class="page-title">Material Management</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="#">List Material</a>
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <!-- <h4 class="m-t-0 header-title"><b>List of User</b></h4> -->
                            <p class="text-muted font-13 m-b-20"></p>
                            @if($errors->any())
                                @foreach($errors->all() as $error)
                                    @if($errors->has('success'))
                                        <div class="alert alert-success">
                                            <strong>{{ $error }}</strong>
                                        </div>
                                    @else
                                        <div class="alert alert-danger">
                                            <strong>{{ $error }}</strong>
                                        </div>
                                    @endif    
                                @endforeach
                            @endif
                            @if (isset($value))
                                    <p style="position: absolute; top: 11px;"><b>Result for "{{ $value }}"</b></p>
                            @endif
                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" colspan="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Foto</th>
                                        <th>Kode Material</th>
                                        <th>Nama</th>
                                        <th>Kelompok</th>
                                        <th>Satuan</th>
                                        <th>Price</th>
                                        <th>Old Price</th>
                                        <th>Modify by</th>
                                        <th>Last Update</th>
                                        @if (Session::get('role')==1)
                                        <th>Action</th>
                                        @endif
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

                <div id="del" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog"> 
                        <div class="modal-content"> 
                            <form method="post" action="{{route('material.del','test')}}">
                                {{method_field('delete')}} {{csrf_field()}}
                                <div class="modal-header"> 
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                    <h4 class="modal-title">Delete Confirmation</h4> 
                                </div> 
                                <div class="modal-body">
                                    <input type="hidden" name="id" id="id" value="">
                                    <input type="hidden" name="act" id="act" value="">
                                    <p>Are you sure want to delete of this record ?</p>
                                </div> 
                                <div class="modal-footer"> 
                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Yes</button>
                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">No</button> 
                                </div> 
                            </form>
                        </div> 
                    </div>
                </div><!-- /.modal -->  
                
                <div id="view" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog"> 
                        <div class="modal-content"> 
                            <div class="modal-header"> 
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                <h4 class="modal-title">Image Detail</h4> 
                            </div> 
                            <div class="modal-body" id="modal-image">
                            </div> 
                            {{-- <div class="modal-footer"> 
                                <button type="submit" class="btn btn-inverse waves-effect waves-light">Yes</button>
                                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">No</button> 
                            </div>  --}}
                        </div> 
                    </div>
                </div><!-- /.modal -->  

                <script type="text/javascript">
                    $(function() {
                        'use strict';

                        $('#del').on('show.bs.modal', function (event) {
                            var button = $(event.relatedTarget) 
                            var id = button.data('id')
                            var modal = $(this)
                            modal.find('.modal-body #id').val(id)
                        });

                        $('#view').on('show.bs.modal', function (event) {
                            var button = $(event.relatedTarget) 
                            var id = button.data('id')
                            var modal = $(this)
                            modal.find('.modal-body #id').val(id)
                            $.ajax({
                                type: "get",
                                url: "{{url('material/view')}}"+'/'+id,
                                dataType: "json",
                                beforeSend: function () {
                                    var load = '<center style="font-size: 32px;"><i class="fa fa-spin fa-refresh"></i></center>';
                                    $('#modal-image').html(load);  
                                },
                                success: function (response) {
                                    var html = '<img src="{{ asset("assets/images/material") }}/'+response[0].foto+'" alt="'+response[0].foto+'" style="width: 100%;">';
                                    $('#modal-image').html(html);
                                }
                            });
                        });

                        var table = $('#datatable').DataTable({
                            searchDelay: 1000,
                            responsive: true,
                            processing: true,
                            language: {
                                searchPlaceholder: 'Search...',
                                sSearch: '',
                                processing: "<i class='fa fa-spin fa-refresh'></i>",
                            },
                            // serverSide: true,
                            // ordering: false,
                            // bLengthChange: false,
                            ajax: {
                                url: "{{url('material/ajax')}}",
                                type: 'GET',
                                dataSrc: 'data',
                            },
                            columns: [
                                { data : 'foto' },
                                { data : 'id_material' },
                                { data : 'descr' },
                                { data : 'kelompok' },
                                { data : 'satuan' },
                                { data : 'harga' },
                                { data : 'old_harga' },
                                { data : 'modify_by' },
                                { data : 'last_update' },
                                <?php 
                                if (Session::get('role') == 1) { ?>
                                    { data: 'action' }
                                <?php } ?>
                            ],
                        });

                        var param = '{{ isset($value)?$value:'' }}';
                        table
                            .columns(2)
                            .search(param)
                            .draw(); 
                    });
                </script>  
@endsection
