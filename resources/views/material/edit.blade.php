@extends('templates.main')
@section('content')

<style>
.container-img {
  position: relative;
  width: 25%;
}

.image {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.container-img:hover .image {
  opacity: 0.3;
}

.container-img:hover .middle {
  opacity: 1;
}

</style>
                <!-- Forms -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="m-t-0 header-title"><b>Edit Material</b></h4>
                                    <p class="text-muted m-b-30 font-13">
                                        Form Material Edit
                                    </p>
                                    @if($errors->any())
                                        @foreach($errors->all() as $error)
                                            @if($errors->has('success'))
                                                <div class="alert alert-success">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @else
                                                <div class="alert alert-danger">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @endif    
                                        @endforeach
                                    @endif
                                    <form method="post" action="{{ route('material.edit', $material->id) }}" class="form-horizontal m-b-10" role="form" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label for="foto" class="col-sm-3 control-label">Photo</label>
                                            <div class="col-sm-9">
                                                <input name="foto" type="file" accept="image/*" class="form-control" id="foto" placeholder="Photo" disabled>
                                                <p  style="line-height: 2; font-size: 10px;     margin-bottom: -10px;">*max 2 MB</p>
                                                <div class="container-img" style="padding-top: 10px">
                                                    <img src="{{ asset('assets/images/material/'.$material->foto) }}"  class="image">
                                                    <div class="middle">
                                                        <a href="#" id="close" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- <div class="form-group">
                                            <label for="id_material" class="col-sm-3 control-label">Kode Material</label>
                                            <div class="col-sm-9">
                                              <input name="id_material" type="text" class="form-control" id="id_material" placeholder="Kode Material" readonly>
                                            </div>
                                        </div> --}}
                                        <div class="form-group">
                                            <label for="descr" class="col-sm-3 control-label">Nama Material</label>
                                            <div class="col-sm-9">
                                                <input name="descr" type="text" class="form-control" id="descr" value="{{ $material->descr }}" placeholder="Nama Material" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="kelompok_material" class="col-sm-3 control-label">Kelompok</label>
                                            <div class="col-sm-9">
                                                <select class="form-control select2-kelompok" id="kelompok" name="kelompok" required>
                                                    @foreach ($kelompok as $item)
                                                    <option value="{{ $item->id }}" {{ $item->id == $material->kelompok?'selected':'' }}>{{ $item->descr }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="satuan" class="col-sm-3 control-label">Satuan</label>
                                            <div class="col-sm-9">
                                                <input name="satuan" type="text" class="form-control" id="satuan" value="{{ $material->satuan }}" placeholder="Satuan" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="harga" class="col-sm-3 control-label">Harga</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="harga" value="{{ $material->harga }}" placeholder="Harga e.g. Rp. 1,234,567.89" data-a-sign="Rp. " class="form-control autonumber">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group m-b-0">
                                            <div class="col-sm-offset-3 col-sm-9">
                                              <button type="submit" class="btn btn-inverse waves-effect waves-light">Update</button>
                                              <a href="{{ url('material') }}" class="btn btn-default waves-effect waves-light">Back</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script src="{{asset('assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js')}}" type="text/javascript"></script>
                <script src="{{asset('assets/plugins/autoNumeric/autoNumeric.js')}}" type="text/javascript"></script>

                <script>
                    $( "#close" ).click(function() {
                        $( ".container-img" ).fadeOut( "slow" );
                        $("#foto").removeAttr("disabled");
                    });
                </script>

                <script type="text/javascript">
                    jQuery(function($) {
                        $('.autonumber').autoNumeric('init');    
                    });
                </script>

                <script type="text/javascript">
                    $(function() {
                        $(".select2-kelompok").select2({
                            placeholder: 'Pilih Kelompok',
                            allowClear: true,
                            // matcher: true,
                            // ajax: {
                            //     url: '{{ url("/kelompok/ajaxselect/") }}',
                            //     dataType: 'json',
                            //     data: function (params) {
                            //         var query = {
                            //             search: params.term,
                            //         }

                            //         return query;
                            //     }
                            // }
                        });                        
                    });
                </script>
@endsection