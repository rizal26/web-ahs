@extends('templates.main')

@section('content')
<style>
    hr {
        margin-top: 0px;
        margin-bottom: 15px;
        border-top: 2px solid #979797;
    }
</style>
                <!-- Forms -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-md-12">
                                    {{-- <h4 class="m-t-0 header-title"><b>Create PIR</b></h4>
                                    <p class="text-muted m-b-30 font-13">
                                        Form PIR Creation
                                    </p> --}}

                                    <div class="row" style="padding-bottom: 40px">
                                        <div class="container">   
                                            <div class="row bs-wizard" style="border-bottom:0;">
                                                <div class="col-xs-3 three-step bs-wizard-step active">
                                                    <div class="text-center bs-wizard-stepnum">Step 1</div>
                                                    <div class="progress"><div class="progress-bar"></div></div>
                                                    <a href="#" class="bs-wizard-dot"></a>
                                                    <div class="bs-wizard-info text-center">Initiations / Idea Generations </div>
                                                </div>
                                                <div class="col-xs-3 three-step bs-wizard-step disabled">
                                                    <div class="text-center bs-wizard-stepnum">Step 2</div>
                                                    <div class="progress"><div class="progress-bar"></div></div>
                                                    <a href="#" class="bs-wizard-dot"></a>
                                                    <div class="bs-wizard-info text-center">Value Evaluations</div>
                                                </div>
                                                <div class="col-xs-3 three-step bs-wizard-step disabled">
                                                    <div class="text-center bs-wizard-stepnum">Step 3</div>
                                                    <div class="progress"><div class="progress-bar"></div></div>
                                                    <a href="#" class="bs-wizard-dot"></a>
                                                    <div class="bs-wizard-info text-center">Risk Evaluations</div>
                                                </div>
                                                <div class="col-xs-3 three-step bs-wizard-step disabled">
                                                    <div class="text-center bs-wizard-stepnum">Step 4</div>
                                                    <div class="progress"><div class="progress-bar"></div></div>
                                                    <a href="#" class="bs-wizard-dot"></a>
                                                    <div class="bs-wizard-info text-center">Resume Path / Finish</div>
                                                </div>
                                            </div>    
                                        </div>
                                    </div>

                                    @if($errors->any())
                                        @foreach($errors->all() as $error)
                                            @if($errors->has('success'))
                                                <div class="alert alert-success">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @else
                                                <div class="alert alert-danger">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @endif    
                                        @endforeach
                                    @endif

                                    <form method="post" onsubmit="return confirm('Are you sure?\nyou will not be able to go back!')" action="{{route('pir.add')}}" class="form-horizontal" role="form" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <div class="row" style="background-color: #e6ffd7">
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;">1. PROJECT DETAILS</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Name</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="descr" placeholder="Project Name" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Category</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2-category" id="category" name="category" required>
                                                            <option></option>
                                                            <option>Productions</option>
                                                            <option>Regulations</option>
                                                            <option>Innovations & Improvement</option>
                                                            <option>Sustainability</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Location</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="location" placeholder="Project location" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Type</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2-type" id="type" name="type" required>
                                                            <option></option>
                                                            <option value="New Project">New Project</option>
                                                            <option value="Typical Existing">Typical Existing</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;">2. PROJECT OWNERSHIP</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Department</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2-department" id="department" name="department" required></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Sponsor</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="sponsor" placeholder="Project Sponsor" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Owner</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="owner" placeholder="Project Owner" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Manager</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="manager" placeholder="Project Manager" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;">3. PROJECT REQUIREMENTS</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label for="mandatory" class="col-md-5 control-label">Is this project mandatory ? </label>
                                                    <div class="col-sm-1" style="padding-top: 2px;"></div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="man_yes" value="1" name="mandatory" required>
                                                            <label for="man_yes"> Yes </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="man_no" value="0" name="mandatory">
                                                            <label for="man_no"> No </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="urgent" class="col-md-5 control-label">Is this project urgent ? </label>
                                                    <div class="col-sm-1" style="padding-top: 2px;"></div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="urg_yes" value="1" name="urgent" required>
                                                            <label for="urg_yes"> Yes </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="urg_normal" value="0" name="urgent">
                                                            <label for="urg_normal"> Normal </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="connect" class="col-md-5 control-label">Is this project connect to other project ? </label>
                                                    <div class="col-sm-1" style="padding-top: 2px;"></div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="conn_yes" value="1" name="connect" required>
                                                            <label for="conn_yes"> Yes </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="conn_no" value="0" name="connect">
                                                            <label for="conn_no"> No </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-5 control-label">What's project reason ?</label>
                                                    <div class="col-md-7">
                                                        <select class="form-control select2-reason" id="reason" name="reason" required>
                                                            <option></option>
                                                            <option>Productions</option>
                                                            <option>Regulations</option>
                                                            <option>Innovations & Improvement</option>
                                                            <option>Sustainability</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-5 control-label">Reason Description</label>
                                                    <div class="col-md-7">
                                                        <textarea class="form-control" name="reason_descr" rows="5" placeholder="Reason Description" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">4. Project Background & Objective</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control" name="project_background" rows="5" placeholder="Project Background & Objective Description" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="background-color: #e6ffd7">
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">5. Project Descriptions</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control" name="project_descr" rows="5" placeholder="Project Description" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">6. Project Budget</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label for="expenditure" class="col-md-5 control-label">Expenditure type ?</label>
                                                    <div class="col-sm-1" style="padding-top: 2px;"></div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="expn_yes" value="1" name="expenditure" required>
                                                            <label for="expn_yes"> Capex </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="expn_no" value="0" name="expenditure" required>
                                                            <label for="expn_no"> Opex </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="annual_budget" class="col-md-5 control-label">Includeed in annual budget ?</label>
                                                    <div class="col-sm-1" style="padding-top: 2px;"></div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="budget_yes" value="1" name="annual_budget" required>
                                                            <label for="budget_yes"> Yes </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="budget_no" value="0" name="annual_budget">
                                                            <label for="budget_no"> No </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-5 control-label">Budget Estimate (USD) ?</label>
                                                    <div class="col-md-7">
                                                        <input type="text" placeholder="Budget Estimate (USD)" data-a-sign="USD. " class="form-control autonumber" name="estimate_budget" id="estimate_budget" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">7. Project Potentials Alternative </span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control" name="project_potential" rows="5" placeholder="Project Potentials" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">8. Project Scope</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Project in scope</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Project in scope" class="form-control" name="in_scope" id="in_scope" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Project Out scope</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Project out scope" class="form-control" name="out_scope" id="out_scope" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="background-color: #e6ffd7">
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">9. Project Key Deliverables</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Key Deliverables</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Key Deliverables" class="form-control" name="deliver" id="deliver" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control" name="deliver_descr" rows="5" placeholder="Key Deliverables Description" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">10. Project Milestones</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Start Date</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" placeholder="dd-Mmm-yyyy" id="datepicker" name="start_date" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">End Date</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" placeholder="dd-Mmm-yyyy" id="datepicker-end" name="end_date" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Key Milestones</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Key Milestones" class="form-control" name="milestones" id="milestones" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Milestones Details</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Milestones Details" class="form-control" name="milestones_descr" id="milestones_descr" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">11. Project Constraint </span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control" name="constraint" rows="5" placeholder="Project Constraint" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">12. Project Implementations Impact </span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Implementations Impact</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2-impact" id="impact" name="impact" required>
                                                            <option></option>
                                                            <option>Productions</option>
                                                            <option>Regulations</option>
                                                            <option>Cost Efficiency</option>
                                                            <option>Sustainability</option>
                                                            <option>etc</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form-impact-etc" hidden>
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Description Implementations Impact" class="form-control" name="impact_etc" id="impact_etc">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="background-color: #e6ffd7">
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">13. Project Dependencies </span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Internal</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Description" class="form-control" name="internal" id="internal" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">External</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Description" class="form-control" name="external" id="external" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">14. Project Specifications </span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Civil & Structure</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2-structure" id="structure" name="structure" required>
                                                            <option></option>
                                                            <option>Building / Warehouse</option>
                                                            <option>Bridge & Road</option>
                                                            <option>Fix Plant</option>
                                                            <option>Jetty / Harbour</option>
                                                            <option>Earthwork</option>
                                                            <option>Support Facilities</option>
                                                            <option>Other Work</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form-structure-etc" hidden>
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Description Civil & Structure" class="form-control" name="structure_etc" id="structure_etc">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Mechanical</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2-mechanical" id="mechanical" name="mechanical" required>
                                                            <option></option>
                                                            <option>Hydrant / Water Pump</option>
                                                            <option>Pipe work</option>
                                                            <option>Fix Plant</option>
                                                            <option>Metal Catcher / Magnetic Separator</option>
                                                            <option>Fuel Tank / Station</option>
                                                            <option>Maintenance / Modifications work</option>
                                                            <option>Motor Installations</option>
                                                            <option>Other Work</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form-mechanical-etc" hidden>
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Description Mechanical" class="form-control" name="mechanical_etc" id="mechanical_etc">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Electrical</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2-electrical" id="electrical" name="electrical" required>
                                                            <option></option>
                                                            <option>Genset</option>
                                                            <option>CCTV</option>
                                                            <option>Scada</option>
                                                            <option>Power House</option>
                                                            <option>Other Work</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form-electrical-etc" hidden>
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Description Electrical" class="form-control" name="electrical_etc" id="electrical_etc">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Design & Build</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2-design" id="design" name="design" required>
                                                            <option></option>
                                                            <option>Fix Plant</option>
                                                            <option>Other Work</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">15. PROJECT COST</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Project Cost Estimate</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Project Cost Estimate" data-a-sign="USD. " class="form-control autonumber" name="cost" id="cost" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">16. PROJECT SPECIFICATIONS</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Preliminary Design</label>
                                                    <div class="col-sm-9">
                                                        <input name="preliminary_design" type="file" accept="image/*" class="form-control" id="preliminary_design" placeholder="Foto" required>
                                                        <p  style="line-height: 2; font-size: 10px; margin-bottom: 1px;">*image max 2 MB</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Preliminary Specifications</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Preliminary Specifications" class="form-control" name="preliminary_spec" id="preliminary_spec" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Time Frame</label>
                                                    <div class="col-sm-9">
                                                        <div class="input-daterange input-group" id="date-range">
                                                            <input type="text" placeholder="dd-Mmm-yyyy" class="form-control" name="time_frame_start" required>
                                                            <span class="input-group-addon bg-custom b-0 text-white">to</span>
                                                            <input type="text" placeholder="dd-Mmm-yyyy" class="form-control" name="time_frame_end" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="padding-top: 47px">
                                            <div class="form-group pull-right">
                                                <div class="col-sm-12" style="padding-right: 40px;">
                                                    <a href="{{ url('pir') }}" class="btn btn-default waves-effect waves-light">Close</a>
                                                    <button type="submit" id="primary-alert" class="btn btn-inverse waves-effect waves-light">Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script src="{{asset('assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js')}}" type="text/javascript"></script>
                <script src="{{asset('assets/plugins/autoNumeric/autoNumeric.js')}}" type="text/javascript"></script>

                <script>
                    $(document).ready(function() {       
                        $('#preliminary_design').bind('change', function() {
                            var ukuran = (this.files[0].size);
                            var allow = ['JPG', 'JPEG', 'PNG'];
                            var value = $(this).val();
                            var type = value.split('.');
                            last =  function(array, n) {
                                if (array == null) 
                                    return void 0;
                                if (n == null) 
                                    return array[array.length - 1];
                                return array.slice(Math.max(array.length - n, 0));  
                            };
                            // console.log(last(type));
                            
                            if ($.inArray(last(type).toUpperCase(), allow) == -1) {
                                // console.log('tidak ada');
                                alert('Wrong file type\nOnly allowed extension jpg, jpeg, png');
                                return $(this).val('');
                            } 
                            // console.log(type);
                            if(ukuran > 2000000) {
                                alert('File size too large !\nMax 2 Mb');
                                return $(this).val('');
                            };
                        });
                    });
                </script>

                <script type="text/javascript">
                    jQuery(function($) {
                        $('.autonumber').autoNumeric('init');    
                        $('#datepicker').datepicker({
                            format: 'dd-M-yyyy'
                        });
                        $('#datepicker-end').datepicker({
                            format: 'dd-M-yyyy'
                        });
                        $('#date-range').datepicker({
                            toggleActive: true,
                            format: 'dd-M-yyyy'
                        });
                    });
                </script>

                <script type="text/javascript">
                    $(document).ready(function () {
                        $(".select2-category").select2({
                            placeholder: 'Select Category',
                            allowClear: true
                        });

                        $(".select2-type").select2({
                            placeholder: 'Select Type',
                            allowClear: true
                        });

                        $(".select2-department").select2({
                            placeholder: 'Select Department',
                            allowClear: true,
                            matcher: true,
                            ajax: {
                                url: '{{ url("/department/ajaxselect") }}',
                                dataType: 'json',
                                data: function (params) {
                                    var query = {
                                        search: params.term,
                                    }

                                    return query;
                                }
                            }
                        });  

                        $(".select2-reason").select2({
                            placeholder: 'Select Reason',
                            allowClear: true
                        });

                        $(".select2-impact").select2({
                            placeholder: 'Select Implementations Impact',
                            allowClear: true
                        });

                        $('.select2-impact').change(function (e) { 
                            e.preventDefault();
                            if ($(this).val() == 'etc') {
                                $('#form-impact-etc').show();
                                $('#impact_etc').attr('required', '');
                            } else {
                                $('#form-impact-etc').hide();
                                $('#impact_etc').removeAttr('required');
                                $('#impact_etc').val('');
                            }
                        });

                        $(".select2-structure").select2({
                            placeholder: 'Select Civil & Structure',
                            allowClear: true
                        });

                        $('.select2-structure').change(function (e) { 
                            e.preventDefault();
                            if ($(this).val() == 'Other Work') {
                                $('#form-structure-etc').show();
                                $('#structure_etc').attr('required', '');
                            } else {
                                $('#form-structure-etc').hide();
                                $('#structure_etc').removeAttr('required');
                                $('#structure_etc').val('');
                            }
                        });

                        $(".select2-mechanical").select2({
                            placeholder: 'Select Mechanical',
                            allowClear: true
                        });

                        $('.select2-mechanical').change(function (e) { 
                            e.preventDefault();
                            if ($(this).val() == 'Other Work') {
                                $('#form-mechanical-etc').show();
                                $('#mechanical_etc').attr('required', '');
                            } else {
                                $('#form-mechanical-etc').hide();
                                $('#mechanical_etc').removeAttr('required');
                                $('#mechanical_etc').val('');
                            }
                        });

                        $(".select2-electrical").select2({
                            placeholder: 'Select Electrical',
                            allowClear: true
                        });

                        $('.select2-electrical').change(function (e) { 
                            e.preventDefault();
                            if ($(this).val() == 'Other Work') {
                                $('#form-electrical-etc').show();
                                $('#electrical_etc').attr('required', '');
                            } else {
                                $('#form-electrical-etc').hide();
                                $('#electrical_etc').removeAttr('required');
                                $('#electrical_etc').val('');
                            }
                        });
                    });
                </script>
                
@endsection