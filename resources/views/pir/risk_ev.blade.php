@extends('templates.main')

@section('content')
<style>
    hr {
        margin-top: 0px;
        margin-bottom: 15px;
        border-top: 2px solid #979797;
    }
</style>
                <!-- Forms -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-md-12">
                                    {{-- <h4 class="m-t-0 header-title"><b>Create PIR</b></h4>
                                    <p class="text-muted m-b-30 font-13">
                                        Form PIR Creation
                                    </p> --}}

                                    <div class="row" style="padding-bottom: 40px">
                                        <div class="container">   
                                            <div class="row bs-wizard" style="border-bottom:0;">
                                                <div class="col-xs-3 three-step bs-wizard-step complete">
                                                    <div class="text-center bs-wizard-stepnum">Step 1</div>
                                                    <div class="progress"><div class="progress-bar"></div></div>
                                                    <a href="#" class="bs-wizard-dot"></a>
                                                    <div class="bs-wizard-info text-center">Initiations / Idea Generations </div>
                                                </div>
                                                <div class="col-xs-3 three-step bs-wizard-step complete">
                                                    <div class="text-center bs-wizard-stepnum">Step 2</div>
                                                    <div class="progress"><div class="progress-bar"></div></div>
                                                    <a href="#" class="bs-wizard-dot"></a>
                                                    <div class="bs-wizard-info text-center">Value Evaluations</div>
                                                </div>
                                                <div class="col-xs-3 three-step bs-wizard-step active">
                                                    <div class="text-center bs-wizard-stepnum">Step 3</div>
                                                    <div class="progress"><div class="progress-bar"></div></div>
                                                    <a href="#" class="bs-wizard-dot"></a>
                                                    <div class="bs-wizard-info text-center">Risk Evaluations</div>
                                                </div>
                                                <div class="col-xs-3 three-step bs-wizard-step disabled">
                                                    <div class="text-center bs-wizard-stepnum">Step 4</div>
                                                    <div class="progress"><div class="progress-bar"></div></div>
                                                    <a href="#" class="bs-wizard-dot"></a>
                                                    <div class="bs-wizard-info text-center">Resume Path / Finish</div>
                                                </div>
                                            </div>    
                                        </div>
                                    </div>

                                    @if($errors->any())
                                        @foreach($errors->all() as $error)
                                            @if($errors->has('success'))
                                                <div class="alert alert-success">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @else
                                                <div class="alert alert-danger">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @endif    
                                        @endforeach
                                    @endif
                                                {{-- {{ Crypt::decrypt('parameter') }} --}}
                                    <form method="post" onsubmit="return confirm('Are you sure?\nyou will not be able to go back!')" action="{{route('risk_ev.add', $param['id_initiation'])}}" class="form-horizontal" role="form" >
                                        {{csrf_field()}}
                                        @foreach ($param['data'] as $item)
                                        <div class="row">
                                            <div class="form-group" style="padding-top: 15px;padding-bottom: 15px;background-color: #e6ffd7">
                                                <label for="kelompok_material" class="col-sm-5 control-label" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{ $item->descr }}">{{$item->question}}</label>
                                                <div class="col-sm-7">
                                                    <select class="form-control select2" style="width: 95%;" name="{{$item->id}}" required>
                                                        <option></option>
                                                    <?php 
                                                        $arrOpt = json_decode($item->option);
                                                        foreach ($arrOpt as $key => $value) { ?>
                                                        <option value="{{$value}}">{{$key}}</option>
                                                    <?php  } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach

                                        <div class="row" style="padding-top: 47px">
                                            <div class="form-group pull-right">
                                                <div class="col-sm-12" style="padding-right: 40px;">
                                                    {{-- <a href="{{ url('pir') }}" class="btn btn-default waves-effect waves-light">Prev</a> --}}
                                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script>
                    $(document).ready(function () {
                        $(".select2").select2({
                            placeholder: 'Select Answer',
                            allowClear: true
                        });
                    });
                </script>
@endsection