@extends('templates.main')

@section('content')
<style>
    hr {
        margin-top: 0px;
        margin-bottom: 15px;
        border-top: 2px solid #979797;
    }
</style>
                <!-- Forms -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-md-12">
                                    {{-- <h4 class="m-t-0 header-title"><b>Create PIR</b></h4>
                                    <p class="text-muted m-b-30 font-13">
                                        Form PIR Creation
                                    </p> --}}

                                    <div class="row" style="padding-bottom: 40px">
                                        <div class="container">   
                                            <div class="row bs-wizard" style="border-bottom:0;">
                                                <div class="col-xs-3 three-step bs-wizard-step complete">
                                                    <div class="text-center bs-wizard-stepnum">Step 1</div>
                                                    <div class="progress"><div class="progress-bar"></div></div>
                                                    <a href="#" class="bs-wizard-dot"></a>
                                                    <div class="bs-wizard-info text-center">Initiations / Idea Generations </div>
                                                </div>
                                                <div class="col-xs-3 three-step bs-wizard-step complete">
                                                    <div class="text-center bs-wizard-stepnum">Step 2</div>
                                                    <div class="progress"><div class="progress-bar"></div></div>
                                                    <a href="#" class="bs-wizard-dot"></a>
                                                    <div class="bs-wizard-info text-center">Value Evaluations</div>
                                                </div>
                                                <div class="col-xs-3 three-step bs-wizard-step complete">
                                                    <div class="text-center bs-wizard-stepnum">Step 3</div>
                                                    <div class="progress"><div class="progress-bar"></div></div>
                                                    <a href="#" class="bs-wizard-dot"></a>
                                                    <div class="bs-wizard-info text-center">Risk Evaluations</div>
                                                </div>
                                                <div class="col-xs-3 three-step bs-wizard-step active">
                                                    <div class="text-center bs-wizard-stepnum">Step 4</div>
                                                    <div class="progress"><div class="progress-bar"></div></div>
                                                    <a href="#" class="bs-wizard-dot"></a>
                                                    <div class="bs-wizard-info text-center">Resume Path / Finish</div>
                                                </div>
                                            </div>    
                                        </div>
                                    </div>

                                    @if($errors->any())
                                        @foreach($errors->all() as $error)
                                            @if($errors->has('success'))
                                                <div class="alert alert-success">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @else
                                                <div class="alert alert-danger">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @endif    
                                        @endforeach
                                    @endif
                                                {{-- {{ Crypt::decrypt('parameter') }} --}}
                                    <form method="post" onsubmit="return confirm('Are you sure?\nyou will not be able to go back!')" action="{{route('resume.edit', $param['id_initiation'])}}" class="form-horizontal" role="form" >
                                        {{csrf_field()}}
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="portlet" style="box-shadow: 2px 3px #8888884a;">
                                                    <div class="portlet-heading bg-inverse">
                                                        <h3 class="portlet-title">
                                                            Initiations / Idea Generations 	
                                                        </h3>
                                                        <div class="portlet-widgets">
                                                            {{-- <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                                            <span class="divider"></span> --}}
                                                            <a data-toggle="collapse" data-parent="#accordion1" href="#bg-inverse"><i class="ion-minus-round"></i></a>
                                                            <span class="divider"></span>
                                                            {{-- <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a> --}}
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div id="bg-inverse" class="panel-collapse collapse in">
                                                        <div class="portlet-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <h4 class="m-t-0 header-title"><b>1. Project Details</b></h4>
                                                                    <table class="table table-light">
                                                                        <tbody>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Project Number</td>
                                                                                <td>:  {{ $data->id }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Project Name</td>
                                                                                <td>:  {{ $data->descr }}</td>
                                                                            </tr>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Project Category</td>
                                                                                <td>:  {{ $data->category }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Project Location</td>
                                                                                <td>:  {{ $data->location }}</td>
                                                                            </tr>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Project Type</td>
                                                                                <td>:  {{ $data->type }}</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <h4 class="m-t-0 header-title"><b>2. Project Ownership</b></h4>
                                                                    <table class="table table-light">
                                                                        <tbody>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Department</td>
                                                                                <td>:  {{ $data->department_nm }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Project Sponsor</td>
                                                                                <td>:  {{ $data->sponsor }}</td>
                                                                            </tr>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Project Owner</td>
                                                                                <td>:  {{ $data->owner }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Project Manager</td>
                                                                                <td>:  {{ $data->manager }}</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                            <div class="row" style="padding-top: 20px;">
                                                                <div class="col-md-6">
                                                                    <h4 class="m-t-0 header-title"><b>3. Project Requirements</b></h4>
                                                                    <table class="table table-light">
                                                                        <tbody>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Is this project mandatory ?</td>
                                                                                <td>:  {{ $data->mandatory==1?'Yes':'No' }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Is this project urgent ?</td>
                                                                                <td>:  {{ $data->urgent==1?'Yes':'Normal' }}</td>
                                                                            </tr>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Is this project connect to other project ?</td>
                                                                                <td>:  {{ $data->connect==1?'Yes':'No' }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>What's project reason ?</td>
                                                                                <td>:  {{ $data->reason }}</td>
                                                                            </tr>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Reason Description</td>
                                                                                <td>:  {{ $data->reason_descr }}</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <h4 class="m-t-0 header-title"><b>4. Project Background & Objective</b></h4>
                                                                    <table class="table table-light">
                                                                        <tbody>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Description</td>
                                                                                <td>:  {{ $data->project_background }}</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                            <div class="row" style="padding-top: 20px;">
                                                                <div class="col-md-6">
                                                                    <h4 class="m-t-0 header-title"><b>5. Project Descriptions</b></h4>
                                                                    <table class="table table-light">
                                                                        <tbody>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Description</td>
                                                                                <td>:  {{ $data->project_descr }}</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <h4 class="m-t-0 header-title"><b>6. Project Budget</b></h4>
                                                                    <table class="table table-light">
                                                                        <tbody>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Expenditure type ?</td>
                                                                                <td>:  {{ $data->expenditure==1?'Capex':'Opex' }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Project includeed in annual budget ?</td>
                                                                                <td>:  {{ $data->annual_budget==1?'Yes':'No' }}</td>
                                                                            </tr>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Project budget estimate (USD) ?</td>
                                                                                <td>:  $ {{ number_format($data->estimate_budget, 3) }}</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                            <div class="row" style="padding-top: 20px;">
                                                                <div class="col-md-6">
                                                                    <h4 class="m-t-0 header-title"><b>7. Project Potentilas Alternative</b></h4>
                                                                    <table class="table table-light">
                                                                        <tbody>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Description</td>
                                                                                <td>:  {{ $data->project_potential }}</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <h4 class="m-t-0 header-title"><b>8. Project Scope</b></h4>
                                                                    <table class="table table-light">
                                                                        <tbody>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Project in scope</td>
                                                                                <td>:  {{ $data->in_scope }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Project out scope</td>
                                                                                <td>:  {{ $data->out_scope }}</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                            <div class="row" style="padding-top: 20px;">
                                                                <div class="col-md-6">
                                                                    <h4 class="m-t-0 header-title"><b>9. Project Key Deliverables</b></h4>
                                                                    <table class="table table-light">
                                                                        <tbody>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Key deliverables</td>
                                                                                <td>:  {{ $data->deliver }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Desciption</td>
                                                                                <td>:  {{ $data->deliver_descr }}</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <h4 class="m-t-0 header-title"><b>10. Project Milestones</b></h4>
                                                                    <table class="table table-light">
                                                                        <tbody>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Project start date</td>
                                                                                <td>:  {{ $data->start_date }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Project end date</td>
                                                                                <td>:  {{ $data->end_date }}</td>
                                                                            </tr>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Key milestones</td>
                                                                                <td>:  {{ $data->milestones }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Milestones details</td>
                                                                                <td>:  {{ $data->milestones_descr }}</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                            <div class="row" style="padding-top: 20px;">
                                                                <div class="col-md-6">
                                                                    <h4 class="m-t-0 header-title"><b>11. Project Constraint</b></h4>
                                                                    <table class="table table-light">
                                                                        <tbody>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Description</td>
                                                                                <td>:  {{ $data->constraint }}</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <h4 class="m-t-0 header-title"><b>12. Project Implementations Impact</b></h4>
                                                                    <table class="table table-light">
                                                                        <tbody>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Description</td>
                                                                                <td>:  {{ $data->impact=='etc'?$data->impact_etc: $data->impact}}</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                            <div class="row" style="padding-top: 20px;">
                                                                <div class="col-md-6">
                                                                    <h4 class="m-t-0 header-title"><b>13. Project Dependencies</b></h4>
                                                                    <table class="table table-light">
                                                                        <tbody>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Internal</td>
                                                                                <td>:  {{ $data->internal }}</td>
                                                                            </tr>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>External</td>
                                                                                <td>:  {{ $data->external }}</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <h4 class="m-t-0 header-title"><b>14. Project Specifications</b></h4>
                                                                    <table class="table table-light">
                                                                        <tbody>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Civil & Structure</td>
                                                                                <td>:  {{ $data->structure=='Other Work'?$data->structure_etc:$data->structure}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Mechanical</td>
                                                                                <td>:  {{ $data->mechanical=='Other Work'?$data->mechanical_etc:$data->mechanical}}</td>
                                                                            </tr>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Electrical</td>
                                                                                <td>:  {{ $data->electrical=='Other Work'?$data->electrical_etc:$data->electrical}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Design & Build</td>
                                                                                <td>:  {{ $data->design }}</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                            <div class="row" style="padding-top: 20px;">
                                                                <div class="col-md-6">
                                                                    <h4 class="m-t-0 header-title"><b>15. Project Cost Estimate</b></h4>
                                                                    <table class="table table-light">
                                                                        <tbody>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Description</td>
                                                                                <td>:  $ {{ number_format($data->cost, 3) }}</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <h4 class="m-t-0 header-title"><b>16. PROJECT SPECIFICATIONS</b></h4>
                                                                    <table class="table table-light">
                                                                        <tbody>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Project Preliminary Design </td>
                                                                                <td>:  
                                                                                    <a href="#" title="Preview" data-id="{{ $data->id }}" data-act="deactivate" data-toggle="modal" data-target="#view">
                                                                                        <img src="{{ asset('assets/images/pir/'.$data->preliminary_design) }}" width="150" alt="" style="background-position: center top; background-repeat: no-repeat; background-size: 50px;">
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Project Preliminary Specifications</td>
                                                                                <td>:  {{ $data->preliminary_spec }}</td>
                                                                            </tr>
                                                                            <tr style="background-color: #e6ffd7">
                                                                                <td>Project Time Frame</td>
                                                                                <td>:  {{ $data->time_frame_start }} / {{ $data->time_frame_end }} </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="portlet" style="box-shadow: 2px 3px #8888884a;">
                                                    <div class="portlet-heading bg-inverse">
                                                        <h3 class="portlet-title">
                                                            VALUE EVALUATIONS 
                                                        </h3>
                                                        <div class="portlet-widgets">
                                                            {{-- <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                                            <span class="divider"></span> --}}
                                                            <a data-toggle="collapse" data-parent="#accordion1" href="#bg-inverse1"><i class="ion-minus-round"></i></a>
                                                            <span class="divider"></span>
                                                            {{-- <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a> --}}
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div id="bg-inverse1" class="panel-collapse collapse in">
                                                        <div class="portlet-body">
                                                            <center>
                                                                <b>Total Value Evaluation : {{ $evaluation['totalVal'] }}</b><br><br>
                                                                <?= $evaluation['catValue'] ?>
                                                            </center>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="portlet" style="box-shadow: 2px 3px #8888884a;">
                                                    <div class="portlet-heading bg-inverse">
                                                        <h3 class="portlet-title">
                                                            RISK EVALUATIONS 
                                                        </h3>
                                                        <div class="portlet-widgets">
                                                            {{-- <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                                            <span class="divider"></span> --}}
                                                            <a data-toggle="collapse" data-parent="#accordion1" href="#bg-inverse2"><i class="ion-minus-round"></i></a>
                                                            <span class="divider"></span>
                                                            {{-- <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a> --}}
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div id="bg-inverse2" class="panel-collapse collapse in">
                                                        <div class="portlet-body">
                                                            <center>
                                                                <b>Total Risk Evaluation : {{ $evaluation['totalRisk'] }}</b><br><br>
                                                                <?= $evaluation['catRisk'] ?>
                                                            </center>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>    
                                        
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="portlet" style="box-shadow: 2px 3px #8888884a;">
                                                    <div class="portlet-heading bg-inverse">
                                                        <h3 class="portlet-title">
                                                            Summary Information Project
                                                        </h3>
                                                        <div class="portlet-widgets">
                                                            {{-- <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                                            <span class="divider"></span> --}}
                                                            <a data-toggle="collapse" data-parent="#accordion1" href="#bg-inverse5"><i class="ion-minus-round"></i></a>
                                                            <span class="divider"></span>
                                                            {{-- <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a> --}}
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div id="bg-inverse5" class="panel-collapse collapse in">
                                                        <div class="portlet-body">
                                                            <center>
                                                                <table class="table table-light">
                                                                    <thead>
                                                                        <th style="width: 50%"></th>
                                                                        <th style="width: 50%"></th>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="text-align: right"><b>Project Name</b></td>
                                                                            <td>{{ $data->descr }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="text-align: right"><b>Project Value Evaluations Result</b></td>
                                                                            <td>{{ $evaluation['totalVal'] }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="text-align: right"><b>Project Risk Evaluations Result</b></td>
                                                                            <td>{{ $evaluation['totalRisk'] }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="text-align: right"><b>Project Approval Path</b></td>
                                                                            <td>{{ $evaluation['path'] }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="text-align: right"><b>Project Rating</b></td>
                                                                            <td><?= $evaluation['matrix'] ?></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </center>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="padding-top: 47px">
                                            <div class="form-group pull-right">
                                                <div class="col-sm-12" style="padding-right: 40px;">
                                                    {{-- <a href="{{ url('pir') }}" class="btn btn-default waves-effect waves-light">Prev</a> --}}
                                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Finish</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="view" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog"> 
                        <div class="modal-content"> 
                            <div class="modal-header"> 
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                <h4 class="modal-title">Image Detail</h4> 
                            </div> 
                            <div class="modal-body" id="modal-image">
                                
                            </div> 
                        </div> 
                    </div>
                </div>

                <script>
                    $(document).ready(function () {
                        $('#view').on('show.bs.modal', function (event) {
                            var button = $(event.relatedTarget) 
                            var id = button.data('id')
                            var modal = $(this)
                            modal.find('.modal-body #id').val(id)
                            $.ajax({
                                type: "get",
                                url: "{{url('pir/view')}}"+'/'+id,
                                dataType: "json",
                                success: function (response) {
                                    var html = '<img src="{{ asset("assets/images/pir") }}/'+response[0].preliminary_design+'" alt="'+response[0].preliminary_design+'" style="width: 100%;">';
                                    $('#modal-image').html(html);
                                }
                            });
                        });
                    });
                </script>

                <script>
                    $(document).ready(function () {
                        $(".select2").select2({
                            placeholder: 'Select Answer',
                            allowClear: true
                        });
                    });
                </script>
@endsection