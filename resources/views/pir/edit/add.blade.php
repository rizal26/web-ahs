@extends('templates.main')

@section('content')
<style>
    hr {
        margin-top: 0px;
        margin-bottom: 15px;
        border-top: 2px solid #979797;
    }
</style>
<style>
.container-img {
  position: relative;
  width: 25%;
}

.image {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.container-img:hover .image {
  opacity: 0.3;
}

.container-img:hover .middle {
  opacity: 1;
}

</style>
                <!-- Forms -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-md-12">
                                    {{-- <h4 class="m-t-0 header-title"><b>Create PIR</b></h4>
                                    <p class="text-muted m-b-30 font-13">
                                        Form PIR Creation
                                    </p> --}}

                                    <div class="row" style="padding-bottom: 40px">
                                        <div class="container">   
                                            <div class="row bs-wizard" style="border-bottom:0;">
                                                <div class="col-xs-3 three-step bs-wizard-step active">
                                                    <div class="text-center bs-wizard-stepnum">Step 1</div>
                                                    <div class="progress"><div class="progress-bar"></div></div>
                                                    <a href="#" class="bs-wizard-dot"></a>
                                                    <div class="bs-wizard-info text-center">Initiations / Idea Generations </div>
                                                </div>
                                                <div class="col-xs-3 three-step bs-wizard-step disabled">
                                                    <div class="text-center bs-wizard-stepnum">Step 2</div>
                                                    <div class="progress"><div class="progress-bar"></div></div>
                                                    <a href="#" class="bs-wizard-dot"></a>
                                                    <div class="bs-wizard-info text-center">Value Evaluations</div>
                                                </div>
                                                <div class="col-xs-3 three-step bs-wizard-step disabled">
                                                    <div class="text-center bs-wizard-stepnum">Step 3</div>
                                                    <div class="progress"><div class="progress-bar"></div></div>
                                                    <a href="#" class="bs-wizard-dot"></a>
                                                    <div class="bs-wizard-info text-center">Risk Evaluations</div>
                                                </div>
                                                <div class="col-xs-3 three-step bs-wizard-step disabled">
                                                    <div class="text-center bs-wizard-stepnum">Step 4</div>
                                                    <div class="progress"><div class="progress-bar"></div></div>
                                                    <a href="#" class="bs-wizard-dot"></a>
                                                    <div class="bs-wizard-info text-center">Resume Path / Finish</div>
                                                </div>
                                            </div>    
                                        </div>
                                    </div>

                                    @if($errors->any())
                                        @foreach($errors->all() as $error)
                                            @if($errors->has('success'))
                                                <div class="alert alert-success">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @else
                                                <div class="alert alert-danger">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @endif    
                                        @endforeach
                                    @endif

                                    <form method="post" onsubmit="return confirm('Are you sure?\nyou will not be able to go back!')" action="{{route('pir.edit', Crypt::encrypt($pir->id))}}" class="form-horizontal" role="form" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <div class="row" style="background-color: #e6ffd7">
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;">1. PROJECT DETAILS</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Name</label>
                                                    <div class="col-md-9">
                                                    <input type="text" class="form-control" name="descr" placeholder="Project Name" value="{{$pir->descr}}" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Category</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2-category" id="category" name="category" required>
                                                            <option></option>
                                                            @php
                                                                $cat = array('Productions', 'Regulations', 'Innovations & Improvement', 'Sustainability');
                                                            @endphp
                                                            @foreach ($cat as $item)
                                                            <option {{ $pir->category==$item?'selected':'' }}>{{$item}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Location</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="location" placeholder="Project location" value="{{ $pir->location }}" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Type</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2-type" id="type" name="type" required>
                                                            <option></option>
                                                            @php
                                                                $type = array('New Project', 'Typical Existing');
                                                            @endphp
                                                            @foreach ($type as $item)
                                                                <option {{ $pir->type==$item?'selected':'' }}>{{ $item }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;">2. PROJECT OWNERSHIP</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Department</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2-department" id="department" name="department" required>
                                                            <option></option>
                                                            @foreach ($department as $item)
                                                            <option value="{{ $item->id }}" {{$pir->department==$item->id?'selected':''}}>{{$item->descr}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Sponsor</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="sponsor" placeholder="Project Sponsor" value="{{ $pir->sponsor }}" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Owner</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="owner" placeholder="Project Owner" value="{{$pir->owner}}" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Manager</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="manager" placeholder="Project Manager" value="{{$pir->manager}}" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;">3. PROJECT REQUIREMENTS</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label for="mandatory" class="col-md-5 control-label">Is this project mandatory ? </label>
                                                    <div class="col-sm-1" style="padding-top: 2px;"></div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="man_yes" value="1" name="mandatory" {{$pir->mandatory==1?'checked':''}} required>
                                                            <label for="man_yes"> Yes </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="man_no" value="0" name="mandatory" {{$pir->mandatory==0?'checked':''}} >
                                                            <label for="man_no"> No </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="urgent" class="col-md-5 control-label">Is this project urgent ? </label>
                                                    <div class="col-sm-1" style="padding-top: 2px;"></div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="urg_yes" value="1" name="urgent" {{$pir->urgent==1?'checked':''}}  required>
                                                            <label for="urg_yes"> Yes </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="urg_normal" value="0" name="urgent" {{$pir->urgent==0?'checked':''}} >
                                                            <label for="urg_normal"> Normal </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="connect" class="col-md-5 control-label">Is this project connect to other project ? </label>
                                                    <div class="col-sm-1" style="padding-top: 2px;"></div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="conn_yes" value="1" name="connect" {{$pir->connect==1?'checked':''}} required>
                                                            <label for="conn_yes"> Yes </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="conn_no" value="0" name="connect" {{$pir->connect==0?'checked':''}}>
                                                            <label for="conn_no"> No </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-5 control-label">What's project reason ?</label>
                                                    <div class="col-md-7">
                                                        <select class="form-control select2-reason" id="reason" name="reason" required>
                                                            <option></option>
                                                            @php
                                                                $reason = array('Productions', 'Regulations', 'Innovations & Improvement', 'Sustainability');
                                                            @endphp
                                                            @foreach ($reason as $item)
                                                            <option {{ $pir->reason==$item?'selected':'' }}>{{$item}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-5 control-label">Reason Description</label>
                                                    <div class="col-md-7">
                                                        <textarea class="form-control" name="reason_descr" rows="5" placeholder="Reason Description" required>{{$pir->reason_descr}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">4. Project Background & Objective</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control" name="project_background" rows="5" placeholder="Project Background & Objective Description" required>{{$pir->project_background}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="background-color: #e6ffd7">
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">5. Project Descriptions</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control" name="project_descr" rows="5" placeholder="Project Description" required>{{$pir->project_descr}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">6. Project Budget</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label for="expenditure" class="col-md-5 control-label">Expenditure type ?</label>
                                                    <div class="col-sm-1" style="padding-top: 2px;"></div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="expn_yes" value="1" name="expenditure" {{$pir->expenditure==1?'checked':''}} required>
                                                            <label for="expn_yes"> Capex </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="expn_no" value="0" name="expenditure" {{$pir->expenditure==0?'checked':''}}>
                                                            <label for="expn_no"> Opex </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="annual_budget" class="col-md-5 control-label">Includeed in annual budget ?</label>
                                                    <div class="col-sm-1" style="padding-top: 2px;"></div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="budget_yes" value="1" name="annual_budget" {{$pir->annual_budget==1?'checked':''}} required>
                                                            <label for="budget_yes"> Yes </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2" style="padding-top: 2px;">
                                                        <div class="radio radio-inline">
                                                            <input type="radio" id="budget_no" value="0" name="annual_budget" {{$pir->annual_budget==0?'checked':''}} >
                                                            <label for="budget_no"> No </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-5 control-label">Budget Estimate (USD) ?</label>
                                                    <div class="col-md-7">
                                                        <input type="text" placeholder="Budget Estimate (USD)" value="{{ $pir->estimate_budget }}" data-a-sign="USD. " class="form-control autonumber" name="estimate_budget" id="estimate_budget" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">7. Project Potentials Alternative </span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control" name="project_potential" rows="5" placeholder="Project Potentials" required>{{ $pir->project_potential }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">8. Project Scope</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Project in scope</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Project in scope" value="{{ $pir->in_scope }}" class="form-control" name="in_scope" id="in_scope" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Project Out scope</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Project out scope" value="{{ $pir->out_scope }}" class="form-control" name="out_scope" id="out_scope" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="background-color: #e6ffd7">
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">9. Project Key Deliverables</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Key Deliverables</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Key Deliverables" value="{{$pir->deliver }}" class="form-control" name="deliver" id="deliver" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control" name="deliver_descr" rows="5" placeholder="Key Deliverables Description" required>{{$pir->deliver_descr}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">10. Project Milestones</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Start Date</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" placeholder="dd-Mmm-yyyy" value="{{$pir->start_date}}" id="datepicker" name="start_date" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">End Date</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" placeholder="dd-Mmm-yyyy" value="{{$pir->end_date}}" id="datepicker-end" name="end_date" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Key Milestones</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Key Milestones" class="form-control" value="{{$pir->milestones}}" name="milestones" id="milestones" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Milestones Details</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Milestones Details" class="form-control" value="{{$pir->milestones_descr}}" name="milestones_descr" id="milestones_descr" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">11. Project Constraint </span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control" name="constraint" rows="5" placeholder="Project Constraint" required>{{$pir->constraint}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">12. Project Implementations Impact </span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Implementations Impact</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2-impact" id="impact" name="impact" required>
                                                            <option></option>
                                                            @php
                                                                $impact = array('Productions', 'Regulations', 'Cost Efficiency', 'Sustainability', 'etc');
                                                            @endphp
                                                            @foreach ($impact as $item)
                                                                <option {{$pir->impact==$item?'selected':''}}>{{$item}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form-impact-etc" {{$pir->impact=='etc'?'':'hidden'}}>
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Description Implementations Impact" value="{{$pir->impact_etc}}" class="form-control" name="impact_etc" id="impact_etc">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="background-color: #e6ffd7">
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">13. Project Dependencies </span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Internal</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Description" class="form-control" value="{{$pir->internal}}" name="internal" id="internal" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">External</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Description" class="form-control" value="{{$pir->external}}" name="external" id="external" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">14. Project Specifications </span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Civil & Structure</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2-structure" id="structure" name="structure" required>
                                                            <option></option>
                                                            @php
                                                                $structure = array(
                                                                    'Building / Warehouse',
                                                                    'Bridge & Road',
                                                                    'Fix Plant',
                                                                    'Jetty / Harbour',
                                                                    'Earthwork',
                                                                    'Support Facilities',
                                                                    'Other Work'
                                                                );
                                                            @endphp
                                                            @foreach ($structure as $item)
                                                            <option {{$pir->structure==$item?'selected':''}}>{{$item}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form-structure-etc" {{ $pir->structure=='Other Work'?'':'hidden' }}>
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Description Civil & Structure" value="{{$pir->structure_etc}}" class="form-control" name="structure_etc" id="structure_etc">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Mechanical</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2-mechanical" id="mechanical" name="mechanical" required>
                                                            <option></option>
                                                            @php
                                                                $mech = array(
                                                                    'Hydrant / Water Pump',
                                                                    'Pipe work',
                                                                    'Fix Plant',
                                                                    'Metal Catcher / Magnetic Separator',
                                                                    'Fuel Tank / Station',
                                                                    'Maintenance / Modifications work',
                                                                    'Motor Installations',
                                                                    'Other Work'
                                                                );
                                                            @endphp
                                                            @foreach ($mech as $item)
                                                                <option {{$pir->mechanical==$item?'selected':''}}>{{$item}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form-mechanical-etc" {{$pir->mechanical=='Other Work'?'':'hidden'}}>
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Description Mechanical" class="form-control" value="{{$pir->mechanical_etc}}" name="mechanical_etc" id="mechanical_etc">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Electrical</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2-electrical" id="electrical" name="electrical" required>
                                                            <option></option>
                                                            @php
                                                                $elec = array(
                                                                    'Genset',
                                                                    'CCTV',
                                                                    'Scada',
                                                                    'Power House',
                                                                    'Other Work'
                                                                );
                                                            @endphp
                                                            @foreach ($elec as $item)
                                                                <option {{$pir->electrical==$item?'selected':''}}>{{$item}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form-electrical-etc" {{$pir->electrical=='Other Work'?'':'hidden'}}>
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Description Electrical" value="{{ $pir->electrical_etc }}" class="form-control" name="electrical_etc" id="electrical_etc">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Design & Build</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select2-design" id="design" name="design" required>
                                                            <option></option>
                                                            @php
                                                                $design = array('Fix Plant', 'Other Work');
                                                            @endphp
                                                            @foreach ($design as $item)
                                                            <option {{$pir->design==$item?'selected':''}}>{{ $item }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">15. PROJECT COST</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Project Cost Estimate</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Project Cost Estimate" data-a-sign="USD. " value="{{$pir->cost}}" class="form-control autonumber" name="cost" id="cost" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <span style="font-size: 15px; font-weight: bold; line-height: 2;" class="text-uppercase">16. PROJECT SPECIFICATIONS</span>
                                                <hr>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Preliminary Design</label>
                                                    <div class="col-sm-9">
                                                        <input name="preliminary_design" type="file" accept="image/*" class="form-control" id="preliminary_design" placeholder="Foto" disabled>
                                                        <p  style="line-height: 2; font-size: 10px; margin-bottom: 1px;">*image max 2 MB</p>
                                                        <div class="container-img" style="padding-top: 10px">
                                                            <img src="{{ asset('assets/images/pir/'.$pir->preliminary_design) }}"  class="image">
                                                            <div class="middle">
                                                                <a href="javascript::void(0)" id="close" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Preliminary Specifications</label>
                                                    <div class="col-md-9">
                                                        <input type="text" placeholder="Preliminary Specifications" class="form-control" value="{{$pir->preliminary_spec}}" name="preliminary_spec" id="preliminary_spec" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Time Frame</label>
                                                    <div class="col-sm-9">
                                                        <div class="input-daterange input-group" id="date-range">
                                                            <input type="text" placeholder="dd-Mmm-yyyy" class="form-control" value="{{$pir->time_frame_start}}" name="time_frame_start" required>
                                                            <span class="input-group-addon bg-custom b-0 text-white">to</span>
                                                            <input type="text" placeholder="dd-Mmm-yyyy" class="form-control" value="{{$pir->time_frame_end}}" name="time_frame_end" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="padding-top: 47px">
                                            <div class="form-group pull-right">
                                                <div class="col-sm-12" style="padding-right: 40px;">
                                                    <a href="{{ url('pir') }}" class="btn btn-default waves-effect waves-light">Close</a>
                                                    <button type="submit" id="primary-alert" class="btn btn-inverse waves-effect waves-light">Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script src="{{asset('assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js')}}" type="text/javascript"></script>
                <script src="{{asset('assets/plugins/autoNumeric/autoNumeric.js')}}" type="text/javascript"></script>
                
                <script>
                    $(document).ready(function() {       
                        $('#preliminary_design').bind('change', function() {
                            var ukuran = (this.files[0].size);
                            var allow = ['JPG', 'JPEG', 'PNG'];
                            var value = $(this).val();
                            var type = value.split('.');
                            last =  function(array, n) {
                                if (array == null) 
                                    return void 0;
                                if (n == null) 
                                    return array[array.length - 1];
                                return array.slice(Math.max(array.length - n, 0));  
                            };
                            // console.log(last(type));
                            
                            if ($.inArray(last(type).toUpperCase(), allow) == -1) {
                                // console.log('tidak ada');
                                alert('Wrong file type\nOnly allowed extension jpg, jpeg, png');
                                return $(this).val('');
                            } 
                            // console.log(type);
                            if(ukuran > 2000000) {
                                alert('File size too large !\nMax 2 Mb');
                                return $(this).val('');
                            };
                        });
                    });
                </script>

                <script>
                    $( "#close" ).click(function() {
                        $( ".container-img" ).fadeOut( "slow" );
                        $("#preliminary_design").removeAttr("disabled");
                    });
                </script>
                <script type="text/javascript">
                    jQuery(function($) {
                        $('.autonumber').autoNumeric('init');    
                        $('#datepicker').datepicker({
                            format: 'dd-M-yyyy'
                        });
                        $('#datepicker-end').datepicker({
                            format: 'dd-M-yyyy'
                        });
                        $('#date-range').datepicker({
                            toggleActive: true,
                            format: 'dd-M-yyyy'
                        });
                    });
                </script>

                <script type="text/javascript">
                    $(document).ready(function () {
                        $(".select2-category").select2({
                            placeholder: 'Select Category',
                            allowClear: true
                        });

                        $(".select2-type").select2({
                            placeholder: 'Select Type',
                            allowClear: true
                        });

                        $(".select2-department").select2({
                            placeholder: 'Select Department',
                            allowClear: true,
                        });  

                        $(".select2-reason").select2({
                            placeholder: 'Select Reason',
                            allowClear: true
                        });

                        $(".select2-impact").select2({
                            placeholder: 'Select Implementations Impact',
                            allowClear: true
                        });

                        $('.select2-impact').change(function (e) { 
                            e.preventDefault();
                            if ($(this).val() == 'etc') {
                                $('#form-impact-etc').show();
                                $('#impact_etc').attr('required', '');
                            } else {
                                $('#form-impact-etc').hide();
                                $('#impact_etc').removeAttr('required');
                                $('#impact_etc').val('');
                            }
                        });

                        $(".select2-structure").select2({
                            placeholder: 'Select Civil & Structure',
                            allowClear: true
                        });

                        $('.select2-structure').change(function (e) { 
                            e.preventDefault();
                            if ($(this).val() == 'Other Work') {
                                $('#form-structure-etc').show();
                                $('#structure_etc').attr('required', '');
                            } else {
                                $('#form-structure-etc').hide();
                                $('#structure_etc').removeAttr('required');
                                $('#structure_etc').val('');
                            }
                        });

                        $(".select2-mechanical").select2({
                            placeholder: 'Select Mechanical',
                            allowClear: true
                        });

                        $('.select2-mechanical').change(function (e) { 
                            e.preventDefault();
                            if ($(this).val() == 'Other Work') {
                                $('#form-mechanical-etc').show();
                                $('#mechanical_etc').attr('required', '');
                            } else {
                                $('#form-mechanical-etc').hide();
                                $('#mechanical_etc').removeAttr('required');
                                $('#mechanical_etc').val('');
                            }
                        });

                        $(".select2-electrical").select2({
                            placeholder: 'Select Electrical',
                            allowClear: true
                        });

                        $('.select2-electrical').change(function (e) { 
                            e.preventDefault();
                            if ($(this).val() == 'Other Work') {
                                $('#form-electrical-etc').show();
                                $('#electrical_etc').attr('required', '');
                            } else {
                                $('#form-electrical-etc').hide();
                                $('#electrical_etc').removeAttr('required');
                                $('#electrical_etc').val('');
                            }
                        });
                    });
                </script>
                
@endsection