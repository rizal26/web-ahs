@extends('templates.main')

@section('content')

<style>
    .widget-bg-color-icon .bg-icon i {
        font-size: 32px;
        line-height: 63px;
    }

    .widget-bg-color-icon .bg-icon {
        height: 63px;
        width: 63px;
        text-align: center;
        -webkit-border-radius: 50%;
        border-radius: 50%;
        -moz-border-radius: 50%;
        background-clip: padding-box; 
    }
/* 
    .modal-dialog{
        overflow-y: initial !important
    } */
    .detail-info{
        /* height: 440px; */
        max-height: 67vh;
        overflow-x: hidden;
        /* width: 867px; */
    }
</style>    

            @if (Request::segment(2) != 'search')
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box widget-inline">
                            <div class="row">
                                <div class="col-sm-2 col-md-2">
                                    <div class="widget-inline-box text-center">
                                        <i class="fa fa-users text-default"></i><h3>{{$param['all']}}</h3>
                                        <h4 class="text-muted">Total</h4>
                                    </div>
                                </div>
                                
                                <div class="col-sm-2 col-md-2">
                                    <div class="widget-inline-box text-center">
                                        <i class="fa fa-asterisk text-info"></i><h3>{{$param['new']}}</h3>
                                        <h4 class="text-muted">New</h4>
                                    </div>
                                </div>
                                
                                <div class="col-sm-2 col-md-2">
                                    <div class="widget-inline-box text-center">
                                        <i class="fa fa-check text-success"></i><h3>{{$param['approve']}}</h3>
                                        <h4 class="text-muted">Approved</h4>
                                    </div>
                                </div>

                                <div class="col-sm-2 col-md-2">
                                    <div class="widget-inline-box text-center">
                                        <i class="fa fa-repeat text-purple"></i><h3>{{$param['resubmit']}}</h3>
                                        <h4 class="text-muted">Resubmitted</h4>
                                    </div>
                                </div>

                                <div class="col-sm-2 col-md-2">
                                    <div class="widget-inline-box text-center">
                                        <i class="fa fa-hourglass-2 text-warning"></i><h3>{{$param['postpon']}}</h3>
                                        <h4 class="text-muted">Postponed</h4>
                                    </div>
                                </div>

                                <div class="col-sm-2 col-md-2">
                                    <div class="widget-inline-box text-center">
                                        <i class="fa fa-times text-danger"></i><h3>{{$param['reject']}}</h3>
                                        <h4 class="text-muted">Rejected</h4>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            @endif
              <!-- Page-Title -->
              <div class="row">
                    <div class="col-sm-12">

                        <div class="btn-group pull-right m-t-15">
                            <a href="{{ url('pir/add') }}" class="btn btn-default dropdown-toggle waves-effect">Add New<span class="m-l-5"><i class="fa fa-plus"></i></span></a>
                        </div>

                        <h4 class="page-title">PIR Management</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="#">Project List & Summery</a>
                            </li>
                        </ol>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <!-- <h4 class="m-t-0 header-title"><b>List of User</b></h4> -->
                            <p class="text-muted font-13 m-b-20"></p>
                            @if($errors->any())
                                @foreach($errors->all() as $error)
                                    @if($errors->has('success'))
                                        <div class="alert alert-success">
                                            <strong>{{ $error }}</strong>
                                        </div>
                                    @else
                                        <div class="alert alert-danger">
                                            <strong>{{ $error }}</strong>
                                        </div>
                                    @endif    
                                @endforeach
                            @endif
                            @if (isset($value))
                                <p style="position: absolute; top: 11px;"><b>Result for "{{ $value }}"</b></p>
                            @endif
                            <table id="datatable" class="table table-striped table-bordered nowrap" colspan="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Status</th>
                                        <th>Action</th>
                                        <th>Project Name</th>
                                        <th>Type</th>
                                        <th>Duration</th>
                                        <th>Cost</th>
                                        <th>Impact</th>
                                        <th>Constraint</th>
                                        <th>Approval Path</th>
                                        <th>Risk Severity Rating</th>
                                        <th>Issue</th>
                                        <th>Submitted By</th>
                                        <th>Executed By</th>
                                        <th>Created Date</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

                <div id="del" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog"> 
                        <div class="modal-content"> 
                            <form method="post" action="{{route('pir.del','test')}}">
                                {{method_field('delete')}} {{csrf_field()}}
                                <div class="modal-header"> 
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                    <h4 class="modal-title">Delete Confirmation</h4> 
                                </div> 
                                <div class="modal-body">
                                    <input type="hidden" name="id" id="id" value="">
                                    <input type="hidden" name="act" id="act" value="">
                                    <p>Are you sure want to delete of this record ?</p>
                                </div> 
                                <div class="modal-footer"> 
                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Yes</button>
                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">No</button> 
                                </div> 
                            </form>
                        </div> 
                    </div>
                </div><!-- /.modal -->  
                
                <div id="detail" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-lg"> 
                        <div class="modal-content" style="max-height: 90vh;"> 
                            <div class="modal-header"> 
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                <h4 class="modal-title">Detail Information</h4> 
                            </div> 
                            <div class="modal-body detail-info">
                                <div id="loading"></div>
                                <div class="col-lg-6 reject_reason">
                                    
                                </div>
                                <div class="col-lg-12"> 
                                    <ul class="nav nav-tabs navtab-bg nav-justified"> 
                                        <li class="active"> 
                                            <a href="#home1" data-toggle="tab" aria-expanded="false"> 
                                                <span class="visible-xs"><i class="fa fa-home"></i></span> 
                                                <span class="hidden-xs">INITIATIONS</span> 
                                            </a> 
                                        </li> 
                                        <li class=""> 
                                            <a href="#settings1" data-toggle="tab" aria-expanded="false"> 
                                                <span class="visible-xs"><i class="fa fa-cog"></i></span> 
                                                <span class="hidden-xs">SUMMARY PROJECT</span> 
                                            </a> 
                                        </li> 
                                    </ul>
                                    <div class="tab-content"> 
                                        <div class="tab-pane active" id="home1">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h4 class="m-t-0 header-title"><b>1. Project Details</b></h4>
                                                    <table class="table table-light">
                                                        <tbody>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Project Number</td>
                                                                <td class="project-number"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Project Name</td>
                                                                <td class="descr"></td>
                                                            </tr>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Project Category</td>
                                                                <td class="category"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Project Location</td>
                                                                <td class="location"></td>
                                                            </tr>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Project Type</td>
                                                                <td class="type"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-md-6">
                                                    <h4 class="m-t-0 header-title"><b>2. Project Ownership</b></h4>
                                                    <table class="table table-light">
                                                        <tbody>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Department</td>
                                                                <td class="department"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Project Sponsor</td>
                                                                <td class="sponsor"></td>
                                                            </tr>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Project Owner</td>
                                                                <td class="owner"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Project Manager</td>
                                                                <td class="manager"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top: 20px;">
                                                <div class="col-md-6">
                                                    <h4 class="m-t-0 header-title"><b>3. Project Requirements</b></h4>
                                                    <table class="table table-light">
                                                        <tbody>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Is this project mandatory ?</td>
                                                                <td class="mandatory"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Is this project urgent ?</td>
                                                                <td class="urgent"></td>
                                                            </tr>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Is this project connect to other project ?</td>
                                                                <td class="connect"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>What's project reason ?</td>
                                                                <td class="reason"></td>
                                                            </tr>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Reason Description</td>
                                                                <td class="reason-descr"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-md-6">
                                                    <h4 class="m-t-0 header-title"><b>4. Project Background & Objective</b></h4>
                                                    <table class="table table-light">
                                                        <tbody>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Description</td>
                                                                <td class="background"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top: 20px;">
                                                <div class="col-md-6">
                                                    <h4 class="m-t-0 header-title"><b>5. Project Descriptions</b></h4>
                                                    <table class="table table-light">
                                                        <tbody>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Description</td>
                                                                <td class="project-descr"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-md-6">
                                                    <h4 class="m-t-0 header-title"><b>6. Project Budget</b></h4>
                                                    <table class="table table-light">
                                                        <tbody>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Expenditure type ?</td>
                                                                <td class="expenditure"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Project includeed in annual budget ?</td>
                                                                <td class="annual"></td>
                                                            </tr>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Project budget estimate (USD) ?</td>
                                                                <td class="estimate"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top: 20px;">
                                                <div class="col-md-6">
                                                    <h4 class="m-t-0 header-title"><b>7. Project Potentilas Alternative</b></h4>
                                                    <table class="table table-light">
                                                        <tbody>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Description</td>
                                                                <td class="potential"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-md-6">
                                                    <h4 class="m-t-0 header-title"><b>8. Project Scope</b></h4>
                                                    <table class="table table-light">
                                                        <tbody>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Project in scope</td>
                                                                <td class="in-scope"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Project out scope</td>
                                                                <td class="out-scope"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top: 20px;">
                                                <div class="col-md-6">
                                                    <h4 class="m-t-0 header-title"><b>9. Project Key Deliverables</b></h4>
                                                    <table class="table table-light">
                                                        <tbody>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Key deliverables</td>
                                                                <td class="deliver"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Desciption</td>
                                                                <td class="deliver-descr"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-md-6">
                                                    <h4 class="m-t-0 header-title"><b>10. Project Milestones</b></h4>
                                                    <table class="table table-light">
                                                        <tbody>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Project start date</td>
                                                                <td class="start-date"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Project end date</td>
                                                                <td class="end-date"></td>
                                                            </tr>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Key milestones</td>
                                                                <td class="milestones"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Milestones details</td>
                                                                <td class="milestones-descr"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top: 20px;">
                                                <div class="col-md-6">
                                                    <h4 class="m-t-0 header-title"><b>11. Project Constraint</b></h4>
                                                    <table class="table table-light">
                                                        <tbody>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Description</td>
                                                                <td class="constraint"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-md-6">
                                                    <h4 class="m-t-0 header-title"><b>12. Project Implementations Impact</b></h4>
                                                    <table class="table table-light">
                                                        <tbody>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Description</td>
                                                                <td class="impact"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top: 20px;">
                                                <div class="col-md-6">
                                                    <h4 class="m-t-0 header-title"><b>13. Project Dependencies</b></h4>
                                                    <table class="table table-light">
                                                        <tbody>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Internal</td>
                                                                <td class="internal"></td>
                                                            </tr>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>External</td>
                                                                <td class="external"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-md-6">
                                                    <h4 class="m-t-0 header-title"><b>14. Project Specifications</b></h4>
                                                    <table class="table table-light">
                                                        <tbody>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Civil & Structure</td>
                                                                <td class="structure"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Mechanical</td>
                                                                <td class="mechanical"></td>
                                                            </tr>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Electrical</td>
                                                                <td class="electrical"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Design & Build</td>
                                                                <td class="design"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="row" style="padding-top: 20px;">
                                                <div class="col-md-6">
                                                    <h4 class="m-t-0 header-title"><b>15. Project Cost Estimate</b></h4>
                                                    <table class="table table-light">
                                                        <tbody>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Description</td>
                                                                <td class="cost"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-md-6">
                                                    <h4 class="m-t-0 header-title"><b>16. PROJECT SPECIFICATIONS</b></h4>
                                                    <table class="table table-light">
                                                        <tbody>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Project Preliminary Design </td>
                                                                <td class="preliminary_design">
                                                                    {{-- <a href="#" title="Preview" data-id="{{ $data->id }}" data-act="deactivate" data-toggle="modal" data-target="#view">
                                                                        <img src="{{ asset('assets/images/pir/'.$data->preliminary_design) }}" width="150" alt="" style="background-position: center top; background-repeat: no-repeat; background-size: 50px;">
                                                                    </a> --}}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Project Preliminary Specifications</td>
                                                                <td class="spec"></td>
                                                            </tr>
                                                            <tr style="background-color: #e6ffd7">
                                                                <td>Project Time Frame</td>
                                                                <td class="time-frame"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div> 
                                        {{-- <div class="tab-pane active" id="profile1"> 

                                        </div> 
                                        <div class="tab-pane" id="messages1">

                                        </div>  --}}
                                        <div class="tab-pane" id="settings1"> 
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="portlet" style="box-shadow: 2px 3px #8888884a;">
                                                        <div class="portlet-heading bg-inverse">
                                                            <h3 class="portlet-title">
                                                                VALUE EVALUATIONS 
                                                            </h3>
                                                            <div class="portlet-widgets">
                                                                <a data-toggle="collapse" data-parent="#accordion1" href="#bg-inverse1"><i class="ion-minus-round"></i></a>
                                                                <span class="divider"></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div id="bg-inverse1" class="panel-collapse collapse in">
                                                            <div class="portlet-body">
                                                                <center>
                                                                    <b>Total Value Evaluation : <span class="tot-val"></span></b><br><br>
                                                                    <span class="cat-val"></span>
                                                                </center>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="portlet" style="box-shadow: 2px 3px #8888884a;">
                                                        <div class="portlet-heading bg-inverse">
                                                            <h3 class="portlet-title">
                                                                RISK EVALUATIONS 
                                                            </h3>
                                                            <div class="portlet-widgets">
                                                                <a data-toggle="collapse" data-parent="#accordion1" href="#bg-inverse2"><i class="ion-minus-round"></i></a>
                                                                <span class="divider"></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div id="bg-inverse2" class="panel-collapse collapse in">
                                                            <div class="portlet-body">
                                                                <center>
                                                                    <b>Total Risk Evaluation : <span class="tot-risk"></span></b><br><br>
                                                                    <span class="cat-risk"></span>
                                                                </center> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>    
                                            
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="portlet" style="box-shadow: 2px 3px #8888884a;">
                                                        <div class="portlet-heading bg-inverse">
                                                            <h3 class="portlet-title">
                                                                Summary Information Project
                                                            </h3>
                                                            <div class="portlet-widgets">
                                                                <a data-toggle="collapse" data-parent="#accordion1" href="#bg-inverse5"><i class="ion-minus-round"></i></a>
                                                                <span class="divider"></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div id="bg-inverse5" class="panel-collapse collapse in">
                                                            <div class="portlet-body">
                                                                <center>
                                                                    <table class="table table-light">
                                                                        <thead>
                                                                            <th style="width: 50%"></th>
                                                                            <th style="width: 50%"></th>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="text-align: right"><b>Project Name</b></td>
                                                                                <td class="descr-summery"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align: right"><b>Project Value Evaluations Result</b></td>
                                                                                <td class="tot-val-summery"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align: right"><b>Project Risk Evaluations Result</b></td>
                                                                                <td class="tot-risk-summery"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align: right"><b>Project Approval Path</b></td>
                                                                                <td class="path"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align: right"><b>Project Rating</b></td>
                                                                                <td class="matrix"></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </center>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div> 
                                    </div> 
                                </div>
                            </div> 
                            <div class="modal-footer"> 
                                @if (Session::get('role') == 1)
                                <div class="text-center" id="approval">
                                </div>
                                @endif
                                {{-- <a href="#" class="btn btn-danger waves-effect" data-dismiss="modal">Close</a>  --}}
                            </div> 
                        </div> 
                    </div>
                </div><!-- /.modal -->  

                <div id="approve" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog"> 
                        <div class="modal-content"> 
                            <div class="modal-header"> 
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                <h4 class="modal-title">Approval Confirmation</h4> 
                            </div> 
                            <form class="approve-form" method="post" action="{{route('pir.approve')}}">
                                {{csrf_field()}}
                                <div class="modal-body">
                                    <p>Are you sure want to approve this PIR ? </p>
                                    <input id="idapprove" type="hidden" name="idapprove">
                                </div> 
                                <div class="modal-footer"> 
                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Yes</button>
                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">No</button> 
                                </div> 
                            </form>
                        </div> 
                    </div>
                </div>

                <div id="postpone" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog"> 
                        <div class="modal-content"> 
                            <div class="modal-header"> 
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                <h4 class="modal-title">Postpone Confirmation</h4> 
                            </div> 
                            <form class="postpone-form" method="post" action="{{route('pir.postpone')}}">
                                {{csrf_field()}}
                                <div class="modal-body">
                                    <p>Are you sure want to postpone this PIR ?</p>
                                    <input id="idpostpone" type="hidden" name="idpostpone">
                                </div> 
                                <div class="modal-footer"> 
                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Yes</button>
                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">No</button> 
                                </div> 
                            </form>
                        </div> 
                    </div>
                </div>

                <div id="reject" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog"> 
                        <div class="modal-content"> 
                            <div class="modal-header"> 
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                <h4 class="modal-title">Reject Reason</h4> 
                            </div> 
                            <form class="reject-form" method="post" action="{{route('pir.reject')}}">
                                {{csrf_field()}}
                                <div class="modal-body">
                                    <p>Are you sure want to reject this PIR ? Please give a reason first : </p>
                                    <input id="idreject" type="hidden" name="idreject">
                                    <textarea class="form-control" name="reason_reject" required></textarea><br>
                                </div> 
                                <div class="modal-footer"> 
                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Yes</button>
                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">No</button> 
                                </div> 
                            </form>
                        </div> 
                    </div>
                </div>

                <div id="view" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog"> 
                        <div class="modal-content"> 
                            <div class="modal-header"> 
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                <h4 class="modal-title">Image Detail</h4> 
                            </div> 
                            <div class="modal-body" id="modal-image">
                            </div> 
                        </div> 
                    </div>
                </div>

                <script type="text/javascript">
                    $(function() {
                        'use strict';
                        $(document).on('show.bs.modal', '.modal', function (event) {
                            var zIndex = 1040 + (10 * $('.modal:visible').length);
                            $(this).css('z-index', zIndex);
                            setTimeout(function() {
                                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                            }, 0);
                        });

                        $('#del').on('show.bs.modal', function (event) {
                            var button = $(event.relatedTarget) 
                            var id = button.data('id')
                            var modal = $(this)
                            modal.find('.modal-body #id').val(id)
                        });

                        $('#approve').on('show.bs.modal', function (event) {
                            var button = $(event.relatedTarget) 
                            var id = button.data('id')
                            var modal = $(this)
                            modal.find('.modal-body #id').val(id)
                            $('#idapprove').val(id);
                        });

                         $('#postpone').on('show.bs.modal', function (event) {
                            var button = $(event.relatedTarget) 
                            var id = button.data('id')
                            var modal = $(this)
                            modal.find('.modal-body #id').val(id)
                            $('#idpostpone').val(id);
                        });

                         $('#reject').on('show.bs.modal', function (event) {
                            var button = $(event.relatedTarget) 
                            var id = button.data('id')
                            var modal = $(this)
                            modal.find('.modal-body #id').val(id)
                            $('#idreject').val(id);
                        });

                         $('#detail').on('show.bs.modal', function (event) {
                            var button = $(event.relatedTarget) 
                            var id = button.data('id')
                            var modal = $(this)
                            modal.find('.modal-body #id').val(id)
                            $.ajax({
                                type: "get",
                                url: "{{ url('pir/detail') }}"+"/"+id,
                                dataType: "json",
                                beforeSend: function() {
                                    var load = '<center style="font-size: 32px;"><i class="fa fa-spin fa-refresh"></i></center>';
                                    $('#loading').html(load);
                                },
                                success: function (response) {
                                    $('#loading').empty();
                                    console.log(response);
                                    var appr = '<a href="" class="btn btn-success waves-effect waves-light" data-id="'+response.id+'" data-act="deactivate" data-toggle="modal" data-target="#approve">Approve</a>';
                                    var postpone = '<a href="" class="btn btn-warning waves-effect waves-light" data-id="'+response.id+'" data-act="deactivate" data-toggle="modal" data-target="#postpone">Postpone</a>';
                                    var reject = '<a href="" class="btn btn-danger waves-effect waves-light" data-id="'+response.id+'" data-act="deactivate" data-toggle="modal" data-target="#reject">Reject</a>';
                                    if (response.reject_reason != null) {
                                        $('.reject_reason').html('<p><b>Reject Reason : '+response.reject_reason+'</b></p>');
                                    }
                                    $('#approval').html(appr+postpone+reject);
                                    $('.project-number').html(response.id);
                                    $('.descr').html(response.descr);
                                    $('.category').html(response.category);
                                    $('.location').html(response.location);
                                    $('.type').html(response.type);
                                    $('.department').html(response.nm_dept);
                                    $('.sponsor').html(response.sponsor);
                                    $('.owner').html(response.owner);
                                    $('.manager').html(response.manager);
                                    if (response.mandatory == 1) {
                                        var mandatory = 'YES';
                                    } else {
                                        var mandatory = 'NO';
                                    }
                                    $('.mandatory').html(mandatory);
                                    if (response.urgent == 1) {
                                        var urgent = 'YES';
                                    } else {
                                        var urgent = 'NORMAL';
                                    }
                                    $('.urgent').html(urgent);
                                    if (response.connect == 1) {
                                        var connect = 'YES';
                                    } else {
                                        var connect = 'NO';
                                    }
                                    $('.connect').html(connect);
                                    $('.reason').html(response.reason);
                                    $('.reason-descr').html(response.reason_descr);
                                    $('.background').html(response.project_background);
                                    $('.project-descr').html(response.project_descr);
                                    if (response.expenditure == 1) {
                                        var expenditure = 'Capex';
                                    } else {
                                        var expenditure = 'Opex';
                                    }
                                    $('.expenditure').html(expenditure);
                                    if (response.annual_budget == 1) {
                                        var annual = 'YES';
                                    } else {
                                        var annual = 'NO';
                                    }
                                    $('.annual').html(annual);
                                    $('.estimate').html(response.estimate_budget);
                                    $('.potential').html(response.project_potential);
                                    $('.in-scope').html(response.in_scope);
                                    $('.out-scope').html(response.out_scope);
                                    $('.deliver').html(response.deliver);
                                    $('.deliver-descr').html(response.deliver_descr);
                                    $('.start-date').html(response.start_date);
                                    $('.end-date').html(response.end_date);
                                    $('.milestones').html(response.milestones);
                                    $('.milestones-descr').html(response.milestones_descr);
                                    $('.constraint').html(response.constraint);
                                    if (response.impact_etc == null) {
                                        var impact = response.impact;
                                    } else {
                                        var impact = response.impact_etc;
                                    }
                                    $('.impact').html(impact);
                                    $('.internal').html(response.internal);
                                    $('.external').html(response.external);
                                    if (response.structure_etc == null) {
                                        var structure = response.structure;
                                    } else {
                                        var structure = response.structure_etc;
                                    }
                                    $('.structure').html(structure);
                                    if (response.mechanical_etc == null) {
                                        var mechanical = response.mechanical;
                                    } else {
                                        var mechanical = response.mechanical_etc;
                                    }
                                    $('.mechanical').html(mechanical);
                                    if (response.electrical_etc == null) {
                                        var electrical = response.electrical;
                                    } else {
                                        var electrical = response.electrical_etc;
                                    }
                                    $('.electrical').html(electrical);
                                    $('.design').html(response.design);
                                    $('.cost').html(response.cost);
                                    var imgUrl = "{{ asset('assets/images/pir') }}"+"/"+response.preliminary_design;
                                    var preliminary = '<a href="#" title="Preview" data-id="'+ response.id +'" data-act="deactivate" data-toggle="modal" data-target="#view">'+
                                                        '<img src="'+ imgUrl +'" width="150" alt="" style="background-position: center top; background-repeat: no-repeat; background-size: 50px;">'+
                                                    '</a>';
                                    $('.preliminary_design').html(preliminary);
                                    $('.spec').html(response.preliminary_spec);
                                    var timeFrame = response.time_frame_start+" / "+response.time_frame_end;
                                    $('.time-frame').html(timeFrame);
                                    $('.cat-val').html(response.cat_val);
                                    $('.cat-risk').html(response.cat_risk);
                                    $('.descr-summery').html(response.descr);
                                    $('.tot-val').html(response.tot_val);
                                    $('.tot-risk').html(response.tot_risk);
                                    $('.tot-val-summery').html(response.tot_val);
                                    $('.tot-risk-summery').html(response.tot_risk);
                                    $('.path').html(response.rating);
                                    $('.matrix').html(response.path);
                                    // $('#project-number').html(response.project-number);
                                }
                            });
                        });

                        $('#view').on('show.bs.modal', function (event) {
                            var button = $(event.relatedTarget) 
                            var id = button.data('id')
                            var modal = $(this)
                            modal.find('.modal-body #id').val(id)
                            $.ajax({
                                type: "get",
                                url: "{{url('pir/view')}}"+'/'+id,
                                dataType: "json",
                                success: function (response) {
                                    var html = '<img src="{{ asset("assets/images/pir") }}/'+response[0].preliminary_design+'" alt="'+response[0].preliminary_design+'" style="width: 100%;">';
                                    $('#modal-image').html(html);
                                }
                            });
                        });
                        
                        var table = $('#datatable').DataTable({
                            scrollX : true,
                            searchDelay: 1000,
                            // responsive: true,
                            processing: true,
                            language: {
                                searchPlaceholder: 'Search...',
                                sSearch: '',
                                processing: "<i class='fa fa-spin fa-refresh'></i>",
                            },
                            // serverSide: true,
                            // ordering: false,
                            // bLengthChange: false,
                            ajax: {
                                url: "{{url('pir/ajax')}}",
                                type: 'GET',
                                dataSrc: 'data',
                            },
                            columns: [
                                { data: 'status' },
                                // { data: 'id' },
                                { data: 'action' },
                                { data: 'descr' },
                                { data: 'type' },
                                { data: 'duration' },
                                { data: 'cost' },
                                { data: 'impact' },
                                { data: 'constraint' },
                                { data: 'path' },
                                { data: 'rating' },
                                { data: 'issue' },
                                { data: 'submit_by' },
                                { data: 'exec_by' },
                                { data: 'create_date' },
                            ],
                        });

                        var param = '{{ isset($value)?$value:'' }}';
                        table
                            .columns(2)
                            .search(param)
                            .draw(); 
                    });
                </script>  
@endsection