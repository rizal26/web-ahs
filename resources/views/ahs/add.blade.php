@extends('templates.main')
@section('content')

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="m-t-0 header-title"><b>Add AHS</b></h4>
                                    <p class="text-muted m-b-30 font-13">
                                        Form AHS Creation
                                    </p>
                                    @if($errors->any())
                                        @foreach($errors->all() as $error)
                                            @if($errors->has('success'))
                                                <div class="alert alert-success">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @else
                                                <div class="alert alert-danger">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @endif    
                                        @endforeach
                                    @endif
                                    <form method="post" action="{{route('ahs.add')}}" class="form-horizontal m-b-10" role="form">
                                        {{csrf_field()}}
                                        <div id="copy">
                                            <div class="form-group">
                                                <label for="pekerjaan" class="col-sm-2 control-label">Pekerjaan</label>
                                                <div class="col-sm-9">
                                                    <select name="pekerjaan[0][]" class="form-control select2-pekerjaan" id="pekerjaan" required>
                                                        <option></option>
                                                        <option>PEKERJAAN PERSIAPAN</option> 
                                                        <option>MOBILISASI ALAT & MATERIAL</option> 
                                                        <option>PEKERJAAN TANAH DAN PONDASI</option> 
                                                        <option>PEKERJAAN STRUKTUR BETON</option> 
                                                        <option>PEKERJAAN RANGKA ATAP DAN PLAFOND</option> 
                                                        <option>PEKERJAAN KUSEN / PINTU/ JENDELA / VENTILASI</option> 
                                                        <option>PEKERJAAN KUNCI DAN ALAT PENGGANTUNG</option> 
                                                        <option>PEKERJAAN MEKANIKAL/ELEKTRIKAL</option> 
                                                        <option>PEKERJAAN SANITAIR</option> 
                                                        <option>PEKERJAAN CAT-CATAN</option> 
                                                        <option>PEKERJAAN LAIN-LAIN</option> 
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="button" class="col-sm-2 control-label"></label>
                                                <div class="col-sm-9">
                                                    <button type="button" class="btn btn-info add-uraian"><i class="fa fa-plus"></i> Add Uraian</button>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="button" class="col-sm-2 control-label"></label>
                                                <div class="col-sm-9">
                                                    <table class="table table-bordered table-striped" id="datatable">
                                                        <thead class="thead-light">
                                                            <tr>
                                                                <th>Uraian</th>
                                                                <th style="width: 11%">Satuan</th>
                                                                <th style="width: 22%">Harga</th>
                                                                <th style="width: 22%">Remarks</th>
                                                                <th style="width: 5%">#</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="child">
                                                            <tr>
                                                                <td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan[0][uraian][]" required></td>
                                                                <td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan[0][satuan][]" required></td>
                                                                <td><input style="width: 100%; height: 28px;" class="form-control autonumber" data-a-sign="Rp. " type="text" name="pekerjaan[0][harga][]" required></td>
                                                                <td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan[0][remarks][]"></td>
                                                                <td></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <hr style="border-top: 2px solid #868686;">
                                        </div>
                                        <div id="paste">
                                        </div>
                                        <div class="form-group m-b-0">
                                            <div class="col-sm-offset-3 col-sm-9">
                                              <button type="submit" class="btn btn-inverse waves-effect waves-divght">Create</button>
                                              <a href="{{ url('ahs') }}" class="btn btn-default waves-effect waves-light">Back</a>
                                              <button id="addpekerjaan" type="button" class="btn btn-success waves-effect waves-light"><i class="fa fa-plus"></i> Add Pekerjaan</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
            <script>
                $(document).ready(function () {
                    
                    $('.add-uraian').click(function (e) { 
                        e.preventDefault();
                        var input = ''+
                        '<tr>'+
                            '<td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan[0][uraian][]" required></td>'+
                            '<td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan[0][satuan][]" required></td>'+
                            '<td><input style="width: 100%; height: 28px;" class="form-control autonumber" data-a-sign="Rp. " type="text" name="pekerjaan[0][harga][]" required></td>'+
                            '<td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan[0][remarks][]"></td>'+
                            '<td><a href="javascript:void(0)" class="delete" title="remove"><i class="fa fa-times" style="color: red"></i></a></td>'+
                        '</tr>';
                            $('.child').append(input);
                            $('.autonumber').autoNumeric('init'); 
                        });
                        
                    $(document).on("click", ".delete", function(){
                        $(this).parents("tr").remove();
                    });

                    
                    $('#datatable').DataTable({
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": false,
                        "bAutoWidth": false,
                        "searching": false,
                        "ordering": false
                    });                        
                });
            </script>

            <script type="text/javascript">
                $(document).ready(function(){ 
                    var addButton = $("#addpekerjaan");
                    var wrapper = $("#paste");
                    
                    var x = 0;
                    $(addButton).click(function(){
                        x+=1; //Increment field counter
                        var html = '<div class="form-pekerjaan">'+
                            '<div class="form-group">'+
                                '<span class="col-sm-1" style="padding-left: 38px;"><a href="javascript:void(0)" title="Remove Form" id="remove'+x+'"><i class="fa fa-times" style="color:red;font-size: 43px;"></i></a></span>'+
                                '<label for="pekerjaan" class="col-sm-1 control-label">Pekerjaan</label>'+
                                '<div class="col-sm-9">'+
                                    '<select name="pekerjaan['+x+'][]" class="form-control select2-pekerjaan" id="pekerjaan" required>'+
                                        '<option></option>'+
                                        '<option>PEKERJAAN PERSIAPAN</option> '+
                                        '<option>MOBILISASI ALAT & MATERIAL</option> '+
                                        '<option>PEKERJAAN TANAH DAN PONDASI</option> '+
                                        '<option>PEKERJAAN STRUKTUR BETON</option> '+
                                        '<option>PEKERJAAN RANGKA ATAP DAN PLAFOND</option> '+
                                        '<option>PEKERJAAN KUSEN / PINTU/ JENDELA / VENTILASI</option> '+
                                        '<option>PEKERJAAN KUNCI DAN ALAT PENGGANTUNG</option> '+
                                        '<option>PEKERJAAN MEKANIKAL/ELEKTRIKAL</option> '+
                                        '<option>PEKERJAAN SANITAIR</option> '+
                                        '<option>PEKERJAAN CAT-CATAN</option> '+
                                        '<option>PEKERJAAN LAIN-LAIN</option> '+
                                    '</select>'+
                                '</div>'+
                            '</div>'+
                            '<div class="event-form">'+
                            '<div class="form-group">'+
                                '<label for="button" class="col-sm-2 control-label"></label>'+
                                '<div class="col-sm-9">'+
                                    '<button type="button" class="btn btn-info add-uraian-'+x+'"><i class="fa fa-plus"></i> Add Uraian</button>'+
                                '</div>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label for="button" class="col-sm-2 control-label"></label>'+
                                '<div class="col-sm-9">'+
                                    '<table class="table table-bordered table-striped" id="datatable">'+
                                        '<thead class="thead-light">'+
                                            '<tr>'+
                                                '<th>Uraian</th>'+
                                                '<th style="width: 11%">Satuan</th>'+
                                                '<th style="width: 22%">Harga</th>'+
                                                '<th style="width: 22%">Remarks</th>'+
                                                '<th style="width: 5%">#</th>'+
                                            '</tr>'+
                                        '</thead>'+
                                        '<tbody class="child-'+x+'">'+
                                            '<tr>'+
                                                '<td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan['+x+'][uraian][]" required></td>'+
                                                '<td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan['+x+'][satuan][]" required></td>'+
                                                '<td><input style="width: 100%; height: 28px;" class="form-control autonumber" data-a-sign="Rp. " type="text" name="pekerjaan['+x+'][harga][]" required></td>'+
                                                '<td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan['+x+'][remarks][]"></td>'+
                                                '<td></td>'+
                                            '</tr>'+
                                        '</tbody>'+
                                    '</table>'+
                                '</div>'+
                            '</div>'+
                            '</div>'+
                            '<hr style="border-top: 2px solid #868686;">'+
                            '</div>';
                        
                        $(wrapper).append($(html).hide().fadeIn()); //Add field html
                        $(wrapper).on('click', '#remove'+x, function(e){
                            e.preventDefault();
                            $(this).closest('.form-pekerjaan').fadeOut(function() {
                                $(this).remove();
                            }); //Remove field html
                            x--; //Decrement field counter
                        });

                        $('.add-uraian-'+x).click(function (event) { 
                            event.preventDefault();
                            var input = ''+
                            '<tr>'+
                                '<td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan['+x+'][uraian][]" required></td>'+
                                '<td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan['+x+'][satuan][]" required></td>'+
                                '<td><input style="width: 100%; height: 28px;" class="form-control autonumber" data-a-sign="Rp. " type="text" name="pekerjaan['+x+'][harga][]" required></td>'+
                                '<td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan['+x+'][remarks][]"></td>'+
                                '<td><a href="javascript:void(0)" class="delete" title="remove"><i class="fa fa-times" style="color: red"></i></a></td>'+
                            '</tr>';
                                $(event.target).closest('.event-form').find('tbody').append(input);
                                $('.autonumber').autoNumeric('init');
                            });
                            
                        $(document).on("click", ".delete", function(){
                            $(this).parents("tr").remove();
                        });

                        $('.select2-pekerjaan').select2({
                            placeholder: 'Select Pekerjaan',
                            allowClear: true,
                        });
                        $('.autonumber').autoNumeric('init');
                    });
                });
                </script>
                <script>
                $(document).ready(function () {
                    $('.select2-pekerjaan').select2({
                        placeholder: 'Select Pekerjaan',
                        allowClear: true,
                    });
                });
            </script>
                
@endsection