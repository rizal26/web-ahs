@extends('templates.main')
@section('content')

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="m-t-0 header-title"><b>Edit AHS</b></h4>
                                    <p class="text-muted m-b-30 font-13">
                                        Form AHS Update
                                    </p>
                                    @if($errors->any())
                                        @foreach($errors->all() as $error)
                                            @if($errors->has('success'))
                                                <div class="alert alert-success">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @else
                                                <div class="alert alert-danger">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @endif    
                                        @endforeach
                                    @endif
                                    <form method="post" action="{{route('ahs.edit', $ahs->id)}}" class="form-horizontal m-b-10" role="form">
                                        {{csrf_field()}}
                                        <div id="copy">
                                            <div class="form-group">
                                                <label for="pekerjaan" class="col-sm-2 control-label">Pekerjaan</label>
                                                <div class="col-sm-9">
                                                    <select name="pekerjaan[0][]" class="form-control select2-pekerjaan" id="pekerjaan" required>
                                                        <option></option>
                                                        @php
                                                            $arrOpt = array(
                                                                'PEKERJAAN PERSIAPAN',
                                                                'MOBILISASI ALAT & MATERIAL',
                                                                'PEKERJAAN TANAH DAN PONDASI',
                                                                'PEKERJAAN STRUKTUR BETON',
                                                                'PEKERJAAN RANGKA ATAP DAN PLAFOND',
                                                                'PEKERJAAN KUSEN / PINTU/ JENDELA / VENTILASI',
                                                                'PEKERJAAN KUNCI DAN ALAT PENGGANTUNG',
                                                                'PEKERJAAN MEKANIKAL/ELEKTRIKAL',
                                                                'PEKERJAAN SANITAIR',
                                                                'PEKERJAAN CAT-CATAN',
                                                                'PEKERJAAN LAIN-LAIN',
                                                            );
                                                        @endphp
                                                        @foreach ($arrOpt as $item)
                                                            <option {{ $ahs->pekerjaan==$item?'selected':''}}>{{$item}}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="button" class="col-sm-2 control-label"></label>
                                                <div class="col-sm-9">
                                                    <button type="button" class="btn btn-info add-uraian"><i class="fa fa-plus"></i> Add Uraian</button>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="button" class="col-sm-2 control-label"></label>
                                                <div class="col-sm-9">
                                                    <table class="table table-bordered table-striped" id="datatable">
                                                        <thead class="thead-light">
                                                            <tr>
                                                                <th>Uraian</th>
                                                                <th style="width: 11%">Satuan</th>
                                                                <th style="width: 22%">Harga</th>
                                                                <th style="width: 22%">Remarks</th>
                                                                <th style="width: 5%">#</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="child">
                                                            <tr>
                                                                <td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan[0][uraian][]" value="{{$ahs->dataDescr->uraian[0]}}" required></td>
                                                                <td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan[0][satuan][]" value="{{$ahs->dataDescr->satuan[0]}}" required></td>
                                                                <td><input style="width: 100%; height: 28px;" class="form-control autonumber" data-a-sign="Rp. " type="text" name="pekerjaan[0][harga][]" value="{{$ahs->dataDescr->harga[0]}}" required></td>
                                                                <td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan[0][remarks][]" value="{{$ahs->dataDescr->remarks[0]}}"></td>
                                                                <td></td>
                                                            </tr>
                                                            @php
                                                                unset($ahs->dataDescr->uraian[0]);
                                                            @endphp
                                                            @for ($i = 1; $i <= count($ahs->dataDescr->uraian); $i++)
                                                            <tr>
                                                                <td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan[0][uraian][]" value="{{ $ahs->dataDescr->uraian[$i] }}" required></td>
                                                                <td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan[0][satuan][]" value="{{ $ahs->dataDescr->satuan[$i] }}" required></td>
                                                                <td><input style="width: 100%; height: 28px;" class="form-control autonumber" data-a-sign="Rp. " type="text" name="pekerjaan[0][harga][]" value="{{ $ahs->dataDescr->harga[$i] }}" required></td>
                                                                <td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan[0][remarks][]" value="{{ $ahs->dataDescr->remarks[$i] }}"></td>
                                                                <td><a href="javascript:void(0)" class="delete" title="remove"><i class="fa fa-times" style="color: red"></i></a></td>
                                                            </tr>
                                                            @endfor
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <hr style="border-top: 2px solid #868686;">
                                        </div>
                                        <div id="paste">
                                        </div>
                                        <div class="form-group m-b-0">
                                            <div class="col-sm-offset-3 col-sm-9">
                                              <button type="submit" class="btn btn-inverse waves-effect waves-divght">Update</button>
                                              <a href="{{ url('ahs') }}" class="btn btn-default waves-effect waves-light">Back</a>
                                              {{-- <button id="addpekerjaan" type="button" class="btn btn-success waves-effect waves-light"><i class="fa fa-plus"></i> Add Pekerjaan</button> --}}
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
            <script>
                $(document).ready(function () {
                    
                    $('.add-uraian').click(function (e) { 
                        e.preventDefault();
                        var input = ''+
                        '<tr>'+
                            '<td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan[0][uraian][]" required></td>'+
                            '<td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan[0][satuan][]" required></td>'+
                            '<td><input style="width: 100%; height: 28px;" class="form-control autonumber" data-a-sign="Rp. " type="text" name="pekerjaan[0][harga][]" required></td>'+
                            '<td><input style="width: 100%; height: 28px;" class="form-control" type="text" name="pekerjaan[0][remarks][]"></td>'+
                            '<td><a href="javascript:void(0)" class="delete" title="remove"><i class="fa fa-times" style="color: red"></i></a></td>'+
                        '</tr>';
                            $('.child').append(input);
                            $('.autonumber').autoNumeric('init'); 
                        });
                        
                    $(document).on("click", ".delete", function(){
                        $(this).parents("tr").remove();
                    });

                    
                    $('#datatable').DataTable({
                        "bPaginate": false,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bInfo": false,
                        "bAutoWidth": false,
                        "searching": false,
                        "ordering": false
                    });                        
                });
            </script>

            
            <script>
                $(document).ready(function () {
                    $('.select2-pekerjaan').select2({
                        placeholder: 'Select Pekerjaan',
                        allowClear: true,
                    });
                });
            </script>
                
@endsection