@extends('templates.main')

@section('content')
              <!-- Page-Title -->
              <div class="row">
                    <div class="col-sm-12">
                        @if (Session::get('role')==1)
                        <div class="btn-group pull-right m-t-15">
                            <a href="{{ url('ahs/add') }}" class="btn btn-default dropdown-toggle waves-effect">Add New<span class="m-l-5"><i class="fa fa-plus"></i></span></a>
                        </div>
                        @endif

                        <h4 class="page-title">AHS Management</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="#">List AHS</a>
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <!-- <h4 class="m-t-0 header-title"><b>List of User</b></h4> -->
                            <p class="text-muted font-13 m-b-20"></p>
                            @if($errors->any())
                                @foreach($errors->all() as $error)
                                    @if($errors->has('success'))
                                        <div class="alert alert-success">
                                            <strong>{{ $error }}</strong>
                                        </div>
                                    @else
                                        <div class="alert alert-danger">
                                            <strong>{{ $error }}</strong>
                                        </div>
                                    @endif    
                                @endforeach
                            @endif
                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" colspan="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Pekerjaan</th>
                                        {{-- <th>Description</th> --}}
                                        <th>Modified by</th>
                                        <th>Last Updated</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

                <div id="del" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog"> 
                        <div class="modal-content"> 
                            <form method="post" action="{{route('ahs.del','test')}}">
                                {{method_field('delete')}} {{csrf_field()}}
                                <div class="modal-header"> 
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                    <h4 class="modal-title">Delete Confirmation</h4> 
                                </div> 
                                <div class="modal-body">
                                    <input type="hidden" name="id" id="id" value="">
                                    <input type="hidden" name="act" id="act" value="">
                                    <p>Are you sure want to delete of this record ?</p>
                                </div> 
                                <div class="modal-footer"> 
                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Yes</button>
                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">No</button> 
                                </div> 
                            </form>
                        </div> 
                    </div>
                </div><!-- /.modal --> 
                
                <div id="det" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-lg"> 
                        <div class="modal-content "> 
                            <div class="modal-header"> 
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                <h4 class="modal-title">Detail Information - <span id="pekerjaan"></span></h4> 
                            </div> 
                            <div class="modal-body detail-info">
                                <table class="table table-light table-striped">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Uraian</th>
                                            <th style="width: 11%">Satuan</th>
                                            <th style="width: 22%">Harga</th>
                                            <th style="width: 22%">Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody class="tbody">
                                        
                                    </tbody>
                                </table>
                            </div> 
                        </div> 
                    </div>
                </div><!-- /.modal --> 

                <script type="text/javascript">
                    $(function() {
                        'use strict';
                        $('#del').on('show.bs.modal', function (event) {
                            var button = $(event.relatedTarget) 
                            var id = button.data('id')
                            var modal = $(this)
                            modal.find('.modal-body #id').val(id)
                        });
                        $('#det').on('show.bs.modal', function (event) {
                            var button = $(event.relatedTarget) 
                            var id = button.data('id')
                            var modal = $(this)
                            modal.find('.modal-body #id').val(id)
                            $.ajax({
                                type: "get",
                                url: "{{url('/ahs/detail')}}"+'/'+id,
                                // data: "data",
                                // dataType: "dataType",
                                success: function (response) {
                                    $('#pekerjaan').html(response['pekerjaan']);
                                    var html = '';
                                    for (var i = 0; i < response['descr']['uraian'].length; i++) {
                                        // const element = array[index];
                                        if (response['descr']['remarks'][i] == null) {
                                            var remarks = '';
                                        } else {
                                            var remarks = response['descr']['remarks'][i];
                                        }
                                        var format = new Intl.NumberFormat('en-US', { 
                                            style: 'currency', 
                                            currency: 'IDR', 
                                            minimumFractionDigits: 2, 
                                        }); 
                                        html += '<tr>'+
                                                    '<td>'+response['descr']['uraian'][i]+'</td>'+
                                                    '<td>'+response['descr']['satuan'][i]+'</td>'+
                                                    '<td>'+format.format(response['descr']['harga'][i])+'</td>'+
                                                    '<td>'+remarks+'</td>'+
                                                '</tr>';
                                    }
                                    $('.tbody').html(html);
                                }
                            });
                        });
                        $('#datatable').DataTable({
                            searchDelay: 1000,
                            responsive: true,
                            processing: true,
                            language: {
                                searchPlaceholder: 'Search...',
                                sSearch: '',
                                processing: "<i class='fa fa-spin fa-refresh'></i>",
                            },
                            "order": [[ 3, "desc" ]],
                            // serverSide: true,
                            // ordering: false,
                            // bLengthChange: false,
                            ajax: {
                                url: "{{url('ahs/ajax')}}",
                                type: 'GET',
                                dataSrc: 'data',
                            },
                            columns: [
                                { data: 'id' },
                                { data: 'pekerjaan' },
                                // { data: 'descr' },
                                { data: 'modify_by' },
                                { data: 'last_update' },
                                { data : 'action' },
                            ],
                        });
                    });
                </script>  
@endsection