@extends('templates.main')

@section('content')
                <!-- Forms -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="m-t-0 header-title"><b>Update User</b></h4>
                                    <p class="text-muted m-b-30 font-13">
                                        Form User Update
                                    </p>
                                    @if($errors->any())
                                        @foreach($errors->all() as $error)
                                            @if($errors->has('success'))
                                                <div class="alert alert-success">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @else
                                                <div class="alert alert-danger">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @endif    
                                        @endforeach
                                    @endif
                                    <form method="post" action="{{route('user.edit', $user->id)}}" class="form-horizontal m-b-10" role="form">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label for="username" class="col-sm-3 control-label">Username</label>
                                            <div class="col-sm-9">
                                              <input name="username" value="{{ $user->username }}" type="text" class="form-control" id="user_name" placeholder="Username" readonly="readonly" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="col-sm-3 control-label">Name</label>
                                            <div class="col-sm-9">
                                              <input name="nama" value="{{ $user->nama }}" type="text" class="form-control" id="user_fullname" placeholder="Fullname" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="col-sm-3 control-label">Email</label>
                                            <div class="col-sm-9">
                                              <input name="email" value="{{ $user->email }}" type="email" class="form-control" id="user_email" placeholder="Email" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="phone" class="col-sm-3 control-label">Phone</label>
                                            <div class="col-sm-9">
                                              <input name="phone" value="{{ $user->phone }}" type="text" class="form-control" id="user_phone" placeholder="Phone" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="role" class="col-sm-3 control-label">Role</label>
                                            <?php
                                            $opt = array(1 => 'Administrator', 'User');
                                            $output = '';

                                            for ($i=1; $i <= count($opt) ; $i++) { ?>
                                                <div class="col-sm-2">
                                                    <div class="radio radio-inline">
                                                        <input type="radio" id="<?=strtolower($opt[$i])?>" value="<?=$i?>" name="role" <?=( $user->role == $i ? 'checked' : '' )?>>
                                                        <label for="<?=strtolower($opt[$i])?>"> <?=$opt[$i]?> </label>
                                                    </div>
                                                </div>
                                            <?php }
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="menu" class="col-sm-3 control-label">Menu</label>
                                            <div class="col-sm-9">
                                                @php
                                                    $viewer = array(
                                                        1 => 'Material',
                                                        2 => 'PIR'
                                                    );
                                                @endphp
                                                <select class="form-control select2-menu" id="menu" name="menu" required>
                                                    <option></option>
                                                    @if ($user->menu == 0)
                                                        <option value="0" selected>All</option>
                                                    @else 
                                                        @foreach ($viewer as $key => $item)
                                                        <option value="{{ $key }}" {{ $user->menu==$key?'selected':'' }}>{{ $item }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group" id="btn-pass">
                                            <label for="role" class="col-sm-3 control-label"></label>
                                            <div class="col-sm-9">
                                                <a href="javascript:void(0);" class="btn btn-info" id="pass">Change Password</a>
                                            </div>
                                        </div>
                                        <div class="form-group hidden" id="btn-newpass">
                                            <label for="password" class="col-sm-3 control-label">Password</label>
                                            <div class="col-sm-9">
                                              <input name="password" minlength="8" type="password" id="val-pass" class="form-control" id="password" placeholder="Password">
                                            </div>
                                        </div>
                                        <div class="form-group hidden" id="btn-renewpass">
                                            <label for="password_confirmation" class="col-sm-3 control-label">Re Password</label>
                                            <div class="col-sm-9">
                                              <input name="password_confirmation" type="password" id="val-repass" class="form-control" id="password_confirmation" placeholder="Retype Password">
                                            </div>
                                        </div>
                                        <div class="form-group m-b-0">
                                            <div class="col-sm-offset-3 col-sm-9">
                                              <button type="submit" id="update" class="btn btn-inverse waves-effect waves-light">Update</button>
                                              <a href="{{ url('user') }}" class="btn btn-default waves-effect waves-light">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                    $("#pass").click(function() {
                        $("#btn-pass").addClass("hidden");
                        $("#btn-newpass").removeClass("hidden");
                        $("#btn-renewpass").removeClass("hidden");

                        $("#update").click(function() {
                            if ($("#val-pass").val() != $("#val-repass").val()) {
                                sweetAlert("Oops...","Password tidak sama","error");  
                                return false;
                            }
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(function() {
                        $(".select2-menu").select2({
                            placeholder: 'Pilih Menu',
                            allowClear: true
                        });                        
                    });
                </script>
                <script>
                    $(document).ready(function () {
                        $('input[name=role]').click(function() {
                            $('#menu').val();
                            var admin = '<option></option>'+
                                        '<option value="0">All</option>';
                                        
                            var user =  '<option></option>'+
                                        '<option value="1">AHS</option>'+
                                        '<option value="2">PIR</option>';
                            if ($(this).val() == 1) {
                                $('#menu').html(admin);
                            } else {
                                $('#menu').html(user);
                            }
                        });
                    });
                </script>
@endsection