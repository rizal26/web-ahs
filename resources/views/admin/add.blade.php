@extends('templates.main')

@section('content')
                <!-- Forms -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="m-t-0 header-title"><b>Add User</b></h4>
                                    <p class="text-muted m-b-30 font-13">
                                        Form User Creation
                                    </p>
                                    @if($errors->any())
                                        @foreach($errors->all() as $error)
                                            @if($errors->has('success'))
                                                <div class="alert alert-success">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @else
                                                <div class="alert alert-danger">
                                                    <strong>{{ $error }}</strong>
                                                </div>
                                            @endif    
                                        @endforeach
                                    @endif
                                    <form method="post" action="{{route('user.add')}}" class="form-horizontal m-b-10" role="form">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label for="username" class="col-sm-3 control-label">Username</label>
                                            <div class="col-sm-9">
                                              <input name="username" type="text" class="form-control" id="username" placeholder="Username" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="col-sm-3 control-label">Name</label>
                                            <div class="col-sm-9">
                                              <input name="nama" type="text" class="form-control" id="nama" placeholder="Fullname" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="col-sm-3 control-label">Email</label>
                                            <div class="col-sm-9">
                                              <input name="email" type="email" class="form-control" id="email" placeholder="Email" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="phone" class="col-sm-3 control-label">Phone</label>
                                            <div class="col-sm-9">
                                              <input name="phone" type="text" class="form-control" id="phone" placeholder="Phone" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="role" class="col-sm-3 control-label">Role</label>
                                            <div class="col-sm-2">
                                                <div class="radio radio-inline">
                                                    <input type="radio" id="admin" value="1" name="role">
                                                    <label for="admin"> Administrator </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="radio radio-inline">
                                                    <input type="radio" id="viewer" value="2" name="role">
                                                    <label for="viewer"> User </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="menu" class="col-sm-3 control-label">Menu</label>
                                            <div class="col-sm-9">
                                                <select class="form-control select2-menu" id="menu" name="menu" required>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="col-sm-3 control-label">Password</label>
                                            <div class="col-sm-9">
                                              <input name="password" type="password" class="form-control" id="password" placeholder="Password" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password_confirmation" class="col-sm-3 control-label">Re Password</label>
                                            <div class="col-sm-9">
                                              <input name="password_confirmation" type="password" class="form-control" id="password_confirmation" placeholder="Retype Password" required>
                                            </div>
                                        </div>
                                        <div class="form-group m-b-0">
                                            <div class="col-sm-offset-3 col-sm-9">
                                              <button type="submit" class="btn btn-inverse waves-effect waves-light">Create</button>
                                              <a href="{{ url('user') }}" class="btn btn-default waves-effect waves-light">Back</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                    $(function() {
                        $(".select2-menu").select2({
                            placeholder: 'Pilih Menu',
                            allowClear: true
                        });                        
                    });
                </script>

                <script>
                    $(document).ready(function () {
                        $('input[name=role]').click(function() {
                            $('#menu').val();
                            var admin = '<option></option>'+
                                        '<option value="0">All</option>';
                                        
                            var user =  '<option></option>'+
                                        '<option value="1">AHS</option>'+
                                        '<option value="2">PIR</option>';
                            if ($(this).val() == 1) {
                                $('#menu').html(admin);
                            } else {
                                $('#menu').html(user);
                            }
                        });
                    });
                </script>
@endsection