                <footer class="footer text-right">
                @if(Request::segment(1) == 'home')
                    <span style="color:white; text-shadow: 5px 5px 5px black;">
                        &copy; {{ date('Y') }}. All rights reserved.
                    </span>
                @else
                    &copy; {{ date('Y') }}. All rights reserved.
                @endif
                </footer>