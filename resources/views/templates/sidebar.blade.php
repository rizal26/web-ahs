            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>

                            <li class="has_sub">
                                <a href="{{ url('/home') }}" class="waves-effect"><i class="ti-home"></i> <span> Home </span></a>
                            </li>

                            @if (Session::get('role') == 1)
                            <li>
                                <a href="{{ url('/user') }}" class="waves-effect"><i class="ti-user"></i> <span> User Management </span> </a>
                            </li>
                            
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-wrench"></i> <span> Master </span> <span class="menu-arrow"></span></a>
                                <ul>
                                    <li><a href="{{ url('/department') }}">Department</a></li>
                                    <li><a href="{{ url('/kelompok') }}">Kelompok</a></li>
                                </ul>
                            </li>
                            
                            @endif
                            @if (Session::get('menu') == 1 || Session::get('role') == 1)
                                
                            <li class="text-muted menu-title">AHS</li>
                            
                            <li class="">
                                <a href="{{ url('/ahs') }}" class="waves-effect"><i class="fa fa-calculator"></i> <span> Analisa Harga Satuan </span> 
                                    {{-- <span class="menu-arrow"></span> --}}
                                </a>
                                {{-- <ul>
                                    <li><a href="#">Persiapan</a></li>
                                    <li><a href="#">Pek. Tanah & Pondasi</a></li>
                                    <li><a href="#">Pek. Pasangan</a></li>
                                    <li><a href="#">Pek. Atap & Flapond</a></li>
                                    <li><a href="#">Pek. Elektrikal</a></li>
                                    <li><a href="#">Pek. Kusen & Kunci</a></li>
                                    <li><a href="#">Pek. Sanitary</a></li>
                                    <li><a href="#">Pek. Penutup Cat</a></li>
                                    
                                </ul> --}}
                            </li>
                            <li>
                                <a href="{{ url('/material') }}" class="waves-effect"><i class="fa fa-cogs"></i> <span> Material </span> </a>
                            </li>
                            @endif
                            @if (Session::get('menu') == 2 || Session::get('role') == 1)
                            <li class="text-muted menu-title">PIR</li>
                            
                            <li>
                                <a href="{{ url('/pir') }}" class="waves-effect"><i class="fa fa-file-text-o"></i> <span> PIR Management</span> </a>
                            </li>
                            
                            @endif
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End -->