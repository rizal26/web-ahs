<!DOCTYPE html>
<html>
    <head>
    @include('templates.header')
    @if(Request::segment(1) == 'home')
    <style>
        body{
            background-image: url("<?=asset('assets/images/background.jpg')?>");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            /* position: relative; */
            /* object-fit: cover; */
        }
        /* .content-page > .content {
            -webkit-box-shadow: 11px 200px 74px -115px rgba(0,0,0,0.7) inset;
            -moz-shadow: 11px 200px 74px -115px rgba(0,0,0,0.7) inset;
            box-shadow: 11px 200px 74px -115px rgba(0,0,0,0.7) inset;
        } */
    </style>
    @endif
    <style>
        .se-pre-con {
            position: fixed;
            left: 0px;
            opacity: 50%;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif) center no-repeat #fff;
        }
    </style>
    </head>
    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
            @include('templates.top')
            @include('templates.sidebar')
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="vignet">
                    @yield('content')
                        </div>
                    </div> <!-- container -->
                </div> <!-- content -->
                @include('templates.footer')
            </div>
            @include('templates.rightbar')
        </div>
        <!-- END wrapper -->
        @include('templates.js')
    </body>
</html>
