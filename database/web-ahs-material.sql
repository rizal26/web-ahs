/*
 Navicat Premium Data Transfer

 Source Server         : 125
 Source Server Type    : MySQL
 Source Server Version : 100038
 Source Host           : 128.199.133.125:3306
 Source Schema         : web-ahs

 Target Server Type    : MySQL
 Target Server Version : 100038
 File Encoding         : 65001

 Date: 13/02/2020 00:46:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `descr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `modify_by` int(10) NOT NULL,
  `create_date` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `last_update` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 129 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES (1, 'A & FIM', 3, '2020-01-30 23:14:44', '2020-01-30 23:16:28');
INSERT INTO `department` VALUES (2, 'ACCOUNT SERVICES FSG & BCE DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (3, 'ACCOUNT SERVICES MINING DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (4, 'ACCOUNTING & TAX DIVISION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (5, 'AREA 1 MARKETING & SALES', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (6, 'AREA 2 MARKETING & SALES', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (7, 'AREA 3 MARKETING & SALES', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (8, 'BANKING & TRADE FINANCE', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (9, 'BC IT OPS DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (10, 'BERAU COAL ENERGY GROUP', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (11, 'BMO DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (12, 'BUDGET CONTROL', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (13, 'BUSINESS IMPROVEMENT & SIPM DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (14, 'C & B STRATEGIC DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (15, 'CASH MANAGEMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (16, 'CATEGORY SECT FUEL LUBRICANTS ENERGY', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (17, 'CATEGORY SECT GA IT LONG TAILS', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (18, 'CATEGORY SECT MAINTENANCE REPAIR RENTAL', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (19, 'CHID', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (20, 'COAL HAULING & CPP DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (21, 'COMUNITY BASE DEVELOPMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (22, 'COMUNITY ENTERPRISE DEV.', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (23, 'CONTRACT & CATEGORY MANAGEMENT DEPT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (24, 'CONTRACTOR MANAGEMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (25, 'CORPORATE PROCUREMENT SCM', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (26, 'CORPORATE SECRETARY', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (27, 'CORPORATE SHARED SERVICES', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (28, 'CRM DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (29, 'CSID DEV.', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (30, 'CSR FINANCIAL PLANING & CONTROL', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (31, 'ELECTRICAL PROJECT IMPROVE DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (32, 'ENVIRONMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (33, 'ER & SAFETY SERVICES', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (34, 'EXIT COMPLIANCE MANAGEMENT DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (35, 'FINANCE & ACCOUNTING DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (36, 'FINANCE & LEGAL', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (37, 'FINANCE DIRECTORAT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (38, 'FINANCIAL CONTROLING', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (39, 'FINANCIAL PLANNING & MGT. REPORTING', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (40, 'FINANCIAL REPORTING', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (41, 'FINANCIAL REPORTING & ACC. DIVISION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (42, 'FINANCIAL RISK MANAGEMENT DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (43, 'FPM', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (44, 'GENERAL SERVICE', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (45, 'GEOLOGY & EXPLORATION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (46, 'GEOLOGY & GEOTECHNIC', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (47, 'GMO CONTRACTS DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (48, 'GMO MARKETING & TRADING DIVISION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (49, 'GMO OPERATION DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (50, 'GS MTL', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (51, 'GURIMBANG MINE PROJECT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (52, 'HR ADVISOR', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (53, 'HR OPERATION & IR DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (54, 'HR OPERATIONS DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (55, 'HR OPERATIONS DEPARTMENT MTL', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (56, 'HRBP DEVELOPMENT GROUP DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (57, 'HRBP DIVISION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (58, 'HRGS OPERATIONS DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (59, 'HSE CAMPAIGN SECTION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (60, 'HSE CT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (61, 'HSE SERVICED DEPT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (62, 'INDUSTRIAL RELATION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (63, 'INDUSTRIAL RELATION DIVISION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (64, 'INFRA PROJECT DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (65, 'INTERNAL AUDIT DIVISION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (66, 'INVESTMENT DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (67, 'IT GOVERNANCE & COMPLIANCE DEPT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (68, 'IT HELPDESK & SUPPORT DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (69, 'JKT LICENSE DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (70, 'K3L AUDIT DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (71, 'L&D DEVELOPMENT PROGRAM DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (72, 'LAM', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (73, 'LEGAL CORPORATE & OPERATION 2 DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (74, 'LEGAL DIRECTORAT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (75, 'LEGAL MINING DEPARTMENT - BC SITE', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (76, 'LEGAL MINING DIVISION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (77, 'LEGAL OPERATION 1 DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (78, 'LEGAL PROJECT DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (79, 'LICENSE & CORP COMMUNICATION DIVISION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (80, 'LOGISTIC', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (81, 'LONG TERM MINE PLAN DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (82, 'MARINE SUPPORT & ADM DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (83, 'MARKETING & MARINE DIVISION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (84, 'MARKETING & MARINE SUPPORT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (85, 'MARKETING & SALES ADM. SUPPORT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (86, 'MARKETING RESEARCH & COAL DEVELOPMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (87, 'MINE BMO 1', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (88, 'MINE BMO 2', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (89, 'MINE CLOSURE', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (90, 'MINE LMO', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (91, 'MINE OPERATIONS & SUPPORT DIVISION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (92, 'MINE PLANNING & TECH. SERVICES DIVISION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (93, 'MINE SMO', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (94, 'MINE SUPPORT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (95, 'MINING TECHNOLOGY DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (96, 'OCCUP HEALTH&INDUSTRIAL HYGIENE SECTION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (97, 'OHS', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (98, 'OPERATION & HSE DEPUTY DIRECTORAT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (99, 'OPERATION & HSE DIRECTORAT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (100, 'OPERATION RISK MANAGEMENT DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (101, 'OPERATIONAL & FINANCIAL AUDIT DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (102, 'OPERATIONS COMPLIANCE DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (103, 'OPERATIONS SUPPORT & RELATIONS DIVISION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (104, 'OPERATIONS SUPPORT DIVISION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (105, 'PARTICIPATIVE & IT AUDIT DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (106, 'PERSONNEL ADMIN DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (107, 'PORT & BARGING OPERATION DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (108, 'PROCUREMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (109, 'PRODUCT QUALITY & DELIVERY CONTROL', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (110, 'PROJECT ENGINEERING DEV DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (111, 'PUBLIC RELATION DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (112, 'RADIO DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (113, 'SAFETY INVESTIGATOR & RISK CONTROL', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (114, 'SAFETY OPERATION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (115, 'SALES & MARKETING BIB DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (116, 'SECURITY', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (117, 'SGI', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (118, 'SHIPPING', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (119, 'SHORT TERM MINE PLAN', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (120, 'SSU', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (121, 'STRATEGY & PERFORMANCE MGMT OFFICE DEPT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (122, 'SYSTEM COMPLIANCE DIVISION', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (123, 'SYSTEM DEVELOPMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (124, 'TALENT & DEVELOPMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (125, 'TAX STRATEGIC', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (126, 'TAX', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');
INSERT INTO `department` VALUES (127, 'TREASURY DEPARTMENT', 3, '2020-01-30 23:14:44', '2020-01-30 23:15:09');

-- ----------------------------
-- Table structure for initiation
-- ----------------------------
DROP TABLE IF EXISTS `initiation`;
CREATE TABLE `initiation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `category` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `location` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `department` int(10) NOT NULL,
  `sponsor` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `owner` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `manager` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mandatory` int(1) NOT NULL COMMENT '1: yes, 0: no',
  `urgent` int(1) NOT NULL COMMENT '1: yes, 0: normal',
  `connect` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '1: yes, 0: no',
  `reason` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `reason_descr` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `project_background` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `project_descr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `expenditure` int(1) NOT NULL COMMENT '1: Capex, 0: Opex',
  `annual_budget` int(1) NOT NULL COMMENT '1: yes, 0: no',
  `estimate_budget` decimal(20, 3) NOT NULL,
  `project_potential` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `in_scope` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `out_scope` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `deliver` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `deliver_descr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `start_date` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `end_date` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `milestones` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `milestones_descr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `constraint` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `impact` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `impact_etc` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `internal` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `external` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `structure` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `structure_etc` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `mechanical` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mechanical_etc` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `electrical` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `electrical_etc` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `design` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `cost` decimal(20, 3) NOT NULL,
  `preliminary_design` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `preliminary_spec` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `time_frame_start` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `time_frame_end` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of initiation
-- ----------------------------
INSERT INTO `initiation` VALUES (1, 'descr', 'Productions', 'pro location', 'New Project', 25, 'pro sponsor', 'pro owner', 'pro manager', 1, 0, '0', 'Productions', 'reason descr', 'pro background', 'pro descr', 0, 1, 5000000.350, 'pro potential', 'in scope', 'out scope', 'deliver', 'descr deliver', '10-Feb-2020', '19-Feb-2020', 'milestones', 'milestones details', 'contraint descr', 'etc', 'descr impact', 'internet', 'external', 'Fix Plant', NULL, 'Other Work', 'mec descr', 'Scada', NULL, 'Fix Plant', 100000000.000, '020220071658-backgroundAHS.jpg', 'spec', '10-Feb-2020', '06-Mar-2020');
INSERT INTO `initiation` VALUES (2, 'descr', 'Productions', 'pro location', 'New Project', 25, 'pro sponsor', 'pro owner', 'pro manager', 1, 0, '0', 'Productions', 'reason descr', 'pro background', 'pro descr', 0, 1, 5000000.350, 'pro potential', 'in scope', 'out scope', 'deliver', 'descr deliver', '10-Feb-2020', '19-Feb-2020', 'milestones', 'milestones details', 'contraint descr', 'etc', 'descr impact', 'internet', 'external', 'Fix Plant', NULL, 'Other Work', 'mec descr', 'Scada', NULL, 'Fix Plant', 100000000.000, '020220071821-backgroundAHS.jpg', 'spec', '10-Feb-2020', '06-Mar-2020');
INSERT INTO `initiation` VALUES (3, 'descr', 'Productions', 'pro location', 'New Project', 25, 'pro sponsor', 'pro owner', 'pro manager', 1, 0, '0', 'Productions', 'reason descr', 'pro background', 'pro descr', 0, 1, 5000000.350, 'pro potential', 'in scope', 'out scope', 'deliver', 'descr deliver', '10-Feb-2020', '19-Feb-2020', 'milestones', 'milestones details', 'contraint descr', 'etc', 'descr impact', 'internet', 'external', 'Fix Plant', NULL, 'Other Work', 'mec descr', 'Scada', NULL, 'Fix Plant', 100000000.000, '020220071857-backgroundAHS.jpg', 'spec', '10-Feb-2020', '06-Mar-2020');
INSERT INTO `initiation` VALUES (4, 'descr', 'Productions', 'pro location', 'New Project', 25, 'pro sponsor', 'pro owner', 'pro manager', 1, 0, '0', 'Productions', 'reason descr', 'pro background', 'pro descr', 0, 1, 5000000.350, 'pro potential', 'in scope', 'out scope', 'deliver', 'descr deliver', '10-Feb-2020', '19-Feb-2020', 'milestones', 'milestones details', 'contraint descr', 'etc', 'descr impact', 'internet', 'external', 'Fix Plant', NULL, 'Other Work', 'mec descr', 'Scada', NULL, 'Fix Plant', 100000000.000, '020220071922-backgroundAHS.jpg', 'spec', '10-Feb-2020', '06-Mar-2020');
INSERT INTO `initiation` VALUES (5, 'Pluit Village', 'Productions', 'berau', 'New Project', 1, 'PT. Berau Coal', 'Herlan', 'Rizal', 1, 0, '1', 'Regulations', 'Reason Description', 'PROJECT BACKGROUND & OBJECTIVE', 'PROJECT DESCRIPTIONS', 1, 0, 5000000.350, 'ROJECT POTENTIALS ALTERNATIVE\r\nDescription', 'in scope', 'out scope', 'deliver', 'ROJECT KEY DELIVERABLES', '19-Feb-2020', '26-Dec-2019', 'milestones', 'milestones details', 'PROJECT CONSTRAINT', 'Cost Efficiency', NULL, 'internet', 'external', 'Fix Plant', NULL, 'Fuel Tank / Station', NULL, 'Power House', NULL, 'Fix Plant', 100000000.000, '040220113938-backgroundAHS.jpg', 'spec', '03-Feb-2020', '27-Feb-2020');
INSERT INTO `initiation` VALUES (6, 'Pluit Village', 'Regulations', 'berau', 'Typical Existing', 2, 'PT. Berau Coal', 'Herlan', 'Rizal', 1, 0, '1', 'Innovations & Improvement', 'Reason Description', 'PROJECT BACKGROUND & OBJECTIVE', 'PROJECT DESCRIPTIONS', 1, 0, 5000000.350, 'ROJECT POTENTIALS ALTERNATIVE\r\nDescription', 'in scope', 'out scope', 'deliver', 'ROJECT KEY DELIVERABLES', '19-Feb-2020', '26-Dec-2019', 'milestones', 'milestones details', 'PROJECT CONSTRAINT', 'Regulations', NULL, 'internet', 'external', 'Jetty / Harbour', NULL, 'Fix Plant', NULL, 'Scada', NULL, 'Fix Plant', 100000000.000, '040220120506-backgroundAHS.jpg', 'spec', '03-Feb-2020', '27-Feb-2020');
INSERT INTO `initiation` VALUES (7, 'Pluit Village', 'Innovations & Improvement', 'berau', 'New Project', 2, 'PT. Berau Coal', 'Herlan', 'Rizal', 1, 0, '1', 'Regulations', 'Reason Description', 'PROJECT BACKGROUND & OBJECTIVE', 'PROJECT DESCRIPTIONS', 1, 0, 5000000.350, 'ROJECT POTENTIALS ALTERNATIVE\r\nDescription', 'in scope', 'out scope', 'deliver', 'ROJECT KEY DELIVERABLES', '19-Feb-2020', '26-Dec-2019', 'milestones', 'milestones details', 'PROJECT CONSTRAINT', 'Cost Efficiency', NULL, 'internet', 'external', 'Building / Warehouse', NULL, 'Pipe work', NULL, 'Genset', NULL, 'Fix Plant', 100000000.000, '040220123028-backgroundAHS.jpg', 'spec', '03-Feb-2020', '27-Feb-2020');
INSERT INTO `initiation` VALUES (8, 'Pluit Village', 'Productions', 'berau', 'Typical Existing', 4, 'PT. Berau Coal', 'Herlan', 'Rizal', 1, 0, '1', 'Regulations', 'Reason Description', 'PROJECT BACKGROUND & OBJECTIVE', 'PROJECT DESCRIPTIONS', 1, 0, 5000000.350, 'ROJECT POTENTIALS ALTERNATIVE\r\nDescription', 'in scope', 'out scope', 'deliver', 'ROJECT KEY DELIVERABLES', '19-Feb-2020', '26-Dec-2019', 'milestones', 'milestones details', 'PROJECT CONSTRAINT', 'Cost Efficiency', NULL, 'internet', 'external', 'Jetty / Harbour', NULL, 'Fuel Tank / Station', NULL, 'CCTV', NULL, 'Fix Plant', 100000000.000, '040220124804-backgroundAHS.jpg', 'spec', '03-Feb-2020', '27-Feb-2020');
INSERT INTO `initiation` VALUES (9, 'Pluit Village', 'Productions', 'berau', 'New Project', 27, 'PT. Berau Coal', 'pro owner', 'pro manager', 0, 0, '1', 'Innovations & Improvement', 'asddasd', 'asdasd', 'asdasd', 1, 0, 256511.000, 'asdasd', 'asdasd', 'asdasd', 'asdasdd', 'asdasd', '11-Feb-2020', '14-May-2020', 'asdasd', 'asdasd', 'asdasd', 'Cost Efficiency', NULL, 'asdasd', 'asdasd', 'Earthwork', NULL, 'Fuel Tank / Station', NULL, 'Other Work', 'qweqwe', 'Fix Plant', 978798.000, '050220111935-backgroundAHS.jpg', 'asdasd', '12-Feb-2020', '07-Mar-2020');
INSERT INTO `initiation` VALUES (10, 'asdasd', 'Productions', 'pro location', 'New Project', 1, 'PT. Berau Coal', 'Herlan', 'Rizal', 0, 0, '0', 'Innovations & Improvement', 'asdasd', 'asdasd', 'asdasd', 1, 0, 5000000.350, 'asdasd', 'asd', 'asdas', 'asdasd', 'asdasd', '11-Feb-2020', '27-Feb-2020', 'milestones', 'asdasd', 'asdasd', 'Regulations', NULL, 'internet', 'asdasd', 'Earthwork', NULL, 'Hydrant / Water Pump', NULL, 'Genset', NULL, 'Other Work', 100000000.000, '050220042207-backgroundAHS.jpg', 'spec', '18-Feb-2020', '18-Feb-2020');
INSERT INTO `initiation` VALUES (11, 'test project1', 'Productions', 'asdasd', 'New Project', 3, 'asda', 'asda', 'asdasasd', 0, 1, '0', 'Regulations', 'asdasd', 'asdas', 'asdasd', 1, 0, 123231123.000, 'asdasd', 'asdasd', 'asdasd', 'asd', 'asdasd', '21-Jan-2020', '30-Dec-2020', 'aasdads', 'asdasd', 'asdasd', 'Sustainability', NULL, 'asdasd', 'asdasd', 'Earthwork', NULL, 'Fuel Tank / Station', NULL, 'Power House', NULL, 'Fix Plant', 123123123.000, '060220054348-backgroundAHS.jpg', 'asdasdasd', '10-Feb-2020', '27-Feb-2020');
INSERT INTO `initiation` VALUES (12, 'Building ABC', 'Productions', 'berau', 'New Project', 26, 'PT. Berau Coal', 'Herlan', 'pro manager', 1, 0, '1', 'Innovations & Improvement', 'asdasd', 'asdasd', 'asdasd', 1, 0, 878788.000, 'asdasda', 'asdasd', 'asdasd', 'asdasd', 'asdasd', '23-Jan-2020', '28-Feb-2020', 'asdasd', 'asdasd', 'asdasd', 'Cost Efficiency', NULL, 'asdas', 'asdas', 'Jetty / Harbour', NULL, 'Metal Catcher / Magnetic Separator', NULL, 'Scada', NULL, 'Fix Plant', 23423423.000, '100220010812-www_albums.jpg', 'spectrum', '10-Feb-2020', '20-Feb-2020');
INSERT INTO `initiation` VALUES (13, 'WEB AHS', 'Regulations', 'asd', 'New Project', 1, 'asd', 'asd', 'asd', 1, 1, '1', 'Regulations', 'asd', 'asd', 'asd', 1, 0, 234234.000, 'qweqwe', 'qwe', 'qwe', 'qwe', 'qwe', '12-Feb-2020', '28-Feb-2020', 'qweqwe', 'qweqwe', 'qweqwe', 'Regulations', NULL, 'qwe', 'qwe', 'Jetty / Harbour', NULL, 'Metal Catcher / Magnetic Separator', NULL, 'Scada', NULL, 'Fix Plant', 234234.000, '100220045029-Matrix_Talent.png', 'qwe', '02-Feb-2020', '27-Feb-2020');

-- ----------------------------
-- Table structure for kelompok
-- ----------------------------
DROP TABLE IF EXISTS `kelompok`;
CREATE TABLE `kelompok`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `descr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `modify_by` int(10) NOT NULL,
  `create_date` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `last_update` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kelompok
-- ----------------------------
INSERT INTO `kelompok` VALUES (1, 'Bahan Batu', 3, '2020-01-30 19:59:48', '2020-01-30 23:05:00');
INSERT INTO `kelompok` VALUES (2, 'Bahan Besi', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (3, 'Bahan Buis Beton', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (4, 'Bahan Elektrikal', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (5, 'Bahan Elektrikal Gedung', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (6, 'Bahan Gorong-gorong', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (7, 'Bahan Kaca', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (8, 'Bahan Kapur', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (9, 'Bahan Kayu', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (10, 'Bahan Lain-lain', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (11, 'Bahan Lantai', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (12, 'Bahan Pasir', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (13, 'Bahan Paving Stone', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (14, 'Bahan Pengecatan + Politur', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (15, 'Bahan Penggantung + Kunci', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (16, 'Bahan Penutup Atap', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (17, 'Bahan Pipa Besi', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (18, 'Bahan Piva PVC', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (19, 'Bahan Pondasi', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (20, 'Bahan Sanitair', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (21, 'Bahan Semen / P.C', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (22, 'Bahan Tripleks / Kayu Lapis', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');

-- ----------------------------
-- Table structure for material
-- ----------------------------
DROP TABLE IF EXISTS `material`;
CREATE TABLE `material`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `foto` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `descr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `kelompok` int(10) NOT NULL,
  `satuan` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `harga` decimal(20, 2) NOT NULL,
  `old_harga` decimal(20, 2) NULL DEFAULT NULL,
  `modify_by` int(10) NOT NULL,
  `create_date` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `last_update` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 96 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of material
-- ----------------------------
INSERT INTO `material` VALUES (8, '110220055537-1596605_302ec8d2-584e-4467-a47e-38df04fe5b11.jpg', 'Pasir Urug (Lokal)', 12, 'm3', 209000.00, 180900.00, 3, '2020-02-11 12:55:37', '2020-02-11 13:37:20');
INSERT INTO `material` VALUES (9, '110220060031-1596605_302ec8d2-584e-4467-a47e-38df04fe5b11.jpg', 'Pasir Pasang  (Lokal)', 12, 'm3', 180900.00, NULL, 3, '2020-02-11 13:00:31', '2020-02-11 13:00:31');
INSERT INTO `material` VALUES (10, '110220060319-download.jpg', 'Pasir Halus', 12, 'm3', 273000.00, 236.25, 3, '2020-02-11 13:03:19', '2020-02-11 13:34:42');
INSERT INTO `material` VALUES (11, '110220060641-pasir-beton.jpg', 'Pasir Beton (Ex.Palu)', 12, 'm3', 582000.00, 503550.00, 3, '2020-02-11 13:06:41', '2020-02-11 13:33:13');
INSERT INTO `material` VALUES (12, '110220061301-pasir-urug.jpg', 'Sirtu urug', 12, 'm3', 702000.00, 209000.00, 3, '2020-02-11 13:13:01', '2020-02-11 13:35:41');
INSERT INTO `material` VALUES (13, '110220061828-Sirtu-Alam.jpg', 'Sirtu Gunung', 12, 'm3', 149000.00, 149000.00, 3, '2020-02-11 13:18:28', '2020-02-11 13:36:22');
INSERT INTO `material` VALUES (14, '110220064019-images.jpg', 'Tanah urug', 12, 'm3', 117000.00, NULL, 3, '2020-02-11 13:40:19', '2020-02-11 13:40:19');
INSERT INTO `material` VALUES (15, '110220064255-Sirtu-Alam.jpg', 'Pasir batu', 12, 'm3', 513000.00, NULL, 3, '2020-02-11 13:42:55', '2020-02-11 13:42:55');
INSERT INTO `material` VALUES (16, '110220065056-Batu_pasang.jpg', 'Batu pasang', 1, 'm3', 621000.00, NULL, 3, '2020-02-11 13:50:56', '2020-02-11 13:50:56');
INSERT INTO `material` VALUES (17, '110220065800-Batu_pecah_1520.jpg', 'Batu pecah 15/20', 1, 'm3', 621000.00, NULL, 3, '2020-02-11 13:58:00', '2020-02-11 13:58:00');
INSERT INTO `material` VALUES (18, '110220065800-Batu_pecah_1520.jpg', 'Batu pecah 15/20', 1, 'm3', 621000.00, NULL, 3, '2020-02-11 13:58:00', '2020-02-11 13:58:00');
INSERT INTO `material` VALUES (19, '110220070205-batu_kali_quary.jpg', 'Batu kali/Batu quary', 1, 'm3', 381000.00, NULL, 3, '2020-02-11 14:02:05', '2020-02-11 14:02:05');
INSERT INTO `material` VALUES (20, '110220070548-Batu_pecah_kali_710.jpg', 'Batu kali pecah 7/10 (tangan)', 1, 'm3', 621000.00, NULL, 3, '2020-02-11 14:05:48', '2020-02-11 14:05:48');
INSERT INTO `material` VALUES (21, '110220070823-makadam-5--7-cm.jpg', 'Batu pecah 5/7 (tangan)', 1, 'm3', 621000.00, 621000.00, 3, '2020-02-11 14:08:23', '2020-02-11 14:14:00');
INSERT INTO `material` VALUES (22, '110220071314-Batu_4-5.jpg', 'Batu kali pecah 4/5 (mesin)', 1, 'm3', 621000.00, 621000.00, 3, '2020-02-11 14:13:14', '2020-02-11 14:14:23');
INSERT INTO `material` VALUES (23, '110220071638-Batu_pecah_2-3.jpg', 'Batu kali pecah 2/3 (mesin)', 1, 'm3', 690000.00, NULL, 3, '2020-02-11 14:16:38', '2020-02-11 14:16:38');
INSERT INTO `material` VALUES (24, '110220072119-Batu_pecah_0.5-1.jpg', 'Batu kali pecah 0.5-1 (mesin', 1, 'm3', 690000.00, NULL, 3, '2020-02-11 14:21:19', '2020-02-11 14:21:19');
INSERT INTO `material` VALUES (25, '110220072348-batu_1-2.jpg', 'Batu pecah 1/2 (mesin)', 1, 'm3', 690000.00, NULL, 3, '2020-02-11 14:23:48', '2020-02-11 14:23:48');
INSERT INTO `material` VALUES (26, '110220072929-laston.jpg', 'Batu pecah tersaring untuk laston', 1, 'm3', 690000.00, NULL, 3, '2020-02-11 14:29:29', '2020-02-11 14:29:29');
INSERT INTO `material` VALUES (27, '110220073353-batu_LPA.jpg', 'Batu pecah tersaring untuk LPA', 1, 'm3', 690000.00, NULL, 3, '2020-02-11 14:33:53', '2020-02-11 14:33:53');
INSERT INTO `material` VALUES (28, '110220074626-LPB.jpg', 'Batu pecah tersaring untuk LPB', 1, 'm3', 690000.00, 690000.00, 3, '2020-02-11 14:46:26', '2020-02-11 14:47:00');
INSERT INTO `material` VALUES (29, '110220081620-batu_pecah.jpg', 'Batu pecah', 1, 'm3', 690000.00, NULL, 3, '2020-02-11 15:16:20', '2020-02-11 15:16:20');
INSERT INTO `material` VALUES (30, '110220081923-batu_padas.jpg', 'Batu padas', 1, 'm3', 381000.00, 381000.00, 3, '2020-02-11 15:19:23', '2020-02-11 15:31:59');
INSERT INTO `material` VALUES (31, '110220082212-batu_gunung.jpg', 'Batu gunung', 1, 'm3', 507000.00, NULL, 3, '2020-02-11 15:22:12', '2020-02-11 15:22:12');
INSERT INTO `material` VALUES (32, '110220082729-batu_koral.JPG', 'Batu koral (biasa)', 1, 'm3', 390000.00, NULL, 3, '2020-02-11 15:27:29', '2020-02-11 15:27:29');
INSERT INTO `material` VALUES (33, '110220083139-batu_alam_10-20.jpg', 'Batu alam 10/20', 1, 'm2', 312000.00, NULL, 3, '2020-02-11 15:31:39', '2020-02-11 15:31:39');
INSERT INTO `material` VALUES (34, '110220083714-batu_kerikil_cor.jpg', 'Batu kerikil cor ( 3 sd 4 cm', 1, 'm3', 567000.00, NULL, 3, '2020-02-11 15:37:14', '2020-02-11 15:37:14');
INSERT INTO `material` VALUES (35, '110220084147-Batu_kerikil_cor_palu.jpg', 'Batu kerikil cor palu', 1, 'm3', 665000.00, NULL, 3, '2020-02-11 15:41:47', '2020-02-11 15:41:47');
INSERT INTO `material` VALUES (36, '110220084501-koral_biasa.jpg', 'Batu kerikil/koral biasa', 1, 'm3', 367000.00, NULL, 3, '2020-02-11 15:45:01', '2020-02-11 15:45:01');
INSERT INTO `material` VALUES (37, '110220084719-koral_halus.jpg', 'batu kerikil/koral halus', 1, 'm3', 542000.00, NULL, 3, '2020-02-11 15:47:19', '2020-02-11 15:47:19');
INSERT INTO `material` VALUES (38, '110220092028-Koral_timbunan.jpg', 'Batu kerikil /koral timbunan(l0kal)', 1, 'm3', 457000.00, NULL, 3, '2020-02-11 16:20:28', '2020-02-11 16:20:28');
INSERT INTO `material` VALUES (39, '110220092313-Batu_tempel_hitam.jpg', 'Batu tempel hitam', 1, 'm2', 264000.00, NULL, 3, '2020-02-11 16:23:13', '2020-02-11 16:23:13');
INSERT INTO `material` VALUES (40, '110220092543-paliman.jpg', 'Batu tempel paliman 15x30', 1, 'm2', 284000.00, NULL, 3, '2020-02-11 16:25:43', '2020-02-11 16:25:43');
INSERT INTO `material` VALUES (41, '110220092739-paras.jpg', 'Batu tempel paras 15x30', 1, 'm2', 306000.00, NULL, 3, '2020-02-11 16:27:39', '2020-02-11 16:27:39');
INSERT INTO `material` VALUES (42, '110220092912-Abu_batu.jpg', 'Batu abu', 1, 'm3', 328000.00, NULL, 3, '2020-02-11 16:29:12', '2020-02-11 16:29:12');
INSERT INTO `material` VALUES (43, '110220093108-batu_bata.jpg', 'Batu bata (cetak tangan)', 1, 'Bh', 2200.00, 2200.00, 3, '2020-02-11 16:31:08', '2020-02-11 16:32:24');
INSERT INTO `material` VALUES (45, '110220093637-Batako.jpg', 'Bata beton 40x15x8 cm K225-17m2', 1, 'Bh', 6100.00, NULL, 3, '2020-02-11 16:36:38', '2020-02-11 16:36:38');
INSERT INTO `material` VALUES (46, '110220093809-Batako.jpg', 'Batu bata beton 30x15x8 cmK175-22M2', 1, 'Bh', 6300.00, NULL, 3, '2020-02-11 16:38:09', '2020-02-11 16:38:09');
INSERT INTO `material` VALUES (47, '110220093941-Batako.jpg', 'Batu bata beton 30x15x7 cm K175m2', 1, 'Bh', 6300.00, NULL, 3, '2020-02-11 16:39:41', '2020-02-11 16:39:41');
INSERT INTO `material` VALUES (48, '110220094208-Batako.jpg', 'Batako lubang 3-40x19x8,5cmK225-12m2', 1, 'Bh', 6300.00, NULL, 3, '2020-02-11 16:42:08', '2020-02-11 16:42:08');
INSERT INTO `material` VALUES (49, '110220094306-Batako.jpg', 'Batako lubang 3-40x20x8,5cmK225-12m2', 1, 'Bh', 6300.00, NULL, 3, '2020-02-11 16:43:06', '2020-02-11 16:43:06');
INSERT INTO `material` VALUES (50, '110220094516-Batako.jpg', 'Batako lubang 3-40x20x810 cmK225-12m2', 1, 'Bh', 6300.00, NULL, 3, '2020-02-11 16:45:16', '2020-02-11 16:45:16');
INSERT INTO `material` VALUES (51, '110220094735-Batako.jpg', 'Batako lubang 2-39x9x9cmK225', 1, 'Bh', 6300.00, NULL, 3, '2020-02-11 16:47:35', '2020-02-11 16:47:35');
INSERT INTO `material` VALUES (52, '120220015914-3_berlian.jpg', 'paving stone abu 2 tb 6 cm (3berlian DT1)', 13, 'Bj', 2000.00, NULL, 3, '2020-02-12 08:59:14', '2020-02-12 08:59:14');
INSERT INTO `material` VALUES (53, '120220022921-Semen_tiga_roda.jpg', 'Semen tiga roda 40 kg', 21, 'zak', 133000.00, NULL, 3, '2020-02-12 09:29:21', '2020-02-12 09:29:21');
INSERT INTO `material` VALUES (54, '120220023250-Semen_tonasa.jpg', 'Semen tonasa 40 kg', 21, 'zak', 100000.00, NULL, 3, '2020-02-12 09:32:50', '2020-02-12 09:32:50');
INSERT INTO `material` VALUES (55, '120220024014-Semen_tiga_roda.jpg', 'Semen 50 kg', 21, 'zak', 160000.00, NULL, 3, '2020-02-12 09:40:14', '2020-02-12 09:40:14');
INSERT INTO `material` VALUES (56, '120220024351-Semen_tiga_roda.jpg', 'semen 40 kg', 21, 'kg', 3400.00, NULL, 3, '2020-02-12 09:43:51', '2020-02-12 09:43:51');
INSERT INTO `material` VALUES (57, '120220024751-semen.jpg', 'Semen 50 kg', 21, 'kg', 320000.00, NULL, 3, '2020-02-12 09:47:51', '2020-02-12 09:47:51');
INSERT INTO `material` VALUES (58, '120220030226-Ready_mix.jpg', 'Beton fc 1453 Mpa setara K-175', 21, 'm3', 2080000.00, NULL, 3, '2020-02-12 10:02:26', '2020-02-12 10:02:26');
INSERT INTO `material` VALUES (59, '120220030357-Ready_mix.jpg', 'Beton fc 18,68 Mpa setara K-225', 21, 'm3', 2174000.00, NULL, 3, '2020-02-12 10:03:57', '2020-02-12 10:03:57');
INSERT INTO `material` VALUES (60, '120220030533-Ready_mix.jpg', 'Beton fc 20,75 Mpa setara K-250', 21, 'm3', 2232000.00, NULL, 3, '2020-02-12 10:05:33', '2020-02-12 10:05:33');
INSERT INTO `material` VALUES (61, '120220031006-Ready_mix.jpg', 'Beton fc 22,83 Mpa setara K-275', 21, 'm3', 2326000.00, NULL, 3, '2020-02-12 10:10:06', '2020-02-12 10:10:06');
INSERT INTO `material` VALUES (62, '120220031149-Ready_mix.jpg', 'Beton fc 24,9 Mpa setara k- 300', 21, 'm3', 2347000.00, NULL, 3, '2020-02-12 10:11:49', '2020-02-12 10:11:49');
INSERT INTO `material` VALUES (63, '120220031412-Ready_mix.jpg', 'Beton fc 29,05 Mpa setara K-350', 21, 'm3', 2442000.00, NULL, 3, '2020-02-12 10:14:12', '2020-02-12 10:14:12');
INSERT INTO `material` VALUES (64, '120220031559-Ready_mix.jpg', 'Beton fc 33,20 Mpa setara k-400', 21, 'm3', 2573000.00, NULL, 3, '2020-02-12 10:15:59', '2020-02-12 10:15:59');
INSERT INTO `material` VALUES (65, '120220033101-Ready_mix.jpg', 'Beton fc 37,35 Mpa setara K-450', 21, 'm3', 2704000.00, NULL, 3, '2020-02-12 10:31:01', '2020-02-12 10:31:01');
INSERT INTO `material` VALUES (66, '120220033206-Ready_mix.jpg', 'Beton fc 41,50 Mpa setara k-500', 21, 'm3', 2835000.00, NULL, 3, '2020-02-12 10:32:06', '2020-02-12 10:32:06');
INSERT INTO `material` VALUES (67, '120220055735-Kapur_bubuk.jpg', 'Kapur bubuk', 8, 'm3', 763000.00, NULL, 3, '2020-02-12 12:57:35', '2020-02-12 12:57:35');
INSERT INTO `material` VALUES (68, '120220055859-Kapur_bubuk.jpg', 'Kapur bubuk', 8, 'kg', 935000.00, NULL, 3, '2020-02-12 12:58:59', '2020-02-12 12:58:59');
INSERT INTO `material` VALUES (69, '120220060027-Kapur_bubuk.jpg', 'Kapur bubuk 1 sak', 8, 'zak', 7700.00, NULL, 3, '2020-02-12 13:00:27', '2020-02-12 13:00:27');
INSERT INTO `material` VALUES (70, '120220060135-Kapur_gamping.jpg', 'Kapur gamping', 8, 'kg', 3960.00, NULL, 3, '2020-02-12 13:01:35', '2020-02-12 13:01:35');
INSERT INTO `material` VALUES (71, '120220060428-kapur_padam.jpg', 'Kapur padam', 8, 'm3', 529000.00, NULL, 3, '2020-02-12 13:04:28', '2020-02-12 13:04:28');
INSERT INTO `material` VALUES (72, '120220064920-Tegel.jpg', 'Tegel ubin warna 20x20 cm', 11, 'Bh', 3400.00, NULL, 3, '2020-02-12 13:49:20', '2020-02-12 13:49:20');
INSERT INTO `material` VALUES (73, '120220065041-Tegel.jpg', 'Tegel ubin warna 30x30 cm', 11, 'Bh', 6800.00, NULL, 3, '2020-02-12 13:50:41', '2020-02-12 13:50:41');
INSERT INTO `material` VALUES (74, '120220065145-Tegel.jpg', 'Tegel ubin warna 40x40 cm', 11, 'Bh', 13500.00, NULL, 3, '2020-02-12 13:51:45', '2020-02-12 13:51:45');
INSERT INTO `material` VALUES (81, '120220092354-plint.jpg', 'Plint ubin pc abu-abu 10x30', 11, 'Bh', 9900.00, NULL, 3, '2020-02-12 16:23:54', '2020-02-12 16:23:54');
INSERT INTO `material` VALUES (82, '120220092843-Keramik.jpg', 'Tegel keramik 10x10 cm', 11, 'Bh', 6200.00, NULL, 3, '2020-02-12 16:28:44', '2020-02-12 16:28:44');
INSERT INTO `material` VALUES (83, '120220092956-Keramik.jpg', 'Tegel keramik 10x20', 11, 'Bh', 12500.00, NULL, 3, '2020-02-12 16:29:56', '2020-02-12 16:29:56');
INSERT INTO `material` VALUES (84, '120220093141-Keramik.jpg', 'Tegel keramik 10x40', 11, 'Bh', 21800.00, NULL, 3, '2020-02-12 16:31:41', '2020-02-12 16:31:41');
INSERT INTO `material` VALUES (85, '120220093235-Keramik.jpg', 'Tegel keramik 20x20', 11, 'Bh', 11700.00, NULL, 3, '2020-02-12 16:32:35', '2020-02-12 16:32:35');
INSERT INTO `material` VALUES (86, '120220093820-Keramik.jpg', 'tegel keramik 30x30 cm motif', 11, 'Bh', 10300.00, NULL, 3, '2020-02-12 16:38:20', '2020-02-12 16:38:20');
INSERT INTO `material` VALUES (87, '120220093926-Keramik.jpg', 'tegel keramik 30x30 cm biasa', 11, 'Bh', 9700.00, NULL, 3, '2020-02-12 16:39:26', '2020-02-12 16:39:26');
INSERT INTO `material` VALUES (88, '120220094027-Keramik.jpg', 'tegel keramik 40x40 cm motif', 11, 'Bh', 21500.00, NULL, 3, '2020-02-12 16:40:27', '2020-02-12 16:40:27');
INSERT INTO `material` VALUES (89, '120220094134-Keramik.jpg', 'Tegel keramik 40x40 cm biasa', 11, 'Bh', 20800.00, NULL, 3, '2020-02-12 16:41:34', '2020-02-12 16:41:34');
INSERT INTO `material` VALUES (90, '120220094704-paras.jpg', 'Tegel keramik 20x25 cm bang terpadu', 11, 'Bh', 5000.00, NULL, 3, '2020-02-12 16:47:04', '2020-02-12 16:47:04');
INSERT INTO `material` VALUES (91, '120220094859-Tegel.jpg', 'Tegel keramik 20x20 cm', 11, 'm2', 57000.00, NULL, 3, '2020-02-12 16:48:59', '2020-02-12 16:48:59');
INSERT INTO `material` VALUES (92, '120220095003-Tegel.jpg', 'Tegel keramik 25x25 cm', 11, 'm2', 57600.00, NULL, 3, '2020-02-12 16:50:03', '2020-02-12 16:50:03');
INSERT INTO `material` VALUES (93, '120220095058-Tegel.jpg', 'Tegel keramik 30x30 cm', 11, 'm2', 62600.00, NULL, 3, '2020-02-12 16:50:58', '2020-02-12 16:50:58');
INSERT INTO `material` VALUES (94, '120220095329-Tegel.jpg', 'Tegel keramik rock tile 30x30 cm', 11, 'm2', 65600.00, NULL, 3, '2020-02-12 16:53:29', '2020-02-12 16:53:29');
INSERT INTO `material` VALUES (95, '120220095501-Tegel.jpg', 'Tegel keramik 40x40 cm', 11, 'm2', 65100.00, NULL, 3, '2020-02-12 16:55:01', '2020-02-12 16:55:01');

-- ----------------------------
-- Table structure for pir
-- ----------------------------
DROP TABLE IF EXISTS `pir`;
CREATE TABLE `pir`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `initiation` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `risk` int(11) NOT NULL,
  `tot_risk` int(11) NOT NULL DEFAULT 0,
  `tot_val` int(11) NOT NULL DEFAULT 0,
  `cat_val` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `cat_risk` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `path` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `rating` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `issue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0: new, 1: approved, 2: rejected, 3: postponed, 4: resubmitted',
  `reason` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `exec_by` int(11) NULL DEFAULT NULL,
  `modify_by` int(11) NOT NULL,
  `create_date` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `last_update` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pir
-- ----------------------------
INSERT INTO `pir` VALUES (13, 13, 8, 5, 70, 54, '<button type=\"button\" class=\"btn btn-success btn-rounded waves-effect waves-light\">Low Value</button>', '<button type=\"button\" class=\"btn btn-warning btn-rounded waves-effect waves-light\">Medium Risk</button>', '<button type=\"button\" class=\"btn btn-info waves-effect waves-light\">Normal Impact Project</button>', '2', 'Has no regulatory, legal or political risk ', 1, NULL, 3, 9, '2020-02-10 12:53:35', '2020-02-10 23:55:06');

-- ----------------------------
-- Table structure for risk_ev
-- ----------------------------
DROP TABLE IF EXISTS `risk_ev`;
CREATE TABLE `risk_ev`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_initiation` int(11) NOT NULL,
  `question` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of risk_ev
-- ----------------------------
INSERT INTO `risk_ev` VALUES (1, 8, '{\"1\":\"6\",\"2\":\"6\",\"3\":\"8\",\"4\":\"6\",\"5\":\"6\",\"6\":\"8\",\"7\":\"6\",\"8\":\"6\",\"9\":\"6\",\"10\":\"6\",\"11\":\"6\",\"12\":\"3\"}');
INSERT INTO `risk_ev` VALUES (2, 9, '{\"1\":\"6\",\"2\":\"8\",\"3\":\"6\",\"4\":\"6\",\"5\":\"6\",\"6\":\"6\",\"7\":\"6\",\"8\":\"6\",\"9\":\"6\",\"10\":\"8\",\"11\":\"6\",\"12\":\"8\"}');
INSERT INTO `risk_ev` VALUES (3, 11, '{\"1\":\"6\",\"2\":\"6\",\"3\":\"6\",\"4\":\"6\",\"5\":\"8\",\"6\":\"6\",\"7\":\"3\",\"8\":\"8\",\"9\":\"3\",\"10\":\"8\",\"11\":\"3\",\"12\":\"8\"}');
INSERT INTO `risk_ev` VALUES (4, 12, '{\"1\":\"6\",\"2\":\"6\",\"3\":\"3\",\"4\":\"3\",\"5\":\"3\",\"6\":\"8\",\"7\":\"6\",\"8\":\"8\",\"9\":\"8\",\"10\":\"8\",\"11\":\"8\",\"12\":\"8\"}');
INSERT INTO `risk_ev` VALUES (5, 13, '{\"1\":\"6\",\"2\":\"6\",\"3\":\"3\",\"4\":\"6\",\"5\":\"6\",\"6\":\"8\",\"7\":\"6\",\"8\":\"6\",\"9\":\"6\",\"10\":\"8\",\"11\":\"3\",\"12\":\"6\"}');

-- ----------------------------
-- Table structure for risk_quest
-- ----------------------------
DROP TABLE IF EXISTS `risk_quest`;
CREATE TABLE `risk_quest`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `descr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `option` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of risk_quest
-- ----------------------------
INSERT INTO `risk_quest` VALUES (1, 'Is there a risk associated with a lack of financial resources and or tight project schedule ? ', 'Evaluate the risk of whether the financial resouces to support the project is sufficient and whether the timeline projected for the project is reasonable ', '{\"There is sufficient financial resources that can be allocated to the project  AND The time that can be allocated is realistic \" :3,\"There is sufficient financial resources that can be allocated to the project BUT no enough time can be allocated \" :6,\"Financial resources are insufficient AND no enough time can be allocated \" :8}');
INSERT INTO `risk_quest` VALUES (2, 'Do you foresee any complexity in terms of implementing the project ? ', 'Evaluate the complexity of the project In terms of planning, execution and control. This relates to the variuos project management process. ', '{\"Project planning, executions and control are straightforward \" :3,\"Project planning, execution and control are moderately difficult \" :6,\"Project planning, execution and control are complicated \" :8}');
INSERT INTO `risk_quest` VALUES (3, 'Has Berau Coal undertaken similar projects before or key project personnel have relevant background experience ? ', 'Evaluate if the company has the relevant expertise to ensure that the implementations of the project would be successful or engagement of external resources is crucial ', '{\"Similar project / technology have previously been realised / implemented \" :3,\"Certain similar elements have been realised / implemented in previous projects \" :6,\"No similar project / technology have been realised / implemented in the past \" :8}');
INSERT INTO `risk_quest` VALUES (4, 'Is there any complexity in implementing this project ? ', 'Evaluate if this project involves high level of technical expertise and whether the succsess oh these techical aspect will have a big impact on success of project ', '{\"No technical system modification is required to realise the project \" :3,\"Technical system modification is required AND/OR software, hardware or network are complex to implement \" :6,\"Technical system modification is required AND software, hardware and network are complex to implement \" :8}');
INSERT INTO `risk_quest` VALUES (5, 'How long do you estimate the project duration to be like ? ', 'Determine the duration that this project will require ', '{\"Less than 6 months \" :3,\"Between 6-12 months \" :6,\"More than 12 months \" :8}');
INSERT INTO `risk_quest` VALUES (6, 'Do you foresee any limitation in the availability of skilled resources for this project ? ', 'Evaluate if current human resources within the company is sufficient to handle the project or external resources might be required to handle it ', '{\"Resouce demend can be fully met by internal resource supply \" :3,\"Resource demand can be met by internal resource supply (> 80%) AND external resource are available \" :6,\"Resource demend can NOT be met by internal resource supply (>80 %) AND external resource are scarce \" :8}');
INSERT INTO `risk_quest` VALUES (7, 'Is the project success depend on other project / external parties ? ', 'Evaluate the risk associated with the interdependencies with other projects / external parties ', '{\"There are no external dependencies for this project \" :3,\"There are weak dependencies or other project are dependent on the successful realisation of this project \" :6,\"Depend on the successful realisation of other project or external parties \" :8}');
INSERT INTO `risk_quest` VALUES (8, 'Do you anticipate that key stakeholders will have any concern regarding this project ? ', 'Evaluate if there is possibility of any risk associated with the alignment of stakeholders, such as misalignment of objective, working style, political opinions, and support to the project ', '{\"The project has the  support of key stakeholders \" :3,\"The project has moderate support of Berau Coal stakeholders / Key stakeholders are indifferent to the project success \" :6,\"Key stakeholders are un supportive of the project \" :8}');
INSERT INTO `risk_quest` VALUES (9, 'Do you anticipate that there will be a lot of changes that area required in the business / operations upon completion of the project ? ', 'Determine the degree of change required after project completion with regards to work methods, processes, procedures and training ', '{\"Little change which will be easily implemented and accepted by the business / operations \" :3,\"Moderate change that will be challenging to implement and components of the change will be  accepted by the bussiness / operations \" :6,\"High degree of change will be difficult to implement and not easily accepted by the business / operations \" :8}');
INSERT INTO `risk_quest` VALUES (10, 'Do you foresee any organizational changes within Berau Coal that will have an impact on the project ? ', 'Evaluate of risk that may result from other organizational changes that could concurrently occur ', '{\"The actual organizational climate is stable AND the projected climate willl remain stable \" :3,\"The actual organizational climate is stable BUT the projected climate won\'t be \" :6,\"The actual organizational climate is unstable AND the projected climate will remain unstable \" :8}');
INSERT INTO `risk_quest` VALUES (11, 'Is the project likely to ecounter any regulatory, political, and/or legal issue as a result of its implementations ? ', 'Evaluate the regulatory, legal and political risk associated with the implementations of the project. This include different laws, operational regulations and the political contex. ', '{\"Has no regulatory, legal or political risk \" :3,\"Has a regulatory and/or legal and/or political risk, but extensive analysis has been performed and a response plan has been developed. \" :6,\"Has significant regulatory / legal / political risk that are difficult to mitigate \" :8}');
INSERT INTO `risk_quest` VALUES (12, 'Do you foresee that Berau Coal brand or market reputation might be affected by the project ? ', 'Assess the impact of the project on the Berau Coal brand equity and reputation in the marketplace ', '{\"Positive impact on Berau Coal brand or reputation \" :3,\"Some negative perceived impact on Berau Coal brand or reputation \" :6,\"Has significant negative impact on Berau Coal brand or reputation \" :8}');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `role` int(2) NOT NULL DEFAULT 2,
  `menu` int(2) NOT NULL DEFAULT 3 COMMENT '0:admin, 1:material, 2:pir, 3:uncategory',
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `updated` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (3, 'New Admin', 'admin123', 'cXdlcnR5dWlvcA==', '0811112222333', 1, 0, 'test@gmail.com', '2019-07-18 14:34:07', '2019-07-28 10:38:51');
INSERT INTO `user` VALUES (9, 'Herlan', 'acil', 'MTIzNDU2Nzg5', '08123456789', 2, 2, 'herlan@mailinator.com', '2020-01-31 00:01:13', '2020-02-10 08:31:34');
INSERT INTO `user` VALUES (10, 'material user', 'material', 'cXdlcnR5dWlvcA==', '08123456789', 2, 1, 'herlan@mailinator.com', '2020-01-31 00:01:13', '2020-02-11 10:04:15');
INSERT INTO `user` VALUES (11, 'staff PIR', 'staffpir', 'cXdlcnR5dWlvcA==', '5039349213', 2, 2, 'staff.fk@mailinator.com', '2020-02-10 10:22:48', '2020-02-11 10:05:33');

-- ----------------------------
-- Table structure for value_ev
-- ----------------------------
DROP TABLE IF EXISTS `value_ev`;
CREATE TABLE `value_ev`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_initiation` int(11) NOT NULL,
  `question` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of value_ev
-- ----------------------------
INSERT INTO `value_ev` VALUES (1, 6, '{\"1\":\"4\",\"2\":\"33\",\"3\":\"3\",\"4\":\"3\",\"5\":\"3\",\"6\":\"3\",\"7\":\"3\",\"8\":\"1\",\"9\":\"4\",\"10\":\"3\",\"11\":\"3\",\"12\":\"3\",\"13\":\"3\",\"14\":\"4\"}');
INSERT INTO `value_ev` VALUES (2, 6, '{\"1\":\"4\",\"2\":\"33\",\"3\":\"3\",\"4\":\"3\",\"5\":\"3\",\"6\":\"3\",\"7\":\"3\",\"8\":\"1\",\"9\":\"4\",\"10\":\"3\",\"11\":\"3\",\"12\":\"3\",\"13\":\"3\",\"14\":\"4\"}');
INSERT INTO `value_ev` VALUES (3, 6, '{\"1\":\"4\",\"2\":\"33\",\"3\":\"3\",\"4\":\"3\",\"5\":\"3\",\"6\":\"3\",\"7\":\"3\",\"8\":\"1\",\"9\":\"4\",\"10\":\"3\",\"11\":\"3\",\"12\":\"3\",\"13\":\"3\",\"14\":\"4\"}');
INSERT INTO `value_ev` VALUES (4, 8, '{\"1\":\"3\",\"2\":\"33\",\"3\":\"3\",\"4\":\"3\",\"5\":\"1\",\"6\":\"3\",\"7\":\"3\",\"8\":\"3\",\"9\":\"3\",\"10\":\"3\",\"11\":\"3\",\"12\":\"1\",\"13\":\"1\",\"14\":\"1\"}');
INSERT INTO `value_ev` VALUES (5, 9, '{\"1\":\"4\",\"2\":\"33\",\"3\":\"3\",\"4\":\"1\",\"5\":\"4\",\"6\":\"3\",\"7\":\"4\",\"8\":\"3\",\"9\":\"3\",\"10\":\"3\",\"11\":\"4\",\"12\":\"3\",\"13\":\"4\",\"14\":\"4\"}');
INSERT INTO `value_ev` VALUES (6, 11, '{\"1\":\"3\",\"2\":\"33\",\"3\":\"3\",\"4\":\"4\",\"5\":\"4\",\"6\":\"1\",\"7\":\"3\",\"8\":\"1\",\"9\":\"3\",\"10\":\"4\",\"11\":\"1\",\"12\":\"4\",\"13\":\"3\",\"14\":\"3\"}');
INSERT INTO `value_ev` VALUES (7, 12, '{\"1\":\"1\",\"2\":\"33\",\"3\":\"3\",\"4\":\"3\",\"5\":\"1\",\"6\":\"3\",\"7\":\"1\",\"8\":\"4\",\"9\":\"1\",\"10\":\"1\",\"11\":\"4\",\"12\":\"1\",\"13\":\"3\",\"14\":\"1\"}');
INSERT INTO `value_ev` VALUES (8, 13, '{\"1\":\"4\",\"2\":\"17\",\"3\":\"1\",\"4\":\"4\",\"5\":\"3\",\"6\":\"1\",\"7\":\"3\",\"8\":\"3\",\"9\":\"1\",\"10\":\"3\",\"11\":\"3\",\"12\":\"4\",\"13\":\"3\",\"14\":\"4\"}');
INSERT INTO `value_ev` VALUES (9, 12, '{\"1\":\"3\",\"2\":\"33\",\"3\":\"1\",\"4\":\"4\",\"5\":\"3\",\"6\":\"1\",\"7\":\"3\",\"8\":\"3\",\"9\":\"3\",\"10\":\"3\",\"11\":\"3\",\"12\":\"3\",\"13\":\"3\",\"14\":\"3\"}');

-- ----------------------------
-- Table structure for value_quest
-- ----------------------------
DROP TABLE IF EXISTS `value_quest`;
CREATE TABLE `value_quest`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `descr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `option` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of value_quest
-- ----------------------------
INSERT INTO `value_quest` VALUES (1, 'What is the estimate quntified benefit (Net Present Value : NPV) that this project is expected to yield ? ', 'The NPV will provide an estimation over the financial benefit that the project can bring to the company ', '{\"Financial benefits < USD 1 Million \" :1,\"Financial benefit beetween USD 1-5 Million \" :3,\"Financial benefit ≥ 5 Million \" :4}');
INSERT INTO `value_quest` VALUES (2, 'What is the projected amount of CAPEX required for this project ? ', 'The amount of CAPEX specified must include all capital requirements which has an impact on both the balance sheet and cash flow ', '{\"CAPEX required ≥ USD 250 thousand \" :17,\"CAPEX required USD 100 - 250 thousand \" :33,\"CAPEX required < USD 100 thousand \" :50}');
INSERT INTO `value_quest` VALUES (3, 'How long will it take for Berau Coal to benefit from the project ? ', 'The evaluations of the speed with which the project generates benefits, both financial and non financial. It should be counted from the start of the spending till when benefits are first realized ', '{\"Payback period is more than 24 month  \" :1,\"Payback period is less than 24 month \" :3,\"Payback period is less than 12 month \" :4}');
INSERT INTO `value_quest` VALUES (4, 'Does this project align with company goals ? ', 'The project must be checked againts the goals set by the company. The extend of alignment should be evaluated as well ', '{\"Project benefit not aligned to company goals \" :1,\"Project benefit partially aligned to company goals \" :3,\"Project benefit fully aligned to company goals \" :4}');
INSERT INTO `value_quest` VALUES (5, 'Do you foresee any complexity in the implementations of the project ? ', 'The project must be evaluated againts the speed and/or complexity with which it can be completed ', '{\"High compelxity / Project durations > 12 month \" :1,\"Medium complexity / Project durations 4 -12 month \" :3,\"Low complexity / Project durations < 4 month \" :4}');
INSERT INTO `value_quest` VALUES (6, 'Would the rejection of the project cause any potentials loss to Berau Coal ? ', 'To evaluate the potentials loss that would result due to the non - realization of the project. Only the cost incurred by the status quo should be taken into account  ', '{\"Impact would be negligible \" :1,\"Impact would result in minor loss of financial benefit / minor loss of intangible value \" :3,\"Impact would result in loss of important financial benefit / Non compliance with regulation \" :4}');
INSERT INTO `value_quest` VALUES (7, 'The project needs to be impplemented within a spesific time frame in order for the benefits to be realized ? ', 'To evaluate the window of opportunity in which the project has. This would highlight the urgency of the project in terms of implementations which would allow it to capture as much value as possible ', '{\"The project start in ≥ 12 month with no significant impact on the benefits \" :1,\"The project needs to start in < 12 month or there will be a minor of impact on the benefits \" :3,\"The project need to start in < 3 month or there will be a major impact on the benefits \" :4}');
INSERT INTO `value_quest` VALUES (8, 'Does the completion of this project have an impact on the other project ? ', 'To evaluate the immportance on this project in relations to other ongoing projects or pending projects. This would further highlight the project in terms of enabling other project ', '{\"Does not enable any other project \" :1,\"Enables the implementations of the project that are expected to create moderate value to Berau Coal \" :3,\"Enables the implementation of project that are expected to create important value to Berau Coal \" :4}');
INSERT INTO `value_quest` VALUES (9, 'Does the completion of this project have an impact on Berau Coal Bussiness Processes ? ', 'To evaluate the impact of project implementation, On whether it woild affect the current bussiness processes and the extend of its impact ', '{\"Little change would be easily implemented and accepted by the bussiness \" :1,\"Contributes to the improvement of the main infrastructure processes of the bussiness\" :3,\"Contributes the improvement of the core bussiness processes \" :4}');
INSERT INTO `value_quest` VALUES (10, 'Are the opportunities for synergies with other project that can increase financial benefits or improve efficiency / effectiveness ? ', 'To identify the synergies that this project can achieve with other project and evaluate the possibility of increasing financial benefits (cost saving) / improving efficiency / effectiveness ', '{\"No or Low opportunity for synergies (<10 % increase in total benefits) \" :1,\"Medium opportunity for synergies (between 10% - 30% Increase in total benefits) \" :3,\"High opportunity for synergies (≥ 30% increase in total benefits) \" :4}');
INSERT INTO `value_quest` VALUES (11, 'Does the project have an impact on the environtment ? ', 'This will provide an overview of whether the project will have an impact on stakeholders like affect the quality of work environtment, safety of the operating environtment and Etc ', '{\"Cretae the negative impact on the environtment \" :1,\"Has No Impact or a neutral impact on the environtment \" :3,\"Create positive impact on the environtment \" :4}');
INSERT INTO `value_quest` VALUES (12, 'Does this project align with Berau Coal Value or Have an impact on the reputation of Berau Coal ? ', 'To evaluate whether project align with cultural and corporate value, as well as contribution to overall corporate reputation. (Safety first, Profit Through Progress, Openness, Respect and Recognition, and Professionalism and Personal Integrity) ', '{\"Negatively impact the values or the reputation of Berau Coal \" :1,\"Has no impact or a neutral impact on the values or the reputations of Berau Coal \" :3,\"Positively support the values or the reputation of Berau Coal \" :4}');
INSERT INTO `value_quest` VALUES (13, 'Does the project contribute to Berau Coal compliance with current or future regulatory requirements ? ', 'To evaluate the necessity of the project to help the company comply with current or future regulatory requirements ', '{\"It helps in ensuring that Berau Coal operates in accordance with industry best practices \" :1,\"It helps in complying with impending regulatory requirements \" :3,\"It is required to comply with current regulatory requirements \" :4}');
INSERT INTO `value_quest` VALUES (14, 'How would this project contribute to costomer satisfaction ? ', 'To identify the factors of customer satisfaction and to determine to what extend does this project contribute to the costomer satisfaction ', '{\"Successful project implementations indirectly contributes to improvement in customer satisfaction \" :1,\"Successful project implementation directly contributes to un-measurable improvement in customer satisfaction \" :3,\"Successful project implementation directly contributes to measurable improvement in customer satisfaction \" :4}');

-- ----------------------------
-- View structure for vw_pir
-- ----------------------------
DROP VIEW IF EXISTS `vw_pir`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `vw_pir` AS select `a`.`status` AS `status`,`e`.`reject_reason` AS `reject_reason`,`b`.`descr` AS `descr`,`a`.`id` AS `id`,`a`.`initiation` AS `id_initiation`,`b`.`type` AS `type`,`b`.`time_frame_start` AS `time_frame_start`,`b`.`time_frame_end` AS `time_frame_end`,`b`.`cost` AS `cost`,`b`.`impact` AS `impact`,`b`.`impact_etc` AS `impact_etc`,`b`.`constraint` AS `constraint`,`a`.`path` AS `path`,`a`.`rating` AS `rating`,`a`.`issue` AS `issue`,`c`.`nama` AS `nama`,`c`.`role` AS `role`,`c`.`id` AS `id_user`,`d`.`nama` AS `exec_by`,`a`.`create_date` AS `create_date` from ((((`pir` `a` left join `vw_pir_detail` `e` on((`e`.`id` = `a`.`initiation`))) left join `user` `d` on((`a`.`exec_by` = `d`.`id`))) left join `initiation` `b` on((`a`.`initiation` = `b`.`id`))) left join `user` `c` on((`a`.`modify_by` = `c`.`id`))) order by `a`.`create_date` desc;

-- ----------------------------
-- View structure for vw_pir_detail
-- ----------------------------
DROP VIEW IF EXISTS `vw_pir_detail`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `vw_pir_detail` AS select `b`.`id` AS `id`,`b`.`descr` AS `descr`,`b`.`category` AS `category`,`b`.`location` AS `location`,`b`.`type` AS `type`,`b`.`department` AS `department`,`b`.`sponsor` AS `sponsor`,`b`.`owner` AS `owner`,`b`.`manager` AS `manager`,`b`.`mandatory` AS `mandatory`,`b`.`urgent` AS `urgent`,`b`.`connect` AS `connect`,`b`.`reason` AS `reason`,`b`.`reason_descr` AS `reason_descr`,`b`.`project_background` AS `project_background`,`b`.`project_descr` AS `project_descr`,`b`.`expenditure` AS `expenditure`,`b`.`annual_budget` AS `annual_budget`,`b`.`estimate_budget` AS `estimate_budget`,`b`.`project_potential` AS `project_potential`,`b`.`in_scope` AS `in_scope`,`b`.`out_scope` AS `out_scope`,`b`.`deliver` AS `deliver`,`b`.`deliver_descr` AS `deliver_descr`,`b`.`start_date` AS `start_date`,`b`.`end_date` AS `end_date`,`b`.`milestones` AS `milestones`,`b`.`milestones_descr` AS `milestones_descr`,`b`.`constraint` AS `constraint`,`b`.`impact` AS `impact`,`b`.`impact_etc` AS `impact_etc`,`b`.`internal` AS `internal`,`b`.`external` AS `external`,`b`.`structure` AS `structure`,`b`.`structure_etc` AS `structure_etc`,`b`.`mechanical` AS `mechanical`,`b`.`mechanical_etc` AS `mechanical_etc`,`b`.`electrical` AS `electrical`,`b`.`electrical_etc` AS `electrical_etc`,`b`.`design` AS `design`,`b`.`cost` AS `cost`,`b`.`preliminary_design` AS `preliminary_design`,`b`.`preliminary_spec` AS `preliminary_spec`,`b`.`time_frame_start` AS `time_frame_start`,`b`.`time_frame_end` AS `time_frame_end`,`c`.`question` AS `val_ev`,`d`.`question` AS `risk_ev`,`e`.`descr` AS `nm_dept`,`a`.`rating` AS `rating`,`a`.`path` AS `path`,`f`.`nama` AS `nm_user`,`g`.`nama` AS `exec_by`,`a`.`reason` AS `reject_reason`,`a`.`tot_val` AS `tot_val`,`a`.`tot_risk` AS `tot_risk`,`a`.`cat_val` AS `cat_val`,`a`.`cat_risk` AS `cat_risk` from ((((((`pir` `a` left join `initiation` `b` on((`a`.`initiation` = `b`.`id`))) left join `value_ev` `c` on((`a`.`value` = `c`.`id`))) left join `risk_ev` `d` on((`a`.`risk` = `d`.`id`))) left join `department` `e` on((`b`.`department` = `e`.`id`))) left join `user` `f` on((`a`.`modify_by` = `f`.`id`))) left join `user` `g` on((`a`.`exec_by` = `g`.`id`)));

SET FOREIGN_KEY_CHECKS = 1;
