/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100408
 Source Host           : localhost:3306
 Source Schema         : web-ahs

 Target Server Type    : MySQL
 Target Server Version : 100408
 File Encoding         : 65001

 Date: 13/02/2020 00:41:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for kelompok
-- ----------------------------
DROP TABLE IF EXISTS `kelompok`;
CREATE TABLE `kelompok`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `descr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `modify_by` int(10) NOT NULL,
  `create_date` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `last_update` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kelompok
-- ----------------------------
INSERT INTO `kelompok` VALUES (1, 'Bahan Batu', 3, '2020-01-30 19:59:48', '2020-01-30 23:05:00');
INSERT INTO `kelompok` VALUES (2, 'Bahan Besi', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (3, 'Bahan Buis Beton', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (4, 'Bahan Elektrikal', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (5, 'Bahan Elektrikal Gedung', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (6, 'Bahan Gorong-gorong', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (7, 'Bahan Kaca', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (8, 'Bahan Kapur', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (9, 'Bahan Kayu', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (10, 'Bahan Lain-lain', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (11, 'Bahan Lantai', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (12, 'Bahan Pasir', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (13, 'Bahan Paving Stone', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (14, 'Bahan Pengecatan + Politur', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (15, 'Bahan Penggantung + Kunci', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (16, 'Bahan Penutup Atap', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (17, 'Bahan Pipa Besi', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (18, 'Bahan Piva PVC', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (19, 'Bahan Pondasi', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (20, 'Bahan Sanitair', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (21, 'Bahan Semen / P.C', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');
INSERT INTO `kelompok` VALUES (22, 'Bahan Tripleks / Kayu Lapis', 3, '2020-01-30 19:59:48', '2020-01-30 22:54:05');

SET FOREIGN_KEY_CHECKS = 1;
