/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100408
 Source Host           : localhost:3306
 Source Schema         : web-ahs

 Target Server Type    : MySQL
 Target Server Version : 100408
 File Encoding         : 65001

 Date: 13/02/2020 00:48:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sub_pekerjaan
-- ----------------------------
DROP TABLE IF EXISTS `sub_pekerjaan`;
CREATE TABLE `sub_pekerjaan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pekerjaan` int(11) NULL DEFAULT NULL,
  `descr` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sub_pekerjaan
-- ----------------------------
INSERT INTO `sub_pekerjaan` VALUES (1, 1, 'MOBILISASI ALAT & MATERIAL ');
INSERT INTO `sub_pekerjaan` VALUES (2, 3, 'PEKERJAAN STRUKTUR BETON');
INSERT INTO `sub_pekerjaan` VALUES (3, 3, 'PEKERJAAN PASANGAN');

SET FOREIGN_KEY_CHECKS = 1;
