/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100408
 Source Host           : localhost:3306
 Source Schema         : web-ahs

 Target Server Type    : MySQL
 Target Server Version : 100408
 File Encoding         : 65001

 Date: 13/02/2020 00:42:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pekerjaan
-- ----------------------------
DROP TABLE IF EXISTS `pekerjaan`;
CREATE TABLE `pekerjaan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descr` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pekerjaan
-- ----------------------------
INSERT INTO `pekerjaan` VALUES (1, 'PEKERJAAN PERSIAPAN');
INSERT INTO `pekerjaan` VALUES (2, 'MOBILISASI ALAT & MATERIAL');
INSERT INTO `pekerjaan` VALUES (3, 'PEKERJAAN TANAH DAN PONDASI');
INSERT INTO `pekerjaan` VALUES (4, 'PEKERJAAN STRUKTUR BETON');
INSERT INTO `pekerjaan` VALUES (5, 'PEKERJAAN RANGKA ATAP DAN PLAFOND');
INSERT INTO `pekerjaan` VALUES (6, 'PEKERJAAN KUSEN / PINTU/ JENDELA / VENTILASI');
INSERT INTO `pekerjaan` VALUES (7, 'PEKERJAAN KUNCI DAN ALAT PENGGANTUNG');
INSERT INTO `pekerjaan` VALUES (8, 'PEKERJAAN MEKANIKAL/ELEKTRIKAL');
INSERT INTO `pekerjaan` VALUES (9, 'PEKERJAAN SANITAIR');
INSERT INTO `pekerjaan` VALUES (10, 'PEKERJAAN CAT-CATAN');
INSERT INTO `pekerjaan` VALUES (11, 'PEKERJAAN LAIN-LAIN');

SET FOREIGN_KEY_CHECKS = 1;
