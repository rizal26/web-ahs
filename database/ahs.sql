/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100408
 Source Host           : localhost:3306
 Source Schema         : web-ahs

 Target Server Type    : MySQL
 Target Server Version : 100408
 File Encoding         : 65001

 Date: 13/02/2020 00:42:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ahs
-- ----------------------------
DROP TABLE IF EXISTS `ahs`;
CREATE TABLE `ahs`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pekerjaan` int(11) NULL DEFAULT NULL,
  `id_sub_pekerjaan` int(11) NULL DEFAULT NULL,
  `id_sub_sub_pekerjaan` int(11) NULL DEFAULT NULL,
  `descr` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `satuan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `harga` decimal(20, 2) NULL DEFAULT NULL,
  `old_harga` decimal(20, 2) NULL DEFAULT NULL,
  `modify_by` int(11) NULL DEFAULT NULL,
  `create_date` timestamp(0) NULL DEFAULT current_timestamp(0),
  `last_update` timestamp(0) NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
