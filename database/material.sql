/*
 Navicat Premium Data Transfer

 Source Server         : 125
 Source Server Type    : MySQL
 Source Server Version : 100038
 Source Host           : 128.199.133.125:3306
 Source Schema         : web-ahs

 Target Server Type    : MySQL
 Target Server Version : 100038
 File Encoding         : 65001

 Date: 13/02/2020 18:09:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for material
-- ----------------------------
DROP TABLE IF EXISTS `material`;
CREATE TABLE `material`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `foto` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `descr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `kelompok` int(10) NOT NULL,
  `satuan` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `harga` decimal(20, 2) NOT NULL,
  `old_harga` decimal(20, 2) NULL DEFAULT NULL,
  `modify_by` int(10) NOT NULL,
  `create_date` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `last_update` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 169 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of material
-- ----------------------------
INSERT INTO `material` VALUES (8, '110220055537-1596605_302ec8d2-584e-4467-a47e-38df04fe5b11.jpg', 'Pasir Urug (Lokal)', 12, 'm3', 209000.00, 180900.00, 3, '2020-02-11 12:55:37', '2020-02-11 13:37:20');
INSERT INTO `material` VALUES (9, '110220060031-1596605_302ec8d2-584e-4467-a47e-38df04fe5b11.jpg', 'Pasir Pasang  (Lokal)', 12, 'm3', 180900.00, NULL, 3, '2020-02-11 13:00:31', '2020-02-11 13:00:31');
INSERT INTO `material` VALUES (10, '110220060319-download.jpg', 'Pasir Halus', 12, 'm3', 273000.00, 236.25, 3, '2020-02-11 13:03:19', '2020-02-11 13:34:42');
INSERT INTO `material` VALUES (11, '110220060641-pasir-beton.jpg', 'Pasir Beton (Ex.Palu)', 12, 'm3', 582000.00, 503550.00, 3, '2020-02-11 13:06:41', '2020-02-11 13:33:13');
INSERT INTO `material` VALUES (12, '110220061301-pasir-urug.jpg', 'Sirtu urug', 12, 'm3', 702000.00, 209000.00, 3, '2020-02-11 13:13:01', '2020-02-11 13:35:41');
INSERT INTO `material` VALUES (13, '110220061828-Sirtu-Alam.jpg', 'Sirtu Gunung', 12, 'm3', 149000.00, 149000.00, 3, '2020-02-11 13:18:28', '2020-02-11 13:36:22');
INSERT INTO `material` VALUES (14, '110220064019-images.jpg', 'Tanah urug', 12, 'm3', 117000.00, NULL, 3, '2020-02-11 13:40:19', '2020-02-11 13:40:19');
INSERT INTO `material` VALUES (15, '110220064255-Sirtu-Alam.jpg', 'Pasir batu', 12, 'm3', 513000.00, NULL, 3, '2020-02-11 13:42:55', '2020-02-11 13:42:55');
INSERT INTO `material` VALUES (16, '110220065056-Batu_pasang.jpg', 'Batu pasang', 1, 'm3', 621000.00, NULL, 3, '2020-02-11 13:50:56', '2020-02-11 13:50:56');
INSERT INTO `material` VALUES (17, '110220065800-Batu_pecah_1520.jpg', 'Batu pecah 15/20', 1, 'm3', 621000.00, NULL, 3, '2020-02-11 13:58:00', '2020-02-11 13:58:00');
INSERT INTO `material` VALUES (18, '110220065800-Batu_pecah_1520.jpg', 'Batu pecah 15/20', 1, 'm3', 621000.00, NULL, 3, '2020-02-11 13:58:00', '2020-02-11 13:58:00');
INSERT INTO `material` VALUES (19, '110220070205-batu_kali_quary.jpg', 'Batu kali/Batu quary', 1, 'm3', 381000.00, NULL, 3, '2020-02-11 14:02:05', '2020-02-11 14:02:05');
INSERT INTO `material` VALUES (20, '110220070548-Batu_pecah_kali_710.jpg', 'Batu kali pecah 7/10 (tangan)', 1, 'm3', 621000.00, NULL, 3, '2020-02-11 14:05:48', '2020-02-11 14:05:48');
INSERT INTO `material` VALUES (21, '110220070823-makadam-5--7-cm.jpg', 'Batu pecah 5/7 (tangan)', 1, 'm3', 621000.00, 621000.00, 3, '2020-02-11 14:08:23', '2020-02-11 14:14:00');
INSERT INTO `material` VALUES (22, '110220071314-Batu_4-5.jpg', 'Batu kali pecah 4/5 (mesin)', 1, 'm3', 621000.00, 621000.00, 3, '2020-02-11 14:13:14', '2020-02-11 14:14:23');
INSERT INTO `material` VALUES (23, '110220071638-Batu_pecah_2-3.jpg', 'Batu kali pecah 2/3 (mesin)', 1, 'm3', 690000.00, NULL, 3, '2020-02-11 14:16:38', '2020-02-11 14:16:38');
INSERT INTO `material` VALUES (24, '110220072119-Batu_pecah_0.5-1.jpg', 'Batu kali pecah 0.5-1 (mesin', 1, 'm3', 690000.00, NULL, 3, '2020-02-11 14:21:19', '2020-02-11 14:21:19');
INSERT INTO `material` VALUES (25, '110220072348-batu_1-2.jpg', 'Batu pecah 1/2 (mesin)', 1, 'm3', 690000.00, NULL, 3, '2020-02-11 14:23:48', '2020-02-11 14:23:48');
INSERT INTO `material` VALUES (26, '110220072929-laston.jpg', 'Batu pecah tersaring untuk laston', 1, 'm3', 690000.00, NULL, 3, '2020-02-11 14:29:29', '2020-02-11 14:29:29');
INSERT INTO `material` VALUES (27, '110220073353-batu_LPA.jpg', 'Batu pecah tersaring untuk LPA', 1, 'm3', 690000.00, NULL, 3, '2020-02-11 14:33:53', '2020-02-11 14:33:53');
INSERT INTO `material` VALUES (28, '110220074626-LPB.jpg', 'Batu pecah tersaring untuk LPB', 1, 'm3', 690000.00, 690000.00, 3, '2020-02-11 14:46:26', '2020-02-11 14:47:00');
INSERT INTO `material` VALUES (29, '110220081620-batu_pecah.jpg', 'Batu pecah', 1, 'm3', 690000.00, NULL, 3, '2020-02-11 15:16:20', '2020-02-11 15:16:20');
INSERT INTO `material` VALUES (30, '110220081923-batu_padas.jpg', 'Batu padas', 1, 'm3', 381000.00, 381000.00, 3, '2020-02-11 15:19:23', '2020-02-11 15:31:59');
INSERT INTO `material` VALUES (31, '110220082212-batu_gunung.jpg', 'Batu gunung', 1, 'm3', 507000.00, NULL, 3, '2020-02-11 15:22:12', '2020-02-11 15:22:12');
INSERT INTO `material` VALUES (32, '110220082729-batu_koral.JPG', 'Batu koral (biasa)', 1, 'm3', 390000.00, NULL, 3, '2020-02-11 15:27:29', '2020-02-11 15:27:29');
INSERT INTO `material` VALUES (33, '110220083139-batu_alam_10-20.jpg', 'Batu alam 10/20', 1, 'm2', 312000.00, NULL, 3, '2020-02-11 15:31:39', '2020-02-11 15:31:39');
INSERT INTO `material` VALUES (34, '110220083714-batu_kerikil_cor.jpg', 'Batu kerikil cor ( 3 sd 4 cm', 1, 'm3', 567000.00, NULL, 3, '2020-02-11 15:37:14', '2020-02-11 15:37:14');
INSERT INTO `material` VALUES (35, '110220084147-Batu_kerikil_cor_palu.jpg', 'Batu kerikil cor palu', 1, 'm3', 665000.00, NULL, 3, '2020-02-11 15:41:47', '2020-02-11 15:41:47');
INSERT INTO `material` VALUES (36, '110220084501-koral_biasa.jpg', 'Batu kerikil/koral biasa', 1, 'm3', 367000.00, NULL, 3, '2020-02-11 15:45:01', '2020-02-11 15:45:01');
INSERT INTO `material` VALUES (37, '110220084719-koral_halus.jpg', 'batu kerikil/koral halus', 1, 'm3', 542000.00, NULL, 3, '2020-02-11 15:47:19', '2020-02-11 15:47:19');
INSERT INTO `material` VALUES (38, '110220092028-Koral_timbunan.jpg', 'Batu kerikil /koral timbunan(l0kal)', 1, 'm3', 457000.00, NULL, 3, '2020-02-11 16:20:28', '2020-02-11 16:20:28');
INSERT INTO `material` VALUES (39, '110220092313-Batu_tempel_hitam.jpg', 'Batu tempel hitam', 1, 'm2', 264000.00, NULL, 3, '2020-02-11 16:23:13', '2020-02-11 16:23:13');
INSERT INTO `material` VALUES (40, '110220092543-paliman.jpg', 'Batu tempel paliman 15x30', 1, 'm2', 284000.00, NULL, 3, '2020-02-11 16:25:43', '2020-02-11 16:25:43');
INSERT INTO `material` VALUES (41, '110220092739-paras.jpg', 'Batu tempel paras 15x30', 1, 'm2', 306000.00, NULL, 3, '2020-02-11 16:27:39', '2020-02-11 16:27:39');
INSERT INTO `material` VALUES (42, '110220092912-Abu_batu.jpg', 'Batu abu', 1, 'm3', 328000.00, NULL, 3, '2020-02-11 16:29:12', '2020-02-11 16:29:12');
INSERT INTO `material` VALUES (43, '110220093108-batu_bata.jpg', 'Batu bata (cetak tangan)', 1, 'Bh', 2200.00, 2200.00, 3, '2020-02-11 16:31:08', '2020-02-11 16:32:24');
INSERT INTO `material` VALUES (45, '110220093637-Batako.jpg', 'Bata beton 40x15x8 cm K225-17m2', 1, 'Bh', 6100.00, NULL, 3, '2020-02-11 16:36:38', '2020-02-11 16:36:38');
INSERT INTO `material` VALUES (46, '110220093809-Batako.jpg', 'Batu bata beton 30x15x8 cmK175-22M2', 1, 'Bh', 6300.00, NULL, 3, '2020-02-11 16:38:09', '2020-02-11 16:38:09');
INSERT INTO `material` VALUES (47, '110220093941-Batako.jpg', 'Batu bata beton 30x15x7 cm K175m2', 1, 'Bh', 6300.00, NULL, 3, '2020-02-11 16:39:41', '2020-02-11 16:39:41');
INSERT INTO `material` VALUES (48, '110220094208-Batako.jpg', 'Batako lubang 3-40x19x8,5cmK225-12m2', 1, 'Bh', 6300.00, NULL, 3, '2020-02-11 16:42:08', '2020-02-11 16:42:08');
INSERT INTO `material` VALUES (49, '110220094306-Batako.jpg', 'Batako lubang 3-40x20x8,5cmK225-12m2', 1, 'Bh', 6300.00, NULL, 3, '2020-02-11 16:43:06', '2020-02-11 16:43:06');
INSERT INTO `material` VALUES (50, '110220094516-Batako.jpg', 'Batako lubang 3-40x20x810 cmK225-12m2', 1, 'Bh', 6300.00, NULL, 3, '2020-02-11 16:45:16', '2020-02-11 16:45:16');
INSERT INTO `material` VALUES (51, '110220094735-Batako.jpg', 'Batako lubang 2-39x9x9cmK225', 1, 'Bh', 6300.00, NULL, 3, '2020-02-11 16:47:35', '2020-02-11 16:47:35');
INSERT INTO `material` VALUES (52, '120220015914-3_berlian.jpg', 'paving stone abu 2 tb 6 cm (3berlian DT1)', 13, 'Bj', 2000.00, NULL, 3, '2020-02-12 08:59:14', '2020-02-12 08:59:14');
INSERT INTO `material` VALUES (53, '120220022921-Semen_tiga_roda.jpg', 'Semen tiga roda 40 kg', 21, 'zak', 133000.00, NULL, 3, '2020-02-12 09:29:21', '2020-02-12 09:29:21');
INSERT INTO `material` VALUES (54, '120220023250-Semen_tonasa.jpg', 'Semen tonasa 40 kg', 21, 'zak', 100000.00, NULL, 3, '2020-02-12 09:32:50', '2020-02-12 09:32:50');
INSERT INTO `material` VALUES (55, '120220024014-Semen_tiga_roda.jpg', 'Semen 50 kg', 21, 'zak', 160000.00, NULL, 3, '2020-02-12 09:40:14', '2020-02-12 09:40:14');
INSERT INTO `material` VALUES (56, '120220024351-Semen_tiga_roda.jpg', 'semen 40 kg', 21, 'kg', 3400.00, NULL, 3, '2020-02-12 09:43:51', '2020-02-12 09:43:51');
INSERT INTO `material` VALUES (57, '120220024751-semen.jpg', 'Semen 50 kg', 21, 'kg', 320000.00, NULL, 3, '2020-02-12 09:47:51', '2020-02-12 09:47:51');
INSERT INTO `material` VALUES (58, '120220030226-Ready_mix.jpg', 'Beton fc 1453 Mpa setara K-175', 21, 'm3', 2080000.00, NULL, 3, '2020-02-12 10:02:26', '2020-02-12 10:02:26');
INSERT INTO `material` VALUES (59, '120220030357-Ready_mix.jpg', 'Beton fc 18,68 Mpa setara K-225', 21, 'm3', 2174000.00, NULL, 3, '2020-02-12 10:03:57', '2020-02-12 10:03:57');
INSERT INTO `material` VALUES (60, '120220030533-Ready_mix.jpg', 'Beton fc 20,75 Mpa setara K-250', 21, 'm3', 2232000.00, NULL, 3, '2020-02-12 10:05:33', '2020-02-12 10:05:33');
INSERT INTO `material` VALUES (61, '120220031006-Ready_mix.jpg', 'Beton fc 22,83 Mpa setara K-275', 21, 'm3', 2326000.00, NULL, 3, '2020-02-12 10:10:06', '2020-02-12 10:10:06');
INSERT INTO `material` VALUES (62, '120220031149-Ready_mix.jpg', 'Beton fc 24,9 Mpa setara k- 300', 21, 'm3', 2347000.00, NULL, 3, '2020-02-12 10:11:49', '2020-02-12 10:11:49');
INSERT INTO `material` VALUES (63, '120220031412-Ready_mix.jpg', 'Beton fc 29,05 Mpa setara K-350', 21, 'm3', 2442000.00, NULL, 3, '2020-02-12 10:14:12', '2020-02-12 10:14:12');
INSERT INTO `material` VALUES (64, '120220031559-Ready_mix.jpg', 'Beton fc 33,20 Mpa setara k-400', 21, 'm3', 2573000.00, NULL, 3, '2020-02-12 10:15:59', '2020-02-12 10:15:59');
INSERT INTO `material` VALUES (65, '120220033101-Ready_mix.jpg', 'Beton fc 37,35 Mpa setara K-450', 21, 'm3', 2704000.00, NULL, 3, '2020-02-12 10:31:01', '2020-02-12 10:31:01');
INSERT INTO `material` VALUES (66, '120220033206-Ready_mix.jpg', 'Beton fc 41,50 Mpa setara k-500', 21, 'm3', 2835000.00, NULL, 3, '2020-02-12 10:32:06', '2020-02-12 10:32:06');
INSERT INTO `material` VALUES (67, '120220055735-Kapur_bubuk.jpg', 'Kapur bubuk', 8, 'm3', 763000.00, NULL, 3, '2020-02-12 12:57:35', '2020-02-12 12:57:35');
INSERT INTO `material` VALUES (68, '120220055859-Kapur_bubuk.jpg', 'Kapur bubuk', 8, 'kg', 935000.00, NULL, 3, '2020-02-12 12:58:59', '2020-02-12 12:58:59');
INSERT INTO `material` VALUES (69, '120220060027-Kapur_bubuk.jpg', 'Kapur bubuk 1 sak', 8, 'zak', 7700.00, NULL, 3, '2020-02-12 13:00:27', '2020-02-12 13:00:27');
INSERT INTO `material` VALUES (70, '120220060135-Kapur_gamping.jpg', 'Kapur gamping', 8, 'kg', 3960.00, NULL, 3, '2020-02-12 13:01:35', '2020-02-12 13:01:35');
INSERT INTO `material` VALUES (71, '120220060428-kapur_padam.jpg', 'Kapur padam', 8, 'm3', 529000.00, NULL, 3, '2020-02-12 13:04:28', '2020-02-12 13:04:28');
INSERT INTO `material` VALUES (72, '120220064920-Tegel.jpg', 'Tegel ubin warna 20x20 cm', 11, 'Bh', 3400.00, NULL, 3, '2020-02-12 13:49:20', '2020-02-12 13:49:20');
INSERT INTO `material` VALUES (73, '120220065041-Tegel.jpg', 'Tegel ubin warna 30x30 cm', 11, 'Bh', 6800.00, NULL, 3, '2020-02-12 13:50:41', '2020-02-12 13:50:41');
INSERT INTO `material` VALUES (74, '120220065145-Tegel.jpg', 'Tegel ubin warna 40x40 cm', 11, 'Bh', 13500.00, NULL, 3, '2020-02-12 13:51:45', '2020-02-12 13:51:45');
INSERT INTO `material` VALUES (81, '120220092354-plint.jpg', 'Plint ubin pc abu-abu 10x30', 11, 'Bh', 9900.00, NULL, 3, '2020-02-12 16:23:54', '2020-02-12 16:23:54');
INSERT INTO `material` VALUES (82, '120220092843-Keramik.jpg', 'Tegel keramik 10x10 cm', 11, 'Bh', 6200.00, NULL, 3, '2020-02-12 16:28:44', '2020-02-12 16:28:44');
INSERT INTO `material` VALUES (83, '120220092956-Keramik.jpg', 'Tegel keramik 10x20', 11, 'Bh', 12500.00, NULL, 3, '2020-02-12 16:29:56', '2020-02-12 16:29:56');
INSERT INTO `material` VALUES (84, '120220093141-Keramik.jpg', 'Tegel keramik 10x40', 11, 'Bh', 21800.00, NULL, 3, '2020-02-12 16:31:41', '2020-02-12 16:31:41');
INSERT INTO `material` VALUES (85, '120220093235-Keramik.jpg', 'Tegel keramik 20x20', 11, 'Bh', 11700.00, NULL, 3, '2020-02-12 16:32:35', '2020-02-12 16:32:35');
INSERT INTO `material` VALUES (86, '120220093820-Keramik.jpg', 'tegel keramik 30x30 cm motif', 11, 'Bh', 10300.00, NULL, 3, '2020-02-12 16:38:20', '2020-02-12 16:38:20');
INSERT INTO `material` VALUES (87, '120220093926-Keramik.jpg', 'tegel keramik 30x30 cm biasa', 11, 'Bh', 9700.00, NULL, 3, '2020-02-12 16:39:26', '2020-02-12 16:39:26');
INSERT INTO `material` VALUES (88, '120220094027-Keramik.jpg', 'tegel keramik 40x40 cm motif', 11, 'Bh', 21500.00, NULL, 3, '2020-02-12 16:40:27', '2020-02-12 16:40:27');
INSERT INTO `material` VALUES (89, '120220094134-Keramik.jpg', 'Tegel keramik 40x40 cm biasa', 11, 'Bh', 20800.00, NULL, 3, '2020-02-12 16:41:34', '2020-02-12 16:41:34');
INSERT INTO `material` VALUES (90, '120220094704-paras.jpg', 'Tegel keramik 20x25 cm bang terpadu', 11, 'Bh', 5000.00, NULL, 3, '2020-02-12 16:47:04', '2020-02-12 16:47:04');
INSERT INTO `material` VALUES (91, '120220094859-Tegel.jpg', 'Tegel keramik 20x20 cm', 11, 'm2', 57000.00, NULL, 3, '2020-02-12 16:48:59', '2020-02-12 16:48:59');
INSERT INTO `material` VALUES (92, '120220095003-Tegel.jpg', 'Tegel keramik 25x25 cm', 11, 'm2', 57600.00, NULL, 3, '2020-02-12 16:50:03', '2020-02-12 16:50:03');
INSERT INTO `material` VALUES (93, '120220095058-Tegel.jpg', 'Tegel keramik 30x30 cm', 11, 'm2', 62600.00, NULL, 3, '2020-02-12 16:50:58', '2020-02-12 16:50:58');
INSERT INTO `material` VALUES (94, '120220095329-Tegel.jpg', 'Tegel keramik rock tile 30x30 cm', 11, 'm2', 65600.00, NULL, 3, '2020-02-12 16:53:29', '2020-02-12 16:53:29');
INSERT INTO `material` VALUES (95, '120220095501-Tegel.jpg', 'Tegel keramik 40x40 cm', 11, 'm2', 65100.00, NULL, 3, '2020-02-12 16:55:01', '2020-02-12 16:55:01');
INSERT INTO `material` VALUES (96, '130220121751-Keramik_dinding.jpg', 'keramik dinding 20x25 cm', 11, 'm2', 63000.00, NULL, 3, '2020-02-13 07:17:52', '2020-02-13 07:17:52');
INSERT INTO `material` VALUES (97, '130220121752-Keramik_dinding.jpg', 'keramik dinding 20x25 cm', 11, 'm2', 63000.00, NULL, 3, '2020-02-13 07:17:52', '2020-02-13 07:17:52');
INSERT INTO `material` VALUES (98, '130220121919-Keramik_dinding.jpg', 'Keramik dinding 25x25 cm', 11, 'm2', 71200.00, NULL, 3, '2020-02-13 07:19:19', '2020-02-13 07:19:19');
INSERT INTO `material` VALUES (99, '130220122030-Keramik_dinding.jpg', 'Keramik dinding 20x40 cm', 11, 'm2', 92400.00, NULL, 3, '2020-02-13 07:20:30', '2020-02-13 07:20:30');
INSERT INTO `material` VALUES (100, '130220122224-Keramik_dinding.jpg', 'Keramik dinding 20x60 cm', 11, 'm2', 121200.00, NULL, 3, '2020-02-13 07:22:24', '2020-02-13 07:22:24');
INSERT INTO `material` VALUES (101, '130220123600-vinil.jpg', 'Vinil oscar / sintetis Kw1', 11, 'm2', 83900.00, NULL, 3, '2020-02-13 07:36:00', '2020-02-13 07:36:00');
INSERT INTO `material` VALUES (102, '130220123700-vinil.jpg', 'Vinil karet', 11, 'm2', 57700.00, NULL, 3, '2020-02-13 07:37:00', '2020-02-13 07:37:00');
INSERT INTO `material` VALUES (103, '130220124006-vinil.jpg', 'Vinil/oscar/sintetis motif kembang', 11, 'm2', 101700.00, NULL, 3, '2020-02-13 07:40:06', '2020-02-13 07:40:06');
INSERT INTO `material` VALUES (104, '130220125018-Granit_vs_Marmer.jpg', 'Marmer biasa', 11, 'Bh', 940000.00, NULL, 3, '2020-02-13 07:50:18', '2020-02-13 07:50:18');
INSERT INTO `material` VALUES (105, '130220010030-Granit_vs_Marmer.jpg', 'Ubin teralux marmer 30x30 cm', 11, 'Bh', 46000.00, 46000.00, 3, '2020-02-13 08:00:30', '2020-02-13 08:02:44');
INSERT INTO `material` VALUES (106, '130220010230-Granit_vs_Marmer.jpg', 'Ubin teralux marmer 40x40 cm', 11, 'Bh', 86900.00, NULL, 3, '2020-02-13 08:02:30', '2020-02-13 08:02:30');
INSERT INTO `material` VALUES (107, '130220010942-Plint_marmer.jpg', 'Plint teralux marmer 10x30', 11, 'Bh', 16500.00, NULL, 3, '2020-02-13 08:09:42', '2020-02-13 08:09:42');
INSERT INTO `material` VALUES (108, '130220011053-Plint_marmer.jpg', 'Plint teralux 10x40 cm', 11, 'Bh', 17400.00, NULL, 3, '2020-02-13 08:10:53', '2020-02-13 08:10:53');
INSERT INTO `material` VALUES (109, '130220012632-Mozaik_porselen.jpg', 'Mozaik porselin 30x30 cm', 11, 'm2', 150000.00, NULL, 3, '2020-02-13 08:26:32', '2020-02-13 08:26:32');
INSERT INTO `material` VALUES (110, '130220013025-Mozaik_porselen.jpg', 'Ubin porselin putih lokal 11x11 cm', 11, 'Bh', 850.00, NULL, 3, '2020-02-13 08:30:25', '2020-02-13 08:30:25');
INSERT INTO `material` VALUES (111, '130220013201-Mozaik_porselen.jpg', 'Ubin porselin lokal 10x20 cm', 11, 'Bh', 4000.00, NULL, 3, '2020-02-13 08:32:01', '2020-02-13 08:32:01');
INSERT INTO `material` VALUES (112, '130220013833-Granit_vs_Marmer.jpg', 'Granite tile 30x30', 11, 'Bh', 38000.00, NULL, 3, '2020-02-13 08:38:33', '2020-02-13 08:38:33');
INSERT INTO `material` VALUES (113, '130220013930-Granit_vs_Marmer.jpg', 'Granite tile 40x40', 11, 'Bh', 84000.00, NULL, 3, '2020-02-13 08:39:30', '2020-02-13 08:39:30');
INSERT INTO `material` VALUES (114, '130220014020-Granit_vs_Marmer.jpg', 'Granite tile 60x60', 11, 'Bh', 173000.00, NULL, 3, '2020-02-13 08:40:20', '2020-02-13 08:40:20');
INSERT INTO `material` VALUES (115, '130220014520-plint.jpg', 'plint ubin Granite 10x30', 11, 'Bh', 16300.00, NULL, 3, '2020-02-13 08:45:20', '2020-02-13 08:45:20');
INSERT INTO `material` VALUES (116, '130220014622-plint.jpg', 'plint ubin granite 10x40', 11, 'Bh', 18000.00, NULL, 3, '2020-02-13 08:46:23', '2020-02-13 08:46:23');
INSERT INTO `material` VALUES (117, '130220015537-hollow_blok.jpg', 'Hollow blok', 11, 'Bh', 9400.00, NULL, 3, '2020-02-13 08:55:37', '2020-02-13 08:55:37');
INSERT INTO `material` VALUES (118, '130220015645-hollow_blok.jpg', 'Concrete blok CB 20', 11, 'Bh', 5500.00, NULL, 3, '2020-02-13 08:56:45', '2020-02-13 08:56:45');
INSERT INTO `material` VALUES (119, '130220015733-hollow_blok.jpg', 'Concrete blok CB 15', 11, 'Bh', 6300.00, NULL, 3, '2020-02-13 08:57:33', '2020-02-13 08:57:33');
INSERT INTO `material` VALUES (120, '130220015816-hollow_blok.jpg', 'Concrete blok CB 10', 11, 'Bh', 5700.00, NULL, 3, '2020-02-13 08:58:16', '2020-02-13 08:58:16');
INSERT INTO `material` VALUES (121, '130220020328-Bond_beam.jpg', 'Bondbeam 40x20x20 cm', 11, 'Bh', 9400.00, NULL, 3, '2020-02-13 09:03:28', '2020-02-13 09:03:28');
INSERT INTO `material` VALUES (122, '130220020609-Roster.jpg', 'Roster/Terawang 40x40x11', 11, 'Bh', 7400.00, NULL, 3, '2020-02-13 09:06:09', '2020-02-13 09:06:09');
INSERT INTO `material` VALUES (123, '130220020700-Roster.jpg', 'Roster/Terawang 12x11x24', 11, 'Bh', 6700.00, NULL, 3, '2020-02-13 09:07:00', '2020-02-13 09:07:00');
INSERT INTO `material` VALUES (124, '130220022807-1_tips_bikin_instalasi_listrik_di_rumah.jpg', 'Instalasi Listrik', 4, 'titik', 317000.00, NULL, 12, '2020-02-13 09:28:07', '2020-02-13 09:28:07');
INSERT INTO `material` VALUES (125, '130220022900-P103054542-1.jpg', 'Stop Kontak', 4, 'titik', 317000.00, NULL, 12, '2020-02-13 09:29:00', '2020-02-13 09:29:00');
INSERT INTO `material` VALUES (126, '130220023026-48370190_c621992a-509c-42bf-b86a-c40b9fa67483_1000_750.jpg', 'Lampu TL 1 x 10 w PHILIPS', 4, 'bh', 78000.00, NULL, 12, '2020-02-13 09:30:26', '2020-02-13 09:30:26');
INSERT INTO `material` VALUES (127, '130220023219-2788853_f61048a1-8982-47b1-9c21-14247122495b_700_700.jpg', 'Lampu TL 2 x 20 w PHILIPS', 4, 'bh', 156000.00, NULL, 12, '2020-02-13 09:32:19', '2020-02-13 09:32:19');
INSERT INTO `material` VALUES (128, '130220023317-5147911_3a381f90-e55b-4825-89e7-d64aeaf89559_700_700.jpg', 'Lampu TL 1 x 40 w PHILIPS', 4, 'bh', 117000.00, NULL, 12, '2020-02-13 09:33:17', '2020-02-13 09:33:17');
INSERT INTO `material` VALUES (129, '130220023406-64409731_90ea4c7c-9eb5-4f68-a8b5-808e6af71955_672_672.jpg', 'Lampu pijar 40 watt completed ( Fitting + Starter )', 4, 'bh', 195000.00, NULL, 12, '2020-02-13 09:34:06', '2020-02-13 09:34:06');
INSERT INTO `material` VALUES (130, '130220023538-jembo-nym-2x1-5-mm---kabel-listrik--100m--56838e8c95a1a.jpg', 'Kabel NYM 3x2.5 mm', 4, 'm', 29000.00, NULL, 12, '2020-02-13 09:35:38', '2020-02-13 09:35:38');
INSERT INTO `material` VALUES (131, '130220023626-272830_DS-Pipa-Conduit-UPVC-2-9m-20mm_84eMRGRtoecfprSF_1555595991.jpg', 'Pipa konduit uPVC 20 mm', 4, 'PC', 19000.00, NULL, 12, '2020-02-13 09:36:26', '2020-02-13 09:36:26');
INSERT INTO `material` VALUES (132, '130220023753-Electrical-Ceramic-Insulators-Black-PVC-Inflaming-Retarding.jpg_350x350.jpg', 'Tape Isolator', 4, 'bh', 18000.00, NULL, 12, '2020-02-13 09:37:53', '2020-02-13 09:37:53');
INSERT INTO `material` VALUES (133, '130220023917-8415c8c79ace64e1cc6d1b0f1a980063.jfif', 'Lasdop', 4, 'bh', 3900.00, NULL, 12, '2020-02-13 09:39:17', '2020-02-13 09:39:17');
INSERT INTO `material` VALUES (134, '130220024010-0_0dd42dd1-0ab9-4dc6-a19b-d2dd1b46debb_700_933.jpg', 'Fitting Plafon', 4, 'bh', 5100.00, NULL, 12, '2020-02-13 09:40:10', '2020-02-13 09:40:10');
INSERT INTO `material` VALUES (135, '130220024146-0_df3f7e3a-08bb-4227-955d-341caab6d3f2_690_637.jpg', 'T-dus PVC', 4, 'bh', 4000.00, NULL, 12, '2020-02-13 09:41:46', '2020-02-13 09:41:46');
INSERT INTO `material` VALUES (136, '130220024236-broco_stopkontak_single_broco_ob_outbow_tunggal_full02_i9n02df0.jpg', 'Stop Kontak Single', 4, 'bh', 66000.00, NULL, 12, '2020-02-13 09:42:36', '2020-02-13 09:42:36');
INSERT INTO `material` VALUES (137, '130220024315-nHBfsgAAsQAAABcAEEaNlwAAdcM.jpg', 'Stop Kontak Double', 4, 'bh', 82000.00, NULL, 12, '2020-02-13 09:43:15', '2020-02-13 09:43:15');
INSERT INTO `material` VALUES (138, '130220024444-Karpet.jpg', 'karpet', 11, 'm2', 590000.00, NULL, 3, '2020-02-13 09:44:44', '2020-02-13 09:44:44');
INSERT INTO `material` VALUES (139, '130220024505-5497387_212a3614-a93f-4fa4-a830-e1d46719743e_594_415.jpg', 'Stop Kontak Triple', 4, 'bh', 123500.00, NULL, 12, '2020-02-13 09:45:05', '2020-02-13 09:45:05');
INSERT INTO `material` VALUES (140, '130220024622-Rubber_corgated.jpg', 'Underlayer/rubber corrugated', 11, 'm2', 81000.00, NULL, 3, '2020-02-13 09:46:22', '2020-02-13 09:46:22');
INSERT INTO `material` VALUES (141, '130220024819-70902fe7641c0b55e8dd0c02d0394db7.jfif', 'Saklar tunggal', 4, 'bh', 48500.00, NULL, 12, '2020-02-13 09:48:19', '2020-02-13 09:48:19');
INSERT INTO `material` VALUES (142, '130220024904-6426265_c9fedd02-87f5-4126-9c48-34ba71925171_1512_1512.jpg', 'Saklar ganda', 4, 'bh', 66000.00, NULL, 12, '2020-02-13 09:49:04', '2020-02-13 09:49:04');
INSERT INTO `material` VALUES (143, '130220024942-gymfloor.jpg', 'Gymfloor', 11, 'm2', 118000.00, NULL, 3, '2020-02-13 09:49:42', '2020-02-13 09:49:42');
INSERT INTO `material` VALUES (144, '130220025012-img521-1534317906.jpg', 'ACB 2000A (NW20H13D2EH)', 4, 'set', 53500000.00, NULL, 12, '2020-02-13 09:50:12', '2020-02-13 09:50:12');
INSERT INTO `material` VALUES (145, '130220025041-Batu_granit.jpg', 'Batu granit', 11, 'm1', 2023000.00, NULL, 3, '2020-02-13 09:50:41', '2020-02-13 09:50:41');
INSERT INTO `material` VALUES (146, '130220025112-ef150.jpg', 'Air Terminal Prevecton R150 offer to EVO Franklin EF-150', 4, 'set', 133000000.00, NULL, 12, '2020-02-13 09:51:12', '2020-02-13 09:51:12');
INSERT INTO `material` VALUES (147, '130220025134-Batu_granit.jpg', 'Batu granit T10 mm', 11, 'kg', 598000.00, NULL, 3, '2020-02-13 09:51:34', '2020-02-13 09:51:34');
INSERT INTO `material` VALUES (148, '130220025227-14515534_32d1728b-ae4d-4641-84fe-81b1e795389d_608_562.png', 'Ampere Meter 0-2000A', 4, 'PC', 694000.00, NULL, 12, '2020-02-13 09:52:27', '2020-02-13 09:52:27');
INSERT INTO `material` VALUES (149, '130220025318-Lantai_parkit.jpg', 'Lantai parkit', 11, 'm2', 598000.00, NULL, 3, '2020-02-13 09:53:18', '2020-02-13 09:53:18');
INSERT INTO `material` VALUES (150, '130220025331-7372389_ae180b7a-e11b-4029-8829-fa1818435b3f.jpg', 'Battery ACCU 12 V, 100 AH Include Box Panel', 4, 'set', 2460000.00, NULL, 12, '2020-02-13 09:53:31', '2020-02-13 09:53:31');
INSERT INTO `material` VALUES (151, '130220025430-s-l400.jpg', 'Box Panel (600HX400WX250D), IP66, SS304 (c/w terminal)', 4, 'set', 19400000.00, NULL, 12, '2020-02-13 09:54:30', '2020-02-13 09:54:30');
INSERT INTO `material` VALUES (152, '130220025540-Walpaper.jpg', 'Wallpaper', 11, 'm2', 635000.00, NULL, 3, '2020-02-13 09:55:40', '2020-02-13 09:55:40');
INSERT INTO `material` VALUES (153, '130220025552-download.jfif', 'Cable Ladder galvanized c/w cover, joint, Elbow Vertical & Horizontal, Cross for control (W : 100mm)', 4, 'set', 2050000.00, NULL, 12, '2020-02-13 09:55:52', '2020-02-13 09:55:52');
INSERT INTO `material` VALUES (154, '130220025657-download.jfif', 'Cable Ladder galvanized c/w cover, joint, Elbow Vertical & Horizontal, Tee, Cross (W : 300mm)', 4, 'set', 2600000.00, NULL, 12, '2020-02-13 09:56:57', '2020-02-13 09:56:57');
INSERT INTO `material` VALUES (155, '130220025750-download.jfif', 'Cable Ladder galvanized c/w cover, joint, Elbow Vertical & Horizontal, Tee, Cross (W : 400mm)', 4, 'set', 3200000.00, NULL, 12, '2020-02-13 09:57:50', '2020-02-13 09:57:50');
INSERT INTO `material` VALUES (156, '130220025843-Internalcove.jpg', 'Internalcove', 11, 'm2', 156000.00, NULL, 3, '2020-02-13 09:58:43', '2020-02-13 09:58:43');
INSERT INTO `material` VALUES (157, '130220030033-Bahan_teraso_cor.jpg', 'Bahan teraso cor', 11, 'm2', 156000.00, 15600.00, 3, '2020-02-13 10:00:33', '2020-02-13 10:00:49');
INSERT INTO `material` VALUES (158, '130220030346-067c90a4b5dd4340727210488d14db2b.jfif', 'Cable NYA 1000 Volt 1 x 1,5 mm', 4, 'roll', 332500.00, NULL, 12, '2020-02-13 10:03:46', '2020-02-13 10:03:46');
INSERT INTO `material` VALUES (159, '130220030419-Tera_kota.jpg', 'Tera kota', 11, 'm2', 100000.00, NULL, 3, '2020-02-13 10:04:19', '2020-02-13 10:04:19');
INSERT INTO `material` VALUES (160, '130220030919-2042520_af2f2149-de7e-4a74-940e-1ad0f47c4180_1060_996.jpg', 'Cable NYA 1000 Volt 1 x 2,5 mm', 4, 'roll', 416000.00, NULL, 12, '2020-02-13 10:09:19', '2020-02-13 10:09:19');
INSERT INTO `material` VALUES (161, '130220031041-18477828_3bc13f19-f1d7-4044-b826-81a7407cef46_768_1024.jpeg', 'Cable NYA 1000 Volt 2 x 2,5 mm', 4, 'roll', 1250000.00, NULL, 12, '2020-02-13 10:10:41', '2020-02-13 10:10:41');
INSERT INTO `material` VALUES (162, '130220031141-Kabel_Listrik_NYA_1x4mm2_SUTRADO_100_M_4_mm_4mm_4mm2_1x4mm_1.jpg', 'Cable NYA 1000 Volt  4 mm', 4, 'roll', 835000.00, NULL, 12, '2020-02-13 10:11:41', '2020-02-13 10:11:41');
INSERT INTO `material` VALUES (163, '130220031327-50970697_a8db6280_37b9_4ded_84ee_12e12ed87d30.jpg', 'Cable NYMHY 3 x 2,5 mm', 4, 'roll', 1875000.00, NULL, 12, '2020-02-13 10:13:27', '2020-02-13 10:13:27');
INSERT INTO `material` VALUES (164, '130220033438-kayu_jati.jpg', 'Kayu jati balok6,15,8/12 paanjang 4 m', 9, 'm3', 26800.00, NULL, 3, '2020-02-13 10:34:39', '2020-02-13 10:34:39');
INSERT INTO `material` VALUES (165, '130220033644-ulin.jpg', 'Kayu ulin bulat diameter 8-10/4m', 9, 'Btg', 220000.00, NULL, 3, '2020-02-13 10:36:44', '2020-02-13 10:36:44');
INSERT INTO `material` VALUES (166, '130220033923-Meranti.jpg', 'Kayu meranti', 9, 'm3', 3645000.00, NULL, 3, '2020-02-13 10:39:23', '2020-02-13 10:39:23');
INSERT INTO `material` VALUES (167, '130220033927-Meranti.jpg', 'Kayu meranti', 9, 'm3', 3645000.00, NULL, 3, '2020-02-13 10:39:27', '2020-02-13 10:39:27');
INSERT INTO `material` VALUES (168, '130220034037-Meranti.jpg', 'Kayu meranti balok', 9, 'm3', 3465000.00, 3465.00, 3, '2020-02-13 10:40:37', '2020-02-13 10:41:16');

SET FOREIGN_KEY_CHECKS = 1;
