<?php

namespace App\Models;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class Material
{
    function __construct()
    {
    }
    
    public function get()
    {
        $results = DB::select('select * from material order by last_update desc');
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function getById($id)
    {
        $results = DB::select('select * from material where id = ' . $id);
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function getIdKelompok($id)
    {
        $results = DB::select('select * from material where kelompok = ' . $id);
        if (!$results) {
            return true;
        } else {
            return false;
        }
    }

    public function create($data)
    {
        $results = DB::table('material')->insert($data);
        if ($results) {
            return true;
        } else {
            return false;
        }
    }

    public function update($data)
    {
        $results = DB::table('material')
            ->where('id', $data['id'])
            ->update($data);
        if ($results) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($data)
    {
        $results = DB::table('material')->where('id', $data['id'])->delete();
        if ($results) {
            return true;
        } else {
            return false;
        }
    }
}
