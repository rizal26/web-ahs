<?php

namespace App\Models;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class ValueEv
{
    function __construct()
    {
    }
    
    public function get()
    {
        $results = DB::select('select * from value_quest order by id asc');
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function getById($id)
    {
        $results = DB::select('select * from value_quest where id = ' . $id);
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function create($data)
    {
        $results = DB::table('value_quest')->insert($data);
        if ($results) {
            return true;
        } else {
            return false;
        }
    }

    public function update($data)
    {
        $results = DB::table('value_quest')
            ->where('id', $data['id'])
            ->update($data);
        if ($results) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($data)
    {
        $results = DB::table('value_quest')->where('id', $data['id'])->delete();
        if ($results) {
            return true;
        } else {
            return false;
        }
    }
}
