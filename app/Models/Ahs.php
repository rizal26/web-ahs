<?php

namespace App\Models;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class Ahs
{
    function __construct()
    {
    }

    public function get()
    {
        $results = DB::select('select * from ahs_pekerjaan');
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function getPekerjaan()
    {
        $results = DB::select('select * from pekerjaan');
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function search($search)
    {
        $results = DB::select("select * from ahs where descr like '%" . $search . "%'");
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function getById($id)
    {
        $results = DB::select('select * from ahs_pekerjaan where id = ' . $id);
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function create($data)
    {
        $results = DB::table('ahs')->insert($data);
        if ($results) {
            return true;
        } else {
            return false;
        }
    }

    public function createPek($data)
    {
        $results = DB::table('ahs_pekerjaan')->insert($data);
        if ($results) {
            return true;
        } else {
            return false;
        }
    }

    public function update($data)
    {
        $results = DB::table('ahs_pekerjaan')
            ->where('id', $data['id'])
            ->update($data);
        if ($results) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($data)
    {
        $results = DB::table('ahs_pekerjaan')->where('id', $data['id'])->delete();
        if ($results) {
            return true;
        } else {
            return false;
        }
    }
}
