<?php

namespace App\Models;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class Department
{
    function __construct()
    {
    }

    public function get()
    {
        $results = DB::select('select * from department order by last_update desc');
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function search($search)
    {
        $results = DB::select("select * from department where descr like '%" . $search . "%'");
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function getById($id)
    {
        $results = DB::select('select * from department where id = ' . $id);
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function create($data)
    {
        $results = DB::table('department')->insert($data);
        if ($results) {
            return true;
        } else {
            return false;
        }
    }

    public function update($data)
    {
        $results = DB::table('department')
            ->where('id', $data['id'])
            ->update($data);
        if ($results) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($data)
    {
        $results = DB::table('department')->where('id', $data['id'])->delete();
        if ($results) {
            return true;
        } else {
            return false;
        }
    }
}
