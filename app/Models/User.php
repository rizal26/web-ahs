<?php

namespace App\Models;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class User {   
    function __construct() {
        
    }
    public function login($login_id, $password) {
        $results = DB::select('select * from user where username = "'.$login_id.'" and password = "'.$password.'"');
       	if ($results) {
	        Session::put('userdata', $results);
	        return true;
       	} else {
       		return false;
       	}
    }

    public function get() {
       	$results = DB::select('select * from user order by updated desc');
       	if ($results) {
	        return $results;
       	} else {
       		return false;
       	} 
    }

    public function getById($id) {
    	$results = DB::select('select * from user where id = '.$id);
    	if ($results) {
	        return $results;
       	} else {
       		return false;
       	} 
    }

    public function create($data) {
    	$results = DB::table('user')->insert($data);
    	if ($results) {
	        return true;
       	} else {
       		return false;
       	} 
    }

    public function update($data) {
    	$results = DB::table('user')
            ->where('id', $data['id'])
            ->update($data);
        if ($results) {
	        return true;
       	} else {
       		return false;
       	} 
    }

    public function delete($data) {
    	$results = DB::table('user')->where('id', $data['id'])->delete();
    	if ($results) {
	        return true;
       	} else {
       		return false;
       	} 
    }
}
