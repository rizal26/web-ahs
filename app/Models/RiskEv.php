<?php

namespace App\Models;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class RiskEv
{
    function __construct()
    {
    }
    
    public function get()
    {
        $results = DB::select('select * from risk_quest order by id asc');
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function getById($id)
    {
        $results = DB::select('select * from risk_quest where id = ' . $id);
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function getIssue()
    {
        $results = DB::select('select * from risk_quest where id = 11');
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function create($data)
    {
        $results = DB::table('risk_quest')->insert($data);
        if ($results) {
            return true;
        } else {
            return false;
        }
    }

    public function update($data)
    {
        $results = DB::table('risk_quest')
            ->where('id', $data['id'])
            ->update($data);
        if ($results) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($data)
    {
        $results = DB::table('risk_quest')->where('id', $data['id'])->delete();
        if ($results) {
            return true;
        } else {
            return false;
        }
    }
}
