<?php

namespace App\Models;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class Pir
{
    function __construct()
    {
    }

    public function get()
    {
        $results = DB::select('select * from vw_pir');
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function getDetailPir($id_initiation)
    {
        $results = DB::select('select * from vw_pir_detail where id = '.$id_initiation);
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function getVwPirById($id)
    {
        $results = DB::select('select * from vw_pir where id_user = '.$id);
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function getStatus($status, $role)
    {
        $results = DB::select('select * from vw_pir where status = '.$status.$role);
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function search($search)
    {
        $results = DB::select("select * from pir where descr like '%" . $search . "%'");
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function getById($id)
    {
        $results = DB::select('select * from initiation where id = ' . $id);
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function getIdInitiation($id)
    {
        $results = DB::select('select * from initiation where department = ' . $id);
        if (!$results) {
            return true;
        } else {
            return false;
        }
    }

    public function getPirByValue($id)
    {
        $results = DB::select('select value from pir where initiation = ' . $id);
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function getPirByRisk($id)
    {
        $results = DB::select('select risk from pir where initiation = ' . $id);
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function getInitiationId($id)
    {
        $results = DB::select('select * from initiation where id = ' . $id);
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function createPir($data)
    {
        $results = DB::table('pir')->insert($data);
        if ($results) {
            // dd($results);
            return true;
        } else {
            return false;
        }
    }

    public function updatePir($data)
    {
        $results = DB::table('pir')
                    ->where(['initiation' => $data['initiation'], 'value' => $data['value'], 'risk' => $data['risk']])
                    ->update($data);
        if ($results >= 0) {
            // dd($results);
            return true;
        } else {
            return false;
        }
    }

    public function create($data)
    {
        $results = DB::table('initiation')->insertGetId($data);
        if ($results) {
            // dd($results);
            return ['id' => $results];
        } else {
            return false;
        }
    }

    public function createValue($data)
    {
        $results = DB::table('value_ev')->insert($data);
        if ($results) {
            return true;
        } else {
            return false;
        }
    }

    public function updateValue($data, $id)
    {
        $results = DB::table('value_ev')->where('id', $id)->update($data);
        if ($results >=0 ) {
            return true;
        } else {
            return false;
        }
    }

    public function updateRisk($data, $id)
    {
        $results = DB::table('risk_ev')->where('id', $id)->update($data);
        if ($results >=0 ) {
            return true;
        } else {
            return false;
        }
    }

    public function getAnswer($id_initiation) {
        $results = DB::select('select question from risk_ev where id_initiation = ' . $id_initiation);
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function createRisk($data)
    {
        $results = DB::table('risk_ev')->insert($data);
        if ($results) {
            return true;
        } else {
            return false;
        }
    }

    public function resume($id)
    {
        $results = DB::select('SELECT a.*, b.question as value_ev, c.question as risk_ev, d.descr as department_nm from initiation a
        left join value_ev b on a.id = b.id_initiation    
        left join risk_ev c on a.id = c.id_initiation  
        left join department d on a.department = d.id  
        where a.id = '.$id);
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }

    public function question($id)
    {
        $results = DB::select('SELECT b.question as quest_val, c.question as quest_risk from pir a
        left join value_ev b on a.value = b.id
        left join risk_ev c on a.risk = c.id
        where a.initiation = '.$id);
        if ($results) {
            return $results;
        } else {
            return false;
        }
    }
    
    public function getInput($id) {
        $results = DB::select('SELECT a.id as id_initiation, b.id as id_value, c.id as id_risk FROM initiation a
        LEFT JOIN value_ev b ON a.id = b.id_initiation
        LEFT JOIN risk_ev c ON a.id = c.id_initiation
        WHERE a.id = '.$id);
        if ($results) {
            return $results;
        } else {
            return false;
        }

    }

    public function update($data)
    {
        $results = DB::table('initiation')
            ->where('id', $data['id'])
            ->update($data);
        if ($results >= 0) {
            return true;
        } else {
            return false;
        }
    }

    public function updateApproval($data)
    {
        $results = DB::table('pir')
            ->where('initiation', $data['initiation'])
            ->update($data);
        if ($results >= 0) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($data)
    {
        $results = DB::table('pir')->where('initiation', $data['id'])->delete();
        if ($results) {
            return true;
        } else {
            return false;
        }
    }
}
