<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use App\Helpers\Utils;

use App\Models\User;
// use App\Models\Audit;

class AuthController extends Controller
{
    public function login(Request $request) {
        if($request->isMethod('post')) {
            $login_id = $request->input('username');
            $password = base64_encode($request->input('password'));
            $user = new User();
            $resp = $user->login($login_id, $password);
            if($resp) {
                Session::put('id', Session::get('userdata')[0]->id);
                Session::put('username', Session::get('userdata')[0]->username);
                Session::put('nama', Session::get('userdata')[0]->nama);
                Session::put('role', Session::get('userdata')[0]->role);
                Session::put('menu', Session::get('userdata')[0]->menu);
                Session::put('login', TRUE);
                Session::forget('userdata');

                return redirect('/home');
            } else {
                return redirect()->back()->withErrors(['error' => 'Authentication Failed.']);
            }
        }
        return view('auth.login');
    }

    public function logout(Request $request) {
        Session::flush();
        return redirect('/login');
    }
}