<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use App\Models\Department;
use App\Models\Pir;
use App\Models\User;

class DepartmentController extends Controller
{

    public function index(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('role') != 1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        return view('master.department.index');
    }

    public function selectKelompok(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if ($request->search) {
            $search = $request->search;
        } else {
            $search = '';
        }

        $returnData = (new Department())->search($search);
        if (!$returnData) {
            $output['results'] = array();
            return $output;
        }

        foreach ($returnData as $data) {
            $output['results'][] = array(
                'id' => $data->id,
                'text' => $data->descr,
            );
        }

        return json_encode($output);
    }

    public function create(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('role') != 1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        if ($request->isMethod('post')) {
            $validator = Validator::make(Input::all(), [
                'descr' => 'required',
            ]);

            if (!$validator->fails()) {
                $descr = $request->input('descr');
                $data = array(
                    'descr' => $descr,
                    'modify_by' => Session::get('id'),
                );

                $saveState = (new Department)->create($data);
                if ($saveState) {
                    $success = array('success' => 'Data has been saved Successfully.');
                    return Redirect::to('department')->withErrors($success);
                } else {
                    $errors = array('error' => 'API Error, Please Contact Administrator');
                    return view('master.department.add')->withErrors($errors);
                }
            } else {
                $errors = $validator->errors();
                return view('master.department.add')->withErrors($errors);
            }
        }
        return view('master.department.add');
    }

    public function update(Request $request, $id = null)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('role') != 1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        $dataUser = (new Department)->getById($id);
        if ($dataUser) {
            $department = $dataUser[0];
        } else {
            $department = array();
            $errors = ['error' => 'Data Not Found, Please Add Data First'];
            return view('master.department.add')->withErrors($errors);
        }

        if ($request->isMethod('post')) {
            $validator = Validator::make(Input::all(), [
                'descr' => 'required',
            ]);
            if (!$validator->fails()) {
                $descr = $request->input('descr');
                $data = array(
                    'id' => intval($id),
                    'descr' => $descr,
                    'modify_by' => Session::get('id')
                );

                $updateState = (new Department)->update($data);
                $success = array('success' => 'Data has been updated Successfully.');
                return Redirect::to('department')->withErrors($success);
            } else {
                $errors = $validator->errors();
                return view('master.department.edit', compact('department'))->withErrors($errors);
            }
        }
        return view('master.department.edit', compact('department'));
    }

    public function delete(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('role') != 1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        $cekDelete = (new Pir)->getIdInitiation($request->id);
        if (!$cekDelete) {
            $errors = array('error' => "Can't delete, data has been used by PIR");
            return Redirect::to('department')->withErrors($errors);
        }
        $dataUser = (new Department)->getById($request->id);
        if ($dataUser) {
            $department = $dataUser[0];
            $data = array(
                'id' => intval($department->id),
            );
            $saveState = (new Department)->delete($data);
            $success = array('success' => 'Data has been deleted Successfully.');
            return Redirect::to('department')->withErrors($success);
        } else {
            $errors = array('error' => 'API Error, Please Contact Administrator');
            return Redirect::to('department')->withErrors($errors);
        }
    }

    public function getAjaxData(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return response()->json("Not Authorized");
        }

        if (Session::get('role') != 1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        $returnData = (new Department())->get();
        if (!$returnData) {
            $output = array('data' => '');
            return $output;
        }
        $num = 0;

        foreach ($returnData as $key => $data) {
            $strAction = '
                <a href="' . url('department/edit/' . $data->id) . '" title="edit" class="btn btn-xs btn-inverse"><i class="fa fa-pencil"></i></a>
                <a href="#" class="btn btn-xs btn-danger" title="delete" data-id="' . $data->id . '" data-act="deactivate" data-toggle="modal" data-target="#del"><i class="fa fa-times"></i></a>
            ';

            $output['data'][] = array(
                'id' => $data->id,
                'descr' => $data->descr,
                'modify_by' => !(new User)->getById($data->modify_by)?'[deleted]':(new User)->getById($data->modify_by)[0]->username,
                'last_update' => date('d-M-Y H:i', strtotime($data->last_update)),
                'action' => $strAction
            );
            if (Session::get('role') == 1) {
                $output['data'][$key]['action'] = $strAction;
            }

            $num++;
        }
        return json_encode($output);
    }
}
