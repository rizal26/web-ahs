<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use App\Models\User;
use App\Helpers\Utils;

class UserManagementController extends Controller {

    public function index(Request $request) {
        if(!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('role') != 1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        $users = (new User())->get();
        if (!$users) {
            $users = array();
            return view('admin.index', compact('users'));
        }
        return view('admin.index', compact('users'));
    }

    public function create(Request $request) {
        if(!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('role') != 1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        if($request->isMethod('post')) {
            $validator = Validator::make(Input::all(), [
                'username' => 'required|max:30',
                'email' => 'required|max:50',
                'nama' => 'required|max:50',
                'phone' => 'required|max:20',
                'role' => 'required',
                'password' => 'required|confirmed|min:8',
            ]);
            if(!$validator->fails()) {
                $username = $request->input('username');
                $nama = $request->input('nama');
                $email = $request->input('email');
                $password = $request->input('password');
                $phone = $request->input('phone');
                $role = $request->input('role');
                $menu = $request->input('menu');
                $data = array(
                    'username' => $username,
                    'nama' => $nama,
                    'email' => $email,
                    'phone' => $phone,
                    'role' => $role,
                    'menu' => $menu,
                    'password' => base64_encode($password),
                );
                $saveState = (new User)->create($data);
                if($saveState) {
                    $success = array('success' => 'Data has been saved Successfully.');
                    return Redirect::to('user')->withErrors($success);
                } else {
                    $errors = array('error' => 'API Error, Please Contact Administrator');
                    return view('admin.add')->withErrors($errors);
                }
            } else {
                $errors = $validator->errors();
                return view('admin.add')->withErrors($errors);
            }          
        }
        return view('admin.add');    
    }

    public function update(Request $request, $id = null) {
        if(!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('role') != 1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        $dataUser = (new User)->getById($id);
        if($dataUser) {
            $user = $dataUser[0];
        } else {
            $user = array();
            $errors = [ 'error' => 'Data Not Found, Please Add Data First' ];
            return view('admin.add')->withErrors($errors);
        }
        if($request->isMethod('post')) {
            $validator = Validator::make(Input::all(), [
                'nama' => 'required|max:50',
                'phone' => 'required|max:20',
                'role' => 'required',
            ]);
            if(!$validator->fails()) {
                $nama = $request->input('nama');
                $phone = $request->input('phone');
                $role = $request->input('role');
                $menu = $request->input('menu');
                $data = array(
                    'id' => intval($id),
                    'nama' => $nama,
                    'phone' => $phone,
                    'role' => $role,
                    'menu' => $menu,
                );
                $password = $request->input('password');
                if ($password != null) {
                    $data['password'] = base64_encode($password);
                }
                $updateState = (new User)->update($data);
                $success = array('success' => 'Data has been updated Successfully.');
                return Redirect::to('user')->withErrors($success);
                
                } else {
                    $errors = $validator->errors();
                    return view('admin.edit', compact('user'))->withErrors($errors);
                }
            }           
        return view('admin.edit', compact('user'));
    }

    public function delete(Request $request) {
        if(!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('role') != 1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        $dataUser = (new User)->getById($request->id);
        if($dataUser) {
            $user = $dataUser[0];
            $data = array(
                'id' => intval($user->id),
            );
            $saveState = (new User)->delete($data);
                $success = array('success' => 'Data has been deleted Successfully.');
                return Redirect::to('user')->withErrors($success);
            } else {
                $errors = array('error' => 'API Error, Please Contact Administrator');
                return Redirect::to('user')->withErrors($errors);
            }            
        } 

    public function getAjaxData(Request $request) {
        if(!(Session::has('login') && Session::get('login'))) {
            return response()->json("Not Authorized");
        }

        if (Session::get('role') != 1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        $returnData = (new User())->get();
        if (!$returnData) {
            $output = array('data' => '');
            return $output;
        }
        // dd($returnData);
        $num = 0;
        foreach ($returnData as $data) {
            $strAction = '
                <a href="'.url('user/edit/'.$data->id).'" title="edit" class="btn btn-xs btn-inverse"><i class="fa fa-pencil"></i></a>
            ';

            if (Session::get('id') != $data->id) {
                $strAction .= '
                    <a href="#" class="btn btn-xs btn-danger" title="delete" data-id="'.$data->id.'" data-act="deactivate" data-toggle="modal" data-target="#del"><i class="fa fa-times"></i></a>
                ';
            }

            if ($data->menu == 0) {
                $menu = 'All';
            } else if ($data->menu == 1) {
                $menu = 'AHS';
            } else if ($data->menu == 2) {
                # code...
                $menu = 'PIR';
            } else {
                $menu = 'Uncategorized';
            }

            // $output = array(
            $output['data'][] = array(
                'id' => $data->id,
                'user' => '<strong><a href="'.url('user/edit/'.$data->id).'" class="">'.$data->nama.'</a></strong><br/><span class="text-inverse ">'.$data->phone.'</span>',
                'email' => $data->email,
                'role' => ( $data->role > 1 ? 'User' : 'Admin' ),
                'menu' => $menu,
                'created' => date('d-M-Y H:i', strtotime($data->created)),
                'action' => $strAction, 
            );
            $num++;
        }
        return json_encode($output);
    }

}