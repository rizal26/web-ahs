<?php

namespace App\Http\Controllers\Apps;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use App\Models\Ahs;
use App\Models\User;

class AhsController extends Controller
{

    public function index(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu')>1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        return view('ahs.index');
    }

    public function selectAhs(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if ($request->search) {
            $search = $request->search;
        } else {
            $search = '';
        }

        $returnData = (new Ahs())->search($search);
        if (!$returnData) {
            $output['results'] = array();
            return $output;
        }

        foreach ($returnData as $data) {
            $output['results'][] = array(
                'id' => $data->id,
                'text' => $data->descr,
            );
        }

        return json_encode($output);
    }

    public function create(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu')>1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        $pekerjaan = (new Ahs())->getPekerjaan();
        // dd($pekerjaan);
        if ($request->isMethod('post')) {
            $arrInput = Input::all();
            unset($arrInput['_token']);
            $id_trx = md5(uniqid());
            // print_r('<pre>');
            // dd($arrInput);
            for ($i=0; $i < count($arrInput['pekerjaan']); $i++) { 
                $index = $arrInput['pekerjaan'];
                $arrIndex = $index[$i][0];
                $arrUraian = $index[$i]['uraian'];
                $arrSatuan = $index[$i]['satuan'];
                $arrHarga = $index[$i]['harga'];
                $harga = str_replace('Rp. ', '', $arrHarga);
                $harga = str_replace(',', '', $harga);
                $arrRemarks = $index[$i]['remarks'];
                $arrDescr = array(
                    'uraian' => $arrUraian,
                    'satuan' => $arrSatuan,
                    'harga' => $harga,
                    'remarks' => $arrRemarks,
                );
                $dataPek = array(
                    'id_trx' => $id_trx,
                    'pekerjaan' => $arrIndex,
                    'descr' => json_encode($arrDescr),
                    'modify_by' => Session::get('id')
                );
                $savePekerjaan = (new Ahs)->createPek($dataPek);
                
            }
            $success = array('success' => 'Data has been saved Successfully.');
            return Redirect::to('ahs')->withErrors($success);
        }
        return view('ahs.add', compact('pekerjaan'));
    }

    public function update(Request $request, $id = null)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu')>1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        $dataUser = (new Ahs)->getById($id);
        if ($dataUser) {
            $ahs = $dataUser[0];
        } else {
            $ahs = array();
            $errors = ['error' => 'Data Not Found, Please Add Data First'];
            return view('ahs.add')->withErrors($errors);
        }

        $dataDescr = json_decode($ahs->descr);
        $ahs->dataDescr = $dataDescr;
        // dd($ahs);

        if ($request->isMethod('post')) {
            $arrInput = Input::all();
            unset($arrInput['_token']);
            // dd($arrInput);
            // print_r('<pre>');
            $index = $arrInput['pekerjaan'];
            $arrIndex = $index[0][0];
            $arrUraian = $index[0]['uraian'];
            $arrSatuan = $index[0]['satuan'];
            $arrHarga = $index[0]['harga'];
            $harga = str_replace('Rp. ', '', $arrHarga);
            $harga = str_replace(',', '', $harga);
            $arrRemarks = $index[0]['remarks'];
            $arrDescr = array(
                'uraian' => $arrUraian,
                'satuan' => $arrSatuan,
                'harga' => $harga,
                'remarks' => $arrRemarks,
            );
            $dataPek = array(
                'id' => $id,
                'pekerjaan' => $arrIndex,
                'descr' => json_encode($arrDescr),
                'modify_by' => Session::get('id')
            );
            // dd($dataPek);
            $savePekerjaan = (new Ahs)->update($dataPek);
            $success = array('success' => 'Data has been saved Successfully.');
            return Redirect::to('ahs')->withErrors($success);
        }
        return view('ahs.edit', compact('ahs'));
    }

    public function delete(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu')>1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        $dataUser = (new Ahs)->getById($request->id);
        if ($dataUser) {
            $ahs = $dataUser[0];
            $data = array(
                'id' => intval($ahs->id),
            );
            $saveState = (new Ahs)->delete($data);
            $success = array('success' => 'Data has been deleted Successfully.');
            return Redirect::to('ahs')->withErrors($success);
        } else {
            $errors = array('error' => 'API Error, Please Contact Administrator');
            return Redirect::to('ahs')->withErrors($errors);
        }
    }

    public function getDetail($id) {
        if (!(Session::has('login') && Session::get('login'))) {
            return response()->json("Not Authorized");
        }

        $data['pekerjaan']= (new Ahs)->getById($id)[0]->pekerjaan;
        $data['descr']= (array) json_decode((new Ahs)->getById($id)[0]->descr);
        // dd($data);
        return $data;

    }

    public function getAjaxData(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return response()->json("Not Authorized");
        }

        if (Session::get('menu')>1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        $returnData = (new Ahs())->get();
        if (!$returnData) {
            $output = array('data' => '');
            return $output;
        }
        $num = 0;

        foreach ($returnData as $key => $data) {
            $strAction = '<a href="#" class="btn btn-xs btn-success" title="detail" data-id="' . $data->id . '" data-act="deactivate" data-toggle="modal" data-target="#det"><i class="fa fa-eye"></i></a>';
            if (Session::get('role') == 1) {
                $strAction .= '
                    <a href="' . url('ahs/edit/' . $data->id) . '" title="edit" class="btn btn-xs btn-inverse"><i class="fa fa-pencil"></i></a>
                    <a href="#" class="btn btn-xs btn-danger" title="delete" data-id="' . $data->id . '" data-act="deactivate" data-toggle="modal" data-target="#del"><i class="fa fa-times"></i></a>
                ';
            }

            $output['data'][] = array(
                'id' => $data->id,
                'id_trx' => $data->id_trx,
                'pekerjaan' => $data->pekerjaan,
                'descr' => $data->descr,
                'modify_by' => !(new User)->getById($data->modify_by)?'[deleted]':(new User)->getById($data->modify_by)[0]->nama,
                'last_update' => date('d-M-Y H:i', strtotime($data->last_update)),
                'action' => $strAction
            );
            $num++;
        }
        return json_encode($output);
    }
}
