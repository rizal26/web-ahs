<?php

namespace App\Http\Controllers\Apps;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use App\Models\Material;
use App\Models\Kelompok;
use App\Models\User;

class MaterialController extends Controller
{

    public function index(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu')>1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }
        
        return view('material.index');
    }

    public function create(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('role')!=1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        if ($request->isMethod('post')) {
            // dd(Input::all());
            $validator = Validator::make(Input::all(), [
                'foto' => 'required|image|mimes:jpeg,png,jpg|max:2048',
                'descr' => 'required|max:100',
                'kelompok' => 'required',
                'satuan' => 'required|max:50',
                'harga' => 'required',
            ]);

            if (!$validator->fails()) {
                $descr = $request->input('descr');
                $kelompok = $request->input('kelompok');
                $satuan = $request->input('satuan');
                $harga = str_replace('Rp. ', '', $request->input('harga'));
                $harga = str_replace(',', '', $harga);
                $data = array(
                    'descr' => $descr,
                    'kelompok' => $kelompok,
                    'satuan' => $satuan,
                    'harga' => $harga,
                    'modify_by' => Session::get('id'),
                );
                $foto = $request->file('foto');
                if ($foto) {
                    $destinationPath = public_path('assets/images/material/'); // upload path
                    $profileImage = date('dmyhis') . "-" . $foto->getClientOriginalName();
                    $profileImageFix = str_replace(')', '', str_replace('(', '', str_replace(' ', '_', $profileImage)));
                    $foto->move($destinationPath, $profileImageFix);
                    $data['foto'] = "$profileImageFix";
                } else {
                    $errors = array('error' => 'Image Error, Please Contact Administrator');
                    return view('material.add')->withErrors($errors);
                }

                $saveState = (new Material)->create($data);
                if ($saveState) {
                    $success = array('success' => 'Data has been saved Successfully.');
                    return Redirect::to('material')->withErrors($success);
                } else {
                    $errors = array('error' => 'API Error, Please Contact Administrator');
                    return view('material.add')->withErrors($errors);
                }
            } else {
                $errors = $validator->errors();
                return view('material.add')->withErrors($errors);
            }
        }
        return view('material.add');
    }

    public function update(Request $request, $id = null)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('role') != 1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        $dataUser = (new Material)->getById($id);
        $kelompok = (new Kelompok)->get();
        if (!$kelompok) {
            $kelompok = array();
        }
        if ($dataUser) {
            $material = $dataUser[0];
        } else {
            $material = array();
            $errors = ['error' => 'Data Not Found, Please Add Data First'];
            return view('material.add')->withErrors($errors);
        }

        if ($request->isMethod('post')) {
            $validator = Validator::make(Input::all(), [
                'foto' => 'image|mimes:jpeg,png,jpg|max:2048',
                'descr' => 'required|max:100',
                'kelompok' => 'required',
                'satuan' => 'required|max:50',
                'harga' => 'required'
            ]);
            if (!$validator->fails()) {
                $descr = $request->input('descr');
                $kelompok = $request->input('kelompok');
                $satuan = $request->input('satuan');
                $harga = str_replace('Rp. ', '', $request->input('harga'));
                $harga = str_replace(',', '', $harga);
                $data = array(
                    'id' => $id,
                    'descr' => $descr,
                    'kelompok' => $kelompok,
                    'satuan' => $satuan,
                    'harga' => $harga,
                    'old_harga' => $material->harga,
                    'modify_by' => Session::get('id'),
                );
                $foto = $request->file('foto');
                if (isset($foto)) {
                    if ($foto) {
                        $oldPath = public_path('assets/images/material/' . $material->foto);
                        unlink($oldPath);
                        $destinationPath = public_path('assets/images/material/'); // upload path
                        $profileImage = date('dmyhis') . "-" . $foto->getClientOriginalName();
                        $profileImageFix = str_replace(')', '', str_replace('(', '', str_replace(' ', '_', $profileImage)));
                        $foto->move($destinationPath, $profileImageFix);
                        $data['foto'] = "$profileImageFix";
                    } else {
                        $errors = array('error' => 'Image Error, Please Contact Administrator');
                        return view('material.edit')->withErrors($errors);
                    }
                }


                $updateState = (new Material)->update($data);
                $success = array('success' => 'Data has been updated Successfully.');
                return Redirect::to('material')->withErrors($success);
            } else {
                $errors = $validator->errors();
                return view('material.edit', compact('material', 'kelompok'))->withErrors($errors);
            }
        }
        return view('material.edit', compact('material','kelompok'));
    }

    public function delete(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('role') != 1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        $dataUser = (new Material)->getById($request->id);
        if ($dataUser) {
            $material = $dataUser[0];
            $data = array(
                'id' => intval($material->id),
            );
            $saveState = (new Material)->delete($data);
            $oldPath = public_path('assets/images/material/' . $material->foto);
            if (isset($oldPath)) {
                unlink($oldPath);
            }
            $success = array('success' => 'Data has been deleted Successfully.');
            return Redirect::to('material')->withErrors($success);
        } else {
            $errors = array('error' => 'API Error, Please Contact Administrator');
            return Redirect::to('material')->withErrors($errors);
        }
    }

    public function getImage($id) {
        if (!(Session::has('login') && Session::get('login'))) {
            return response()->json("Not Authorized");
        }

        if (Session::get('menu')>1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        $data = (new Material)->getById($id);
        return json_encode($data);
    }

    public function getAjaxData(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return response()->json("Not Authorized");
        }

        if (Session::get('menu')>1) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }
        $returnData = (new Material())->get();
        if (!$returnData) {
            $output = array('data' => '');
            return $output;
        }
        $num = 0;

        foreach ($returnData as $i => $data) {
            $strAction = '
                <a href="' . url('material/edit/' . $data->id) . '" title="edit" class="btn btn-xs btn-inverse"><i class="fa fa-pencil"></i></a>
                <a href="#" class="btn btn-xs btn-danger" title="delete" data-id="' . $data->id . '" data-act="deactivate" data-toggle="modal" data-target="#del"><i class="fa fa-times"></i></a>
            ';
            
            $output['data'][] = array(
                'id' => $data->id,
                'foto' => '<a href="#" title="Preview" data-id="' . $data->id . '" data-act="deactivate" data-toggle="modal" data-target="#view"><div style="width:50px;height:50px;background-position: center top;background-repeat: no-repeat;background-image: url(' . asset('assets/images/material/' . $data->foto) . ');background-size:50px;"></div></a>',
                'id_material' => 'MT'.$data->id,
                'kelompok' => (new Kelompok)->getById($data->kelompok)[0]->descr,
                'descr' => $data->descr,
                'satuan' => $data->satuan,
                'harga' => 'Rp. ' . number_format($data->harga, 2),
                'old_harga' => $data->old_harga?'Rp. ' . number_format($data->old_harga, 2):'',
                'modify_by' => !(new User)->getById($data->modify_by)?'[deleted]':(new User)->getById($data->modify_by)[0]->username,
                'create_date' => date('d-M-Y H:i', strtotime($data->create_date)),
                'last_update' => date('d-M-Y H:i', strtotime($data->last_update)),
            );
            if (Session::get('role') == 1) {
                $output['data'][$i]['action'] = $strAction;
            }
            $num++;
        }
        // dd($output);
        return json_encode($output);
    }
}
