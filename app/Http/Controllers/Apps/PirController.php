<?php

namespace App\Http\Controllers\Apps;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Models\Pir;
use App\Models\ValueEv;
use App\Models\RiskEv;
use App\Models\User;
use App\Models\Department;

class PirController extends Controller
{

    public function index(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu') == 1 ) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        if(Session::get('role') != 1) { // 1 = admin
            $role = ' and role = 2 and id_user = '.Session::get('id');
            $getById = !(new Pir())->getVwPirById(Session::get('id')) ? 0 : count((new Pir())->getVwPirById(Session::get('id')));
        } else {
            $role = '';
            $getById = !(new Pir())->get() ? 0 : count((new Pir())->get());
        }
        
        $param = array(
            'all' =>  $getById,
            'new' =>  !(new Pir())->getStatus(0, $role)?0:count((new Pir())->getStatus(0, $role)),
            'approve' =>  !(new Pir())->getStatus(1, $role)?0:count((new Pir())->getStatus(1, $role)),
            'resubmit' =>  !(new Pir())->getStatus(4, $role)?0:count((new Pir())->getStatus(4, $role)),
            'postpon' =>  !(new Pir())->getStatus(3, $role)?0:count((new Pir())->getStatus(3, $role)),
            'reject' =>  !(new Pir())->getStatus(2, $role)?0:count((new Pir())->getStatus(2, $role)),
        );

        return view('pir.index', compact('param'));
    }

    public function search(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu') == 1 ) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        $value = $request->input('search');
        // dd($value);
        return view('pir.index', compact('value'));
    }

    public function create(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu') == 1 ) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        if ($request->isMethod('post')) {
            $arrInput = Input::all();
            unset($arrInput['_token']);

            $budget = str_replace('USD. ', '', $arrInput['estimate_budget']);
            $budget = str_replace(',', '', $budget);
            $arrInput['estimate_budget'] = $budget;

            $cost = str_replace('USD. ', '', $arrInput['cost']);
            $cost = str_replace(',', '', $cost);
            $arrInput['cost'] = $cost;

            // dd($arrInput);
            $foto = $arrInput['preliminary_design'];
            if ($foto) {
                $destinationPath = public_path('assets/images/pir/'); // upload path
                $profileImage = date('dmyhis') . "-" . $foto->getClientOriginalName();
                $profileImageFix = str_replace(')', '', str_replace('(', '', str_replace(' ', '_', $profileImage)));
                $foto->move($destinationPath, $profileImageFix);
                $arrInput['preliminary_design'] = "$profileImageFix";
            } else {
                $errors = array('error' => 'Image Error, Please Contact Administrator');
                return view('pir.add')->withErrors($errors);
            }
            
            $saveState = (new Pir)->create($arrInput);
            if ($saveState) {
                $lastId = $saveState['id'];
                return redirect('pir/value_ev/add/'. Crypt::encrypt($lastId));
            } else {
                $errors = array('error' => 'API Error, Please Contact Administrator');
                return view('pir.add')->withErrors($errors);
            }
        }
        return view('pir.add');
    }

    public function createValue(Request $request, $id=null)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu') == 1 ) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        if ($id == null) {
            $errors = array('error' => 'Please add Initiation first !');
            return view('pir.add')->withErrors($errors);
        }

        try {
            $id_initiation = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            $errors = array('error' => 'The parameter is invalid !');
            return view('pir.add')->withErrors($errors);
        }
        
        $param = array(
            'data' => (new ValueEv())->get(),
            'id_initiation' => $id
        ); 

        if ($request->isMethod('post')) {
            $arrInput = Input::all();
            unset($arrInput['_token']);
            $jsInput = json_encode($arrInput);

            $data = array(
                'id_initiation' => $id_initiation,
                'question' => $jsInput
            );

            $saveState = (new Pir)->createValue($data);
            if ($saveState) {
                return redirect('pir/risk_ev/add/' . $id);
            } else {
                $errors = array('error' => 'API Error, Please Contact Administrator');
                return redirect('pir/value_ev/add/'. $id)->withErrors($errors);
            }
        }

        return view('pir.value_ev', compact('param'));
    }

    public function editValue(Request $request, $id=null)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu') == 1 ) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }
        // dd($id);
        if ($id == null) {
            $errors = array('error' => 'Something went wrong!');
            return redirect('pir')->withErrors($errors);
        }

        try {
            $id_initiation = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            $errors = array('error' => 'The parameter is invalid !');
            return redirect('pir')->withErrors($errors);
        }

        $id_val = (new Pir())->getPirByValue($id_initiation)[0]->value;
        
        $param = array(
            'data' => (new ValueEv())->get(),
            'id_initiation' => $id,
            'id' => $id_val
        ); 
        // dd($param);

        if ($request->isMethod('post')) {
            $arrInput = Input::all();
            unset($arrInput['_token']);
            $jsInput = json_encode($arrInput);

            $data = array(
                // 'id' => $id_initiation,
                'question' => $jsInput
            );

            // dd($id);

            $saveState = (new Pir)->updateValue($data, $id_val);
            if ($saveState) {
                return redirect('pir/risk_ev/edit/' . $id);
            } else {
                $errors = array('error' => 'API Error, Please Contact Administrator');
                return redirect('pir/value_ev/add/'. $id)->withErrors($errors);
            }
        }

        return view('pir.edit.value_ev', compact('param'));
    }

    public function createRisk(Request $request, $id = null)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu') == 1 ) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        if ($id == null) {
            $errors = array('error' => 'Please add Initiation first !');
            return view('pir.add')->withErrors($errors);
        }

        try {
            $id_initiation = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            $errors = array('error' => 'The parameter is invalid !');
            return view('pir.add')->withErrors($errors);
        }

        $param = array(
            'data' => (new RiskEv())->get(),
            'id_initiation' => $id
        );

        if ($request->isMethod('post')) {
            $arrInput = Input::all();
            unset($arrInput['_token']);
            $jsInput = json_encode($arrInput);

            $data = array(
                'id_initiation' => $id_initiation,
                'question' => $jsInput
            );

            $saveState = (new Pir)->createRisk($data);
            if ($saveState) {
                return redirect('pir/resume/add/' . $id);
            } else {
                $errors = array('error' => 'API Error, Please Contact Administrator');
                return redirect('pir/risk_ev/add/' . $id)->withErrors($errors);
            }
        }

        return view('pir.risk_ev', compact('param'));
    }

    public function editRisk(Request $request, $id = null)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu') == 1 ) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        if ($id == null) {
            $errors = array('error' => 'Something went wrong!');
            return redirect('pir')->withErrors($errors);
        }

        try {
            $id_initiation = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            $errors = array('error' => 'The parameter is invalid !');
            return redirect('pir')->withErrors($errors);
        }

        $id_risk = (new Pir())->getPirByRisk($id_initiation)[0]->risk;
        // dd($id_risk);
        $param = array(
            'data' => (new RiskEv())->get(),
            'id_initiation' => $id
        );

        if ($request->isMethod('post')) {
            $arrInput = Input::all();
            unset($arrInput['_token']);
            $jsInput = json_encode($arrInput);

            $data = array(
                // 'id_initiation' => $id_initiation,
                'question' => $jsInput
            );

            // dd($data);

            $saveState = (new Pir)->updateRisk($data, $id_risk);
            if ($saveState) {
                return redirect('pir/resume/edit/' . $id);
            } else {
                $errors = array('error' => 'API Error, Please Contact Administrator');
                return redirect('pir/risk_ev/edit/' . $id)->withErrors($errors);
            }
        }

        return view('pir.edit.risk_ev', compact('param'));
    }

    public function createResume(Request $request, $id=null) {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu') == 1 ) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        if ($id == null) {
            $errors = array('error' => 'Please add Initiation first !');
            return view('pir.add')->withErrors($errors);
        }

        try {
            $id_initiation = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            $errors = array('error' => 'The parameter is invalid !');
            return view('pir.add')->withErrors($errors);
        }

        $param = array(
            'id_initiation' => $id
        );

        $data = (new Pir())->resume($id_initiation)[0];
        
        if ($data->value_ev == null) {
            $errors = array('error' => 'Please fill Value Evaluation first');
            return redirect('pir/value_ev/add/' . $id)->withErrors($errors);
        }

        if ($data->risk_ev == null) {
            $errors = array('error' => 'Please fill Risk Evaluation first');
            return redirect('pir/risk_ev/add/' . $id)->withErrors($errors);
        }
        // dd($param);

        $arrValue = json_decode($data->value_ev);
        $totalVal = 0;
        foreach ($arrValue as $key => $score) {
            $totalVal += $score;
        }

        if ($totalVal <= 100 && $totalVal >= 81) {
            $catValue = '<button type="button" class="btn btn-danger btn-rounded waves-effect waves-light">High Value</button>';
            $txtVal = 3;
        } else if($totalVal <= 80 && $totalVal >= 61) {
            $catValue = '<button type="button" class="btn btn-warning btn-rounded waves-effect waves-light">Medium Value</button>';
            $txtVal = 2;
        } else if ($totalVal <= 60) {
            $catValue = '<button type="button" class="btn btn-success btn-rounded waves-effect waves-light">Low Value</button>';
            $txtVal = 1;
        }

        $arrRisk = json_decode($data->risk_ev);
        $totalRisk = 0;
        foreach ($arrRisk as $key => $score) {
            $totalRisk += $score;
        }

        if ($totalRisk <= 100 && $totalRisk >= 81) {
            $catRisk = '<button type="button" class="btn btn-danger btn-rounded waves-effect waves-light">High Risk</button>';
            $txtRisk = 3;
        } else if ($totalRisk <= 80 && $totalRisk >= 61) {
            $catRisk = '<button type="button" class="btn btn-warning btn-rounded waves-effect waves-light">Medium Risk</button>';
            $txtRisk = 2;
        } else if ($totalRisk <= 60) {
            $catRisk = '<button type="button" class="btn btn-success btn-rounded waves-effect waves-light">Low Risk</button>';
            $txtRisk = 1;
        }

        $rating = $totalVal + $totalRisk / 2;
        if ($txtVal == 3 && $txtRisk == 3 || 
            $txtVal == 3 && $txtRisk == 2 || 
            $txtVal == 3 && $txtRisk == 1 || 
            $txtVal == 2 && $txtRisk == 3) {
            $matrix = '<button type="button" class="btn btn-primary waves-effect waves-light">Major Impact Project</button>';
            $path = 3;
        } else if ($txtVal == 2 && $txtRisk == 2 
            || $txtVal == 2 && $txtRisk == 1 
            || $txtVal == 2 && $txtRisk == 1 
            || $txtVal == 1 && $txtRisk == 3 
            || $txtVal == 1 && $txtRisk == 2) {
            $matrix = '<button type="button" class="btn btn-info waves-effect waves-light">Normal Impact Project</button>';
            $path = 2;
        } else {
            $matrix = '<button type="button" class="btn waves-effect" style="background-color: #b9f6ff !important; border: #b9f6ff !important;">Minor Impact Project</button>';
            $path = 2;
        }

        $evaluation = array(
            'totalVal' => $totalVal,
            'totalRisk' => $totalRisk,
            'catValue' => $catValue,
            'catRisk' => $catRisk,
            'matrix' => $matrix,
            'path' => $path
        );

        
        $question = (new RiskEv())->getIssue()[0];
        $arrOption = (array) json_decode($question->option);
        $getAnswer = (new Pir())->getAnswer($id_initiation)[0];
        $arrAnswer = (array) json_decode($getAnswer->question);
        foreach ($arrOption as $key => $item) {
            if ($item == $arrAnswer['11']) {
               $issue = $key;
            }
        }
        
        if ($request->isMethod('post')) {   
            $inputAll =  (new Pir)->getInput($id_initiation)[0];
            $inputState = array(
                'initiation' => $inputAll->id_initiation,
                'value' => $inputAll->id_value,
                'risk' => $inputAll->id_risk,
                'path' => $matrix,
                'rating' => $path,
                'issue' => $issue,
                'tot_val' => $totalVal,
                'tot_risk' => $totalRisk,
                'cat_val' => $catValue,
                'cat_risk' => $catRisk,
                'status' => 0, //new pir
                'modify_by' => Session::get('id')
            );
            // dd($inputState);
            $saveState = (new Pir)->createPir($inputState);
            if ($saveState) {
                $success = array('success' => 'Data has been saved Successfully.');
                return Redirect::to('pir')->withErrors($success);
            } else {
                $errors = array('error' => 'API Error, Please Contact Administrator');
                return view('pir.resume', compact('data', 'param', 'evaluation'))->withErrors($errors);
            }

        }

        return view('pir.resume', compact('data', 'param', 'evaluation'));

    }

    public function editResume(Request $request, $id=null) {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu') == 1 ) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        if ($id == null) {
            $errors = array('error' => 'Something went wrong!');
            return redirect('pir')->withErrors($errors);
        }

        try {
            $id_initiation = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            $errors = array('error' => 'The parameter is invalid !');
            return redirect('pir')->withErrors($errors);
        }

        $param = array(
            'id_initiation' => $id
        );

        $data = (new Pir())->resume($id_initiation)[0];
        
        if ($data->value_ev == null) {
            $errors = array('error' => 'Please fill Value Evaluation first');
            return redirect('pir/value_ev/edit/' . $id)->withErrors($errors);
        }

        if ($data->risk_ev == null) {
            $errors = array('error' => 'Please fill Risk Evaluation first');
            return redirect('pir/risk_ev/edit/' . $id)->withErrors($errors);
        }
        // dd($param);

        $arrValue = json_decode($data->value_ev);
        $totalVal = 0;
        foreach ($arrValue as $key => $score) {
            $totalVal += $score;
        }

        if ($totalVal <= 100 && $totalVal >= 81) {
            $catValue = '<button type="button" class="btn btn-danger btn-rounded waves-effect waves-light">High Value</button>';
            $txtVal = 3;
        } else if($totalVal <= 80 && $totalVal >= 61) {
            $catValue = '<button type="button" class="btn btn-warning btn-rounded waves-effect waves-light">Medium Value</button>';
            $txtVal = 2;
        } else if ($totalVal <= 60) {
            $catValue = '<button type="button" class="btn btn-success btn-rounded waves-effect waves-light">Low Value</button>';
            $txtVal = 1;
        }

        $arrRisk = json_decode($data->risk_ev);
        $totalRisk = 0;
        foreach ($arrRisk as $key => $score) {
            $totalRisk += $score;
        }

        if ($totalRisk <= 100 && $totalRisk >= 81) {
            $catRisk = '<button type="button" class="btn btn-danger btn-rounded waves-effect waves-light">High Risk</button>';
            $txtRisk = 3;
        } else if ($totalRisk <= 80 && $totalRisk >= 61) {
            $catRisk = '<button type="button" class="btn btn-warning btn-rounded waves-effect waves-light">Medium Risk</button>';
            $txtRisk = 2;
        } else if ($totalRisk <= 60) {
            $catRisk = '<button type="button" class="btn btn-success btn-rounded waves-effect waves-light">Low Risk</button>';
            $txtRisk = 1;
        }

        $rating = $totalVal + $totalRisk / 2;
        if ($txtVal == 3 && $txtRisk == 3 || 
            $txtVal == 3 && $txtRisk == 2 || 
            $txtVal == 3 && $txtRisk == 1 || 
            $txtVal == 2 && $txtRisk == 3) {
            $matrix = '<button type="button" class="btn btn-primary waves-effect waves-light">Major Impact Project</button>';
            $path = 3;
        } else if ($txtVal == 2 && $txtRisk == 2 
            || $txtVal == 2 && $txtRisk == 1 
            || $txtVal == 2 && $txtRisk == 1 
            || $txtVal == 1 && $txtRisk == 3 
            || $txtVal == 1 && $txtRisk == 2) {
            $matrix = '<button type="button" class="btn btn-info waves-effect waves-light">Normal Impact Project</button>';
            $path = 2;
        } else {
            $matrix = '<button type="button" class="btn waves-effect" style="background-color: #b9f6ff !important; border: #b9f6ff !important;">Minor Impact Project</button>';
            $path = 2;
        }

        $evaluation = array(
            'totalVal' => $totalVal,
            'totalRisk' => $totalRisk,
            'catValue' => $catValue,
            'catRisk' => $catRisk,
            'matrix' => $matrix,
            'path' => $path
        );

        
        $question = (new RiskEv())->getIssue()[0];
        $arrOption = (array) json_decode($question->option);
        $getAnswer = (new Pir())->getAnswer($id_initiation)[0];
        $arrAnswer = (array) json_decode($getAnswer->question);
        foreach ($arrOption as $key => $item) {
            if ($item == $arrAnswer['11']) {
               $issue = $key;
            }
        }
        
        if ($request->isMethod('post')) {   
            $inputAll =  (new Pir)->getInput($id_initiation)[0];
            $inputState = array(
                'initiation' => $inputAll->id_initiation,
                'value' => $inputAll->id_value,
                'risk' => $inputAll->id_risk,
                'path' => $matrix,
                'rating' => $path,
                'issue' => $issue,
                'tot_val' => $totalVal,
                'tot_risk' => $totalRisk,
                'cat_val' => $catValue,
                'cat_risk' => $catRisk,
                'status' => 4, //resubmit
                'modify_by' => Session::get('id')
            );
            // dd($inputState);
            $saveState = (new Pir)->updatePir($inputState);
            if ($saveState) {
                $success = array('success' => 'Data has been edit Successfully.');
                return Redirect::to('pir')->withErrors($success);
            } else {
                $errors = array('error' => 'API Error, Please Contact Administrator');
                return view('pir.edit.resume', compact('data', 'param', 'evaluation'))->withErrors($errors);
            }

        }

        return view('pir.edit.resume', compact('data', 'param', 'evaluation'));

    }

    public function getImage($id)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return response()->json("Not Authorized");
        }

        if (Session::get('menu') == 1 ) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        $data = (new Pir)->getInitiationId($id);
        return json_encode($data);
    }

    public function approve(Request $request) {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu') == 1 ) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }
        $id_initiation = $request->input('idapprove');
        $data = array(
            'initiation' => $id_initiation,
            'status' => 1,
            'reason' => null,
            'exec_by' => Session::get('id')
        );
        $saveState = (new Pir())->updateApproval($data);
        if ($saveState) {
            $success = array('success' => 'Data has been updated Successfully.');
            return Redirect::to('pir')->withErrors($success);
        } else {
            $errors = array('error' => 'API Error, Please Contact Administrator');
            return Redirect::to('pir')->withErrors($errors);
        }
    }

    public function postpone(Request $request) {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu') == 1 ) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }
        $id_initiation = $request->input('idpostpone');
        $data = array(
            'initiation' => $id_initiation,
            'status' => 3,
            'reason' => null,
            'exec_by' => Session::get('id')
        );
        $saveState = (new Pir())->updateApproval($data);
        if ($saveState) {
            $success = array('success' => 'Data has been updated Successfully.');
            return Redirect::to('pir')->withErrors($success);
        } else {
            $errors = array('error' => 'API Error, Please Contact Administrator');
            return Redirect::to('pir')->withErrors($errors);
        }
    }

    public function reject(Request $request) {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu') == 1 ) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }
        $id_initiation = $request->input('idreject');
        $reason = $request->input('reason_reject');
        $data = array(
            'initiation' => $id_initiation,
            'status' => 2,
            'reason' => $reason,
            'exec_by' => Session::get('id')
        );
        // dd($data);
        $saveState = (new Pir())->updateApproval($data);
        if ($saveState) {
            $success = array('success' => 'Data has been updated Successfully.');
            return Redirect::to('pir')->withErrors($success);
        } else {
            $errors = array('error' => 'API Error, Please Contact Administrator');
            return Redirect::to('pir')->withErrors($errors);
        }

    }
    
    public function update(Request $request, $id = null)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu') == 1 ) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }
        // dd($id);
        try {
            $id_initiation = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            $errors = array('error' => 'The parameter is invalid !');
            return redirect('pir')->withErrors($errors);
        }
        $dataUser = (new Pir)->getById($id_initiation);
        $department = (new Department())->get();
        // dd($dataUser);
        if ($dataUser) {
            $pir = $dataUser[0];
        } else {
            $pir = array();
            $errors = ['error' => 'Data Not Found, Please Add Data First'];
            return view('pir.add')->withErrors($errors);
        }

        if ($request->isMethod('post')) {
            $arrInput = Input::all();
            $arrInput['id'] = $id_initiation;
            unset($arrInput['_token']);

            $budget = str_replace('USD. ', '', $arrInput['estimate_budget']);
            $budget = str_replace(',', '', $budget);
            $arrInput['estimate_budget'] = $budget;

            $cost = str_replace('USD. ', '', $arrInput['cost']);
            $cost = str_replace(',', '', $cost);
            $arrInput['cost'] = $cost;
            
            $foto = $request->file('preliminary_design');
            if (isset($foto)) {
                if ($foto) {
                    $oldPath = public_path('assets/images/pir/' . $pir->preliminary_design);
                    unlink($oldPath);
                    $destinationPath = public_path('assets/images/pir/'); // upload path
                    $profileImage = date('dmyhis') . "-" . $foto->getClientOriginalName();
                    $profileImageFix = str_replace(')', '', str_replace('(', '', str_replace(' ', '_', $profileImage)));
                    $foto->move($destinationPath, $profileImageFix);
                    $arrInput['preliminary_design'] = "$profileImageFix";
                } else {
                    $errors = array('error' => 'Image Error, Please Contact Administrator');
                    return view('pir.edit.add', compact('pir','department'))->withErrors($errors);
                }
            }
            $saveState = (new Pir)->update($arrInput);
            // dd($saveState);
            if ($saveState) {
                return redirect('pir/value_ev/edit/' . Crypt::encrypt($id_initiation));
            } else {
                $errors = array('error' => 'API Error, Please Contact Administrator');
                return view('pir.edit.add', compact('pir','department'))->withErrors($errors);
            }
        }
        return view('pir.edit.add', compact('pir','department'));
    }

    public function delete(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        if (Session::get('menu') == 1 ) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        $dataUser = (new Pir)->getById($request->id);
        if ($dataUser) {
            $pir = $dataUser[0];
            $data = array(
                'id' => intval($pir->id),
            );
            $saveState = (new Pir)->delete($data);
            $success = array('success' => 'Data has been deleted Successfully.');
            return Redirect::to('pir')->withErrors($success);
        } else {
            $errors = array('error' => 'API Error, Please Contact Administrator');
            return Redirect::to('pir')->withErrors($errors);
        }
    }

    public function getDetailPir($id_initiation) {
        if (!(Session::has('login') && Session::get('login'))) {
            return response()->json("Not Authorized");
        }

        if (Session::get('menu') == 1 ) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        $data = (new pir())->getDetailPir($id_initiation)[0];
        $data->estimate_budget = '$. '.number_format($data->estimate_budget, 3);
        $data->cost = '$. '.number_format($data->cost, 3);
        
        return json_encode($data);
    }

    public function getAjaxData(Request $request)
    {
        if (!(Session::has('login') && Session::get('login'))) {
            return response()->json("Not Authorized");
        }

        if (Session::get('menu') == 1 ) {
            Session::flush();
            return Redirect::to('login')->withErrors(['error' => 'Access Denied']);
        }

        if (Session::get('role') != 1) {
            $returnData = (new Pir())->getVwPirById(Session::get('id'));
        } else {
            $returnData = (new Pir())->get();
        }
        
        if (!$returnData) {
            $output = array('data' => '');
            return $output;
        }
        $num = 0;

        foreach ($returnData as $key => $data) {
            $strAction = '';
            $strAction .= '
                <a href="" class="btn btn-xs btn-default" title="Detail" data-id="' . $data->id_initiation . '" data-act="deactivate" data-toggle="modal" data-target="#detail"><i class="fa fa-info-circle"></i></a>
                <a href="" class="btn btn-xs btn-danger" title="Delete" data-id="' . $data->id_initiation . '" data-act="deactivate" data-toggle="modal" data-target="#del"><i class="fa fa-trash"></i></a>
            ';
            if ($data->status == 2 && Session::get('id') == $data->id_user) {
                $strAction .= '<a href="' . url('pir/edit/' . Crypt::encrypt($data->id_initiation)) . '" title="edit" class="btn btn-xs btn-inverse"><i class="fa fa-pencil"></i></a>';   
            }

            if ($data->status == 0) {
                $status = '<span class="label label-info">New</span>';
            } else if($data->status == 1) {
                $status = '<span class="label label-success">Approved</span>';
            } else if ($data->status == 2) {
                // $status = (new Pir())->getDetailPir($data->id_initiation)[0]->reject_reason;
                $status = '<span class="label label-danger">Rejected</span>';
            } else if ($data->status == 3) {
                $status = '<span class="label label-warning">Postponed</span>';
            } else {
                $status = '<span class="label label-purple">Resubmitted</span>';
            }

            $output['data'][] = array(
                'id' => $data->id,
                'status' => $status,
                'descr' => $data->descr,
                'type' => $data->type,
                'duration' => $data->time_frame_start.' / '.$data->time_frame_end,
                'cost' => '$ '.number_format($data->cost, 3),
                'impact' => $data->impact_etc==null?$data->impact:$data->impact_etc,
                'constraint' => $data->constraint,
                'path' => $data->path,
                'rating' => $data->rating,
                'issue' => $data->issue,
                'submit_by' => $data->nama,
                'exec_by' => $data->exec_by,
                'create_date' => date('d-M-Y H:i', strtotime($data->create_date)),
                'action' => $strAction
            );

            $num++;
        }
        return json_encode($output);
    }
}
