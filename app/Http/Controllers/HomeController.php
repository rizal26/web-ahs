<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Models\Employee;

class HomeController extends Controller {

    public function __construct() {
        // $this->middleware('auth');
    }

    public function index() {
    	if(!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('login')->withErrors(['error' => 'Please Sign in to continue.']);
        }

        return view('dashboard');
    }

    public function timestamp() {
    	date_default_timezone_set('Asia/Kuala_Lumpur');
        echo $timestamp = date('d M Y - H:i:s').' WITA';
        // return view('dashboard');
    }
}