<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    if(!(Session::get('login'))){
        return redirect('login');
    }
    return redirect('home');
});

//Login
Route::match(['get','post'], '/login', 'Auth\AuthController@login')->name('login');
Route::get('/login',['as'=>'login','uses'=>'Auth\AuthController@login']);
Route::get('/logout', 'Auth\AuthController@logout')->name('logout');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/timestamp', 'HomeController@timestamp')->name('home.timestamp');

//user-manage
Route::get('/user', 'Admin\UserManagementController@index')->name('user');
Route::get('/user/ajax', 'Admin\UserManagementController@getAjaxData')->name('user.ajax');
Route::match(['get','post'], '/user/add', 'Admin\UserManagementController@create')->name('user.add');
Route::match(['get','post'], '/user/edit/{id}', 'Admin\UserManagementController@update')->name('user.edit');
Route::delete('/user/del/{id}', 'Admin\UserManagementController@delete')->name('user.del');

//material-manage
Route::get('/material', 'Apps\MaterialController@index')->name('material');
Route::get('/material/ajax', 'Apps\MaterialController@getAjaxData')->name('material.ajax');
Route::get('/material/view/{id}', 'Apps\MaterialController@getImage')->name('material.image');
Route::match(['get','post'], '/material/add', 'Apps\MaterialController@create')->name('material.add');
Route::match(['get','post'], '/material/edit/{id}', 'Apps\MaterialController@update')->name('material.edit');
Route::delete('/material/del/{id}', 'Apps\MaterialController@delete')->name('material.del');

//kelompok-manage
Route::get('/kelompok', 'Master\KelompokController@index')->name('kelompok');
Route::get('/kelompok/ajax', 'Master\KelompokController@getAjaxData')->name('kelompok.ajax');
Route::match(['get', 'post'], '/kelompok/ajaxselect/', 'Master\KelompokController@selectKelompok')->name('kelompok.select');
Route::match(['get', 'post'], '/kelompok/add', 'Master\KelompokController@create')->name('kelompok.add');
Route::match(['get', 'post'], '/kelompok/edit/{id}', 'Master\KelompokController@update')->name('kelompok.edit');
Route::delete('/kelompok/del/{id}', 'Master\KelompokController@delete')->name('kelompok.del');

//AHS
Route::get('/ahs', 'Apps\AhsController@index')->name('ahs');
Route::get('/ahs/ajax', 'Apps\AhsController@getAjaxData')->name('ahs.ajax');
Route::get('/ahs/detail/{id}', 'Apps\AhsController@getDetail')->name('ahs.detail');
Route::match(['get', 'post'], '/ahs/ajaxselect/', 'Apps\AhsController@selectahs')->name('ahs.select');
Route::match(['get', 'post'], '/ahs/add', 'Apps\AhsController@create')->name('ahs.add');
Route::match(['get', 'post'], '/ahs/edit/{id}', 'Apps\AhsController@update')->name('ahs.edit');
Route::delete('/ahs/del/{id}', 'Apps\AhsController@delete')->name('ahs.del');

//department-manage
Route::get('/department', 'Master\DepartmentController@index')->name('department');
Route::get('/department/ajax', 'Master\DepartmentController@getAjaxData')->name('department.ajax');
Route::match(['get', 'post'], '/department/ajaxselect/', 'Master\DepartmentController@selectKelompok')->name('department.select');
Route::match(['get', 'post'], '/department/add', 'Master\DepartmentController@create')->name('department.add');
Route::match(['get', 'post'], '/department/edit/{id}', 'Master\DepartmentController@update')->name('department.edit');
Route::delete('/department/del/{id}', 'Master\DepartmentController@delete')->name('department.del');

//pir-manage
Route::get('/pir', 'Apps\PirController@index')->name('pir');
Route::get('/pir/ajax', 'Apps\PirController@getAjaxData')->name('pir.ajax');
Route::get('/pir/view/{id}', 'Apps\PirController@getImage')->name('pir.image');
Route::get('/pir/detail/{id}', 'Apps\PirController@getDetailPir')->name('pir.detail');
Route::match(['get', 'post'], '/pir/ajaxselect/', 'Apps\PirController@selectKelompok')->name('pir.select');
Route::match(['get', 'post'], '/pir/add', 'Apps\PirController@create')->name('pir.add');
Route::match(['get', 'post'], '/pir/edit/{id}', 'Apps\PirController@update')->name('pir.edit');
Route::delete('/pir/del/{id}', 'Apps\PirController@delete')->name('pir.del');
Route::match(['get','post'], '/pir/search', 'Apps\PirController@search')->name('pir.search');

//step 2
Route::match(['get', 'post'], '/pir/value_ev/add/{id}', 'Apps\PirController@createValue')->name('value_ev.add');
Route::match(['get', 'post'], '/pir/value_ev/edit/{id}', 'Apps\PirController@editValue')->name('value_ev.edit');

//step 3
Route::match(['get', 'post'], '/pir/risk_ev/add/{id}', 'Apps\PirController@createRisk')->name('risk_ev.add');
Route::match(['get', 'post'], '/pir/risk_ev/edit/{id}', 'Apps\PirController@editRisk')->name('risk_ev.edit');

//step 4
Route::match(['get', 'post'], '/pir/resume/add/{id}', 'Apps\PirController@createResume')->name('resume.add');
Route::match(['get', 'post'], '/pir/resume/edit/{id}', 'Apps\PirController@editResume')->name('resume.edit');

Route::match(['get', 'post'], '/pir/approve', 'Apps\PirController@approve')->name('pir.approve');
Route::match(['get', 'post'], '/pir/postpone', 'Apps\PirController@postpone')->name('pir.postpone');
Route::match(['get', 'post'], '/pir/reject', 'Apps\PirController@reject')->name('pir.reject');